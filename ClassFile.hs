module ClassFile where
import Data.List
import Data.Char (ord)
import ClassFormat
import AbstrakteSyntax
import KonstantenPool

createPrg :: [Class] -> [ClassFile]
createPrg ([]) = []
createPrg (a:ass) = (createClassFile a : createPrg ass)

createClassFile :: Class -> ClassFile
createClassFile(Class(access, typ, ClassBody(fields, methods))) =
    let
        constantPool = classToCP(Class(access, typ, ClassBody(fields, methods)))
        cp_count = listLength(constantPool) + 1
        accessF = buildAccessFlags(access)
        this = findClassIndex(constantPool, typ)
        super = findClassIndex(constantPool, "java/lang/Object")
        fieldInfos = getFieldsInfos(constantPool, fields) 
        fieldCount = listLength(fieldInfos)
        methodInfos = buildMethods(constantPool, methods, typ)
        methodCount = listLength(methodInfos)
    in
        ClassFile Magic (MinorVersion(0)) (MajorVersion(49)) cp_count constantPool accessF (ThisClass(this)) (SuperClass(super)) 0 [] fieldCount fieldInfos methodCount methodInfos 0 []

-- Length of List
listLength :: [a] -> Int
listLength [] = 0
listLength (_:tail) = 1 + listLength(tail)


getFieldsInfos :: ([CP_Info], [FieldDecl]) -> [Field_Info]
getFieldsInfos(_, []) = []
getFieldsInfos(constantPool, (field : fieldRest)) =
    let
        fieldInfo = createFieldInfo(constantPool, field)
        buffer = getFieldsInfos(constantPool, fieldRest)
    in
        (fieldInfo : buffer)


createFieldInfo :: ([CP_Info], FieldDecl) -> Field_Info
createFieldInfo(constantpool, FieldDecl(access, typ, name)) =
    let
        accessF = buildAccessFlagsInMethod(access)
        index = findUtf8InfoIndex(constantpool, name)
        typString = typeToCPType(typ)
        typId = findUtf8InfoIndex(constantpool, typString)
    in
        Field_Info accessF index typId 0 []


buildMethods :: ([CP_Info], [MethodDecl], String) -> [Method_Info]
buildMethods(cp, methods, classname) =
    let
        defconstructor = findConstructor(methods, classname, cp)
        infos = getMethodInfos(cp, methods)
    in
        constrHelp(defconstructor, infos)

constrHelp :: (Maybe Method_Info, [Method_Info]) ->  [Method_Info]
constrHelp(Nothing, cs) = cs
constrHelp(Just c, cs) = (c : cs)

getMethodInfos :: ([CP_Info], [MethodDecl]) -> [Method_Info]
getMethodInfos(_, []) = []
getMethodInfos(cp, (method : methodRest)) =
    let
        methodInfo = createMethodInfo(cp, method)
        buffer = getMethodInfos(cp, methodRest)
    in
        (methodInfo : buffer)

createMethodInfo :: ([CP_Info], MethodDecl) -> Method_Info
createMethodInfo(cp, MethodDecl(access, typ, name, parameter, code)) =
    let
        assemblerCode = generateCode(code, parameter, cp, 0, -1)
        aCWithReturn = addReturnVoidToMethod(assemblerCode)
        length = listLength(aCWithReturn)
        codeid = findUtf8InfoIndex(cp, "Code")
        accessflags = buildAccessFlagsInMethod(access)
        methodNameIndex = findUtf8InfoIndex(cp, name)
        lengthAttribute = length + 4 + 4 + 2 + 2
        paramsOnlyTypes = transformToOnlyTypes(parameter)
        ownTypeIDString = buildOwnTypeId(paramsOnlyTypes, typ, "")
        ownTypeID = findUtf8InfoIndex(cp, ownTypeIDString)
    in
        Method_Info accessflags methodNameIndex ownTypeID 1 [(AttributeCode codeid lengthAttribute (getStackSize(aCWithReturn)) (getLocalSize(aCWithReturn, parameter,0)) length aCWithReturn 0 [] 0 [])]



createMethodInfo(cp, ConstructorDecl(access, name, parameter, code)) =
    let
        assemblerCode = generateCode(code, parameter, cp, 0, -1)
        aCWithReturn = addReturnVoidToMethod(assemblerCode)
        classIndex = findMethodRef(cp, "<init>", [] ,"void", "java/lang/Object")
        fullCode = joinCodeParts([42, 183, 0, classIndex], aCWithReturn)
        length = listLength(fullCode)
        codeid = findUtf8InfoIndex(cp, "Code")
        accessflags = buildAccessFlagsInMethod(access)
        methodNameIndex = findUtf8InfoIndex(cp, "<init>")
        lengthAttribute = length + 4 + 4 + 2 + 2
        paramsOnlyTypes = transformToOnlyTypes(parameter)
        ownTypeIDString = buildOwnTypeId(paramsOnlyTypes, "void", "")
        ownTypeID = findUtf8InfoIndex(cp, ownTypeIDString)
    in
        Method_Info accessflags methodNameIndex ownTypeID 1 [(AttributeCode codeid lengthAttribute (getStackSize(fullCode)) (getLocalSize(fullCode, parameter,0)) length fullCode 0 [] 0 [])]

transformToOnlyTypes :: [(Type, String)] -> [Type]
transformToOnlyTypes([]) = []
transformToOnlyTypes((t,n) : ts) = (t : transformToOnlyTypes(ts))

--TODO
-- bei MaxStackSize: Berechnung der Parameterzahl bei invokevirtual/invokespecial
-- Sprünge berücksichtigen => nach Sprungbefehl zwei Zahlen überspringen (skipCode)
-- evtl. Befehle vergessen?

getLocalSize :: ([Int], [(Type, String)], Int) -> Int
getLocalSize(code, [], x) = getLocalSizeRek(code, x + 1)
getLocalSize(code, (c:cp), x) = getLocalSize(code, cp, x + 1)

getLocalSizeRek :: ([Int], Int) -> Int
getLocalSizeRek([], current) = current
getLocalSizeRek((c : cc), current) 
    | (c == 21 || c == 25 || c == 54 || c == 58) = getLocalSizeRek(skipCode(cc, 1), checkLocalPosition(getLoadStoreParam(cc)+1, current))
    | (c >= 26 && c <= 29) = getLocalSizeRek(cc, checkLocalPosition(c-25, current))
    | (c >= 42 && c <= 45) = getLocalSizeRek(cc, checkLocalPosition(c-41, current))
    | (c >= 59 && c <= 62) = getLocalSizeRek(cc, checkLocalPosition(c-58, current))
    | (c >= 75 && c <= 78) = getLocalSizeRek(cc, checkLocalPosition(c-74, current))
    | otherwise = getLocalSizeRek(cc, current)

getStackSize :: ([Int]) -> Int
getStackSize(code) = getStackSizeRek(code, 0, 0)

getStackSizeRek :: ([Int], Int, Int) -> Int
getStackSizeRek([], current, highest) = highest
getStackSizeRek((c:cc), current, highest)
    | current > highest = getStackSizeRek((c:cc), current, current)
    | ((c >= 3 && c <=8) || c == 16 || c == 18) = getStackSizeRek(cc, current + 1, highest) --iconst, bipush, ldc
    | (c == 21 || c == 25 || (c >= 26 && c <= 29) || (c >= 42 && c <= 45)) = getStackSizeRek(cc, current + 1, highest) --iload, aload
    | (c == 89 || c == 187) = getStackSizeRek(cc, current + 1, highest) --dup, new
    | (c == 54 || c == 58 || (c >= 59 && c <= 62) || (c >= 75 && c <= 78)) = getStackSizeRek(cc, current - 1, highest) --istore, astore
    | (c == 96 ||c == 100 || c == 104 || c == 108 || c == 112) = getStackSizeRek(cc, current - 1, highest) --Rechnungen
    | (c >= 153 && c <= 158) = getStackSizeRek(cc, current - 1, highest) --if mit einem Parameter
    | (c >= 159 && c <= 166) = getStackSizeRek(cc, current - 2, highest) --if mit zwei Parametern
    | (c == 182 || c == 183) = getStackSizeRek(cc, current - 1, highest) --invoke Method => noch nicht fertig!
    | otherwise = getStackSizeRek(cc, current, highest)

getLoadStoreParam :: ([Int]) -> Int
getLoadStoreParam(c:cp) = c

checkLocalPosition :: (Int, Int) -> Int
checkLocalPosition(newValue, current) = 
    if(newValue > current) then
        newValue
    else
        current

skipCode :: ([Int], Int) -> [Int]
skipCode(c, 0) = c
skipCode((c:cc), x) = skipCode(cc, x - 1)

methodDeclParamsToOnlyTypes :: [(Type, String)] -> [Type]
methodDeclParamsToOnlyTypes([]) = []
methodDeclParamsToOnlyTypes((t,s):cp) = (t : methodDeclParamsToOnlyTypes(cp))

buildAccessFlags :: Maybe Access -> AccessFlags
buildAccessFlags(Nothing) = AccessFlags [32]
buildAccessFlags(Just Public) = AccessFlags [1, 32]
buildAccessFlags(Just Private) = AccessFlags [2, 32]
buildAccessFlags(Just Protected) = AccessFlags [4, 32]

buildAccessFlagsInMethod :: Maybe Access -> AccessFlags
buildAccessFlagsInMethod(Nothing) = AccessFlags []
buildAccessFlagsInMethod(Just Public) = AccessFlags [1]
buildAccessFlagsInMethod(Just Private) = AccessFlags [2]
buildAccessFlagsInMethod(Just Protected) = AccessFlags [4]

findConstructor :: ([MethodDecl], String, [CP_Info]) -> Maybe Method_Info
findConstructor([], name, cp) = 
    let
        constructor = createDefaultConstructor(name, cp)
    in
        Just constructor
findConstructor((MethodDecl(_,_,_,_,_) : cs), name, cp) = findConstructor(cs, name, cp)
findConstructor((ConstructorDecl(_,_,_,_) : cs), _, _) = Nothing
    

createDefaultConstructor :: (String, [CP_Info]) -> Method_Info
createDefaultConstructor(name, cp) =
    let
        nameID = findUtf8InfoIndex(cp, "<init>")
        descID = findUtf8InfoIndex(cp, "()V")
        code = createConstructorCode(cp)
    in
        Method_Info (AccessFlags [1]) nameID descID 1 [code]

createConstructorCode :: ([CP_Info]) -> Attribute_Info
createConstructorCode(cp) = 
    let
        id = findUtf8InfoIndex(cp, "Code")
        classIndex = findMethodRef(cp, "<init>", [] ,"void", "java/lang/Object")
    in
        AttributeCode id 17 1 1 5 [42, 183, 0, classIndex, 177] 0 [] 0 []


addReturnVoidToMethod :: [Int] -> [Int]
addReturnVoidToMethod([177]) = [177]
addReturnVoidToMethod([176]) = [176]
addReturnVoidToMethod([172]) = [172]
addReturnVoidToMethod([]) = [177]
addReturnVoidToMethod(c:cs) = 
    let
        buffer = addReturnVoidToMethod(cs)
    in
        (c:buffer)


generateCode ::  (Stmt, [(Type, String)], [CP_Info], Int, Int) -> [Int]

generateCode(TypedStmt(x,y), vars, cp, stelle, schleifeStelle) = generateCode(x, vars, cp, stelle, schleifeStelle)
generateCode(Block([]), _, _, _, _) = []
generateCode(Block(TypedStmt(LocalVarDecl(t, s),_):cs), locVars, cp, stelle, schleifeStelle) =
    let
        newVars = joinCodeParts(locVars, [(t, s)])
    in
        generateCode(Block(cs), newVars, cp, stelle, schleifeStelle)

generateCode(Block(c:cs), locVars, cp, stelle, schleifeStelle) = 
    let
        code = generateCode(c, locVars, cp, stelle, schleifeStelle)
        codeLength = listLength(code)
        buffer = generateCode(Block(cs), locVars, cp, stelle +  codeLength, schleifeStelle)
    in
        joinCodeParts(code, buffer)

generateCode(ReturnVoid, _, _, _, _) = [177]
generateCode(Empty, _, _, _, _) = []
generateCode(Break, _, _, _, _) = [167, 0, -6]
generateCode(Continue, _, _, stelle, schleifeStelle) = [167, 255, 255 - stelle + schleifeStelle]

generateCode(Return(exp), vars, cp, _, _) = generateCodeForReturn(cp, vars, exp)
generateCode(While(exp ,stm), vars, cp, stelle, _) = generateCodeForWhile(cp, vars,exp,stm, stelle)
generateCode(If(x,y,z), vars, cp, stelle, schleifeStelle) = generateCodeForIf(cp, vars, (x,y,z), stelle, schleifeStelle)
generateCode(ElseIf(x, y , z, a), vars, cp, stelle, schleifeStelle ) = generateCodeForElseIf(cp, vars, x,y,z,a, stelle, schleifeStelle)
generateCode(StmtExprStmt(x), vars, cp, exp, _) = stmtExprCode(x, cp, vars, exp)


generateCodeForReturn :: ([CP_Info], [(Type, String)], Expr) -> [Int]
generateCodeForReturn(cp, vars, TypedExpr(exp, t)) = 
    let 
        returnCode = typeReturn(t)
        expCode = exprCode(cp, exp, vars)
    in
        joinCodeParts(expCode, [returnCode])

typeReturn :: String -> Int
typeReturn("int") = 172
typeReturn("boolean") = 172
typeReturn("char") = 172
typeReturn(_) = 176

generateCodeForWhile :: ([CP_Info], [(Type, String)], Expr, Stmt, Int) -> [Int]                                         -- Fehlt für Binary
generateCodeForWhile(cp, vars, TypedExpr(exp, t) ,stm, stelle)  = generateCodeForWhile(cp, vars, exp ,stm, stelle)
generateCodeForWhile(cp, vars, LocalOrFieldVar(exp) ,stm, stelle) = 
    let
        expCode = exprCode(cp, LocalOrFieldVar(exp), vars)
        expCodeLength = listLength(expCode)
        stmtCode = generateCode(stm, vars, cp, stelle + expCodeLength + 3, stelle)
        jmpto = listLength(stmtCode) + 3
        jmptoCode = [153, 0, jmpto]
        expAndJmp = joinCodeParts(expCode, jmptoCode)
        missingGoTo = joinCodeParts(expAndJmp, stmtCode)
        goTo = [167, 255,  255 - (listLength(stmtCode) + expCodeLength + 3)]
        goToFinal = if (goTo == [167, 255, 255]) then
                [167, 0, 0]
            else
                goTo
        fullCode = joinCodeParts(missingGoTo, goToFinal)
        length = listLength(fullCode)
    in
        replaceBreaks(fullCode, stelle, length + stelle + 2)

generateCodeForWhile(cp, vars, InstVar(exp, s) ,stm, stelle) = 
    let
        expCode = exprCode(cp, InstVar(exp, s), vars)
        expCodeLength = listLength(expCode)
        stmtCode = generateCode(stm, vars, cp, stelle + expCodeLength + 3, stelle)
        jmpto = listLength(stmtCode) + 3
        jmptoCode = [153, 0, jmpto]
        expAndJmp = joinCodeParts(expCode, jmptoCode)
        missingGoTo = joinCodeParts(expAndJmp, stmtCode)
        goTo = [167, 255,  255 - (listLength(stmtCode) + expCodeLength + 3)]
        goToFinal = if (goTo == [167, 255, 255]) then
                [167, 0, 0]
            else
                goTo
        fullCode = joinCodeParts(missingGoTo, goToFinal)
        length = listLength(fullCode)
    in
        replaceBreaks(fullCode, stelle, length + stelle + 2)

generateCodeForWhile(cp, vars, StmtExprExpr(MethodCall(x, y, z)) ,stm, stelle) = 
    let
        expCode = exprCode(cp, StmtExprExpr(MethodCall(x, y, z)), vars)
        expCodeLength = listLength(expCode)
        stmtCode = generateCode(stm, vars, cp, stelle + expCodeLength + 3, stelle)
        jmpto = listLength(stmtCode) + 3
        jmptoCode = [153, 0, jmpto]
        expAndJmp = joinCodeParts(expCode, jmptoCode)
        missingGoTo = joinCodeParts(expAndJmp, stmtCode)
        goTo = [167, 255,  255 - (listLength(stmtCode) + expCodeLength + 3)]
        goToFinal = if (goTo == [167, 255, 255]) then
                [167, 0, 0]
            else
                goTo
        fullCode = joinCodeParts(missingGoTo, goToFinal)
        length = listLength(fullCode)
    in
        replaceBreaks(fullCode, stelle, length + stelle + 2)

generateCodeForWhile(cp, vars, Bool(True) ,stm, stelle) = 
    let 
        stmtCode = generateCode(stm, vars, cp, stelle, stelle)
        lengthOfStmt = listLength(stmtCode)
        goTo = [167, 255,  255 - lengthOfStmt + 1]
        goToFinal = if (goTo == [167, 255, 255]) then
                [167, 0, 0]
            else
                goTo
        fullCode = joinCodeParts(stmtCode, goToFinal)
        length = listLength(fullCode)
    in 
        replaceBreaks(fullCode, stelle, length + stelle + 2)
generateCodeForWhile(cp, vars, Bool(False) ,stm, stelle) = []

generateCodeForWhile(cp, vars, Binary(op, par1, par2), stm, stelle) =
    let
        whileCondCode = exprCode(cp, Binary(op, par1, par2), vars)
        lengthCondCode = listLength(whileCondCode)
        stmts = generateCode(stm, vars, cp, stelle, stelle)
        lengthOfStmt = listLength(stmts)
        checkCode = [3, 159, 0, lengthOfStmt + 6]
        goBack = [167, 255,  255 - (lengthOfStmt + lengthCondCode + 3)]
        goToFinal = if (goBack == [167, 255, 255]) then
                [167, 0, 0]
            else
                goBack
        code1 = joinCodeParts(whileCondCode, checkCode)
        code2 = joinCodeParts(code1, stmts)
        fullCode = joinCodeParts(code2, goToFinal)
        length = listLength(fullCode)
    in
        replaceBreaks(fullCode, stelle, length + stelle + 2)

generateCodeForWhile(cp, vars, Unary(op, par), stm, stelle) =
    let
        whileCondCode = exprCode(cp, Unary(op, par), vars)
        lengthCondCode = listLength(whileCondCode)
        stmts = generateCode(stm, vars, cp, stelle, stelle)
        lengthOfStmt = listLength(stmts)
        checkCode = [3, 159, 0, lengthOfStmt + 6]
        goBack = [167, 255,  255 - (lengthOfStmt + lengthCondCode + 3)]
        goToFinal = if (goBack == [167, 255, 255]) then
                [167, 0, 0]
            else
                goBack
        code1 = joinCodeParts(whileCondCode, checkCode)
        code2 = joinCodeParts(code1, stmts)
        fullCode = joinCodeParts(code2, goToFinal)
        length = listLength(fullCode)
    in
        replaceBreaks(fullCode, stelle, length + stelle + 2)

replaceBreaks :: ([Int], Int, Int) -> [Int]
replaceBreaks([], _, _) = []
replaceBreaks((c : cs), stelle, jmpTo) =
    let 
        buffer = replaceBreaks(cs, stelle + 1, jmpTo)
    in
        if c == -6 then (jmpTo-stelle : buffer)
        else (c : buffer)

generateCodeForIf :: ([CP_Info], [(Type, String)], (Expr, Stmt, Maybe Stmt), Int, Int) -> [Int]
generateCodeForIf(cp, vars, (TypedExpr(exp, t), tru, fals), stelle, schleifeStelle) = generateCodeForIf(cp, vars, (exp, tru, fals), stelle, schleifeStelle)
generateCodeForIf(cp, vars, (Bool(True), tru, fals), stelle, schleifeStelle) = generateCode(tru, vars, cp, stelle, schleifeStelle)
generateCodeForIf(cp, vars, (Bool(False), tru, Nothing), stelle, _) = []
generateCodeForIf(cp, vars, (Bool(False), tru, Just fals), stelle, schleifeStelle) = generateCode(fals, vars, cp, stelle, schleifeStelle)
generateCodeForIf(cp, vars, (cond, tru, Nothing), stelle, schleifeStelle) = generateCodeForJustIf(cp, vars, cond, tru, stelle, schleifeStelle)
generateCodeForIf(cp, vars, (cond, tru, Just fals), stelle, schleifeStelle) = generateCodeForIfElse(cp, vars, cond, tru, fals, stelle, schleifeStelle)

generateCodeForJustIf :: ([CP_Info], [(Type, String)], Expr, Stmt, Int, Int) -> [Int]
generateCodeForJustIf(cp, vars, exp, stm, stelle, schleifeStelle) = 
    let
        expCode = exprCode(cp, exp, vars)
        expCodeLength = listLength(expCode)
        stmtCode = generateCode(stm, vars, cp, stelle + expCodeLength + 3, schleifeStelle)
        jmpto = listLength(stmtCode) + 3
        jmptoCode = [153, 0, jmpto]
        expAndJmp = joinCodeParts(expCode, jmptoCode)
        fullCode = joinCodeParts (expAndJmp, stmtCode)
    in
        joinCodeParts (fullCode, [0])

generateCodeForIfElse :: ([CP_Info], [(Type, String)], Expr, Stmt, Stmt, Int, Int) -> [Int]
generateCodeForIfElse(cp, vars, cond, tru, fals, stelle, schleifeStelle) = 
    let
        expCode = exprCode(cp, cond, vars)
        expCodeLength = listLength(expCode)
        thenCode = generateCode(tru, vars, cp, stelle + expCodeLength + 3, schleifeStelle)
        jmpto1 =  6 + listLength(thenCode) 
        jmpto1Code = [153, 0, jmpto1]
        elseCode = generateCode(fals, vars, cp, jmpto1, schleifeStelle)
        jmpto2 = listLength(elseCode) + 3
        jmpto2Code = [167, 0, jmpto2]
        code1 = joinCodeParts(expCode, jmpto1Code)
        code2 = joinCodeParts(code1, thenCode)
        code3 = joinCodeParts(code2, jmpto2Code)
        fullCode = joinCodeParts(code3, elseCode)
    in
        joinCodeParts (fullCode, [0])
        

generateCodeForElseIf :: ([CP_Info], [(Type, String)], Expr, Stmt , [(Expr,Stmt)], Maybe Stmt, Int, Int) -> [Int]
generateCodeForElseIf(cp, vars, exp, stm, elseIfs, Nothing, stelle, schleifeStelle) = 
    let
        code = elseIfCode(cp, vars, ((exp, stm) : elseIfs), stelle, schleifeStelle)
        end = listLength(code) + stelle
        fullCode = joinCodeParts(code, [0])
    in
        replaceJumpInElseIf(fullCode, stelle, end)

generateCodeForElseIf(cp, vars, exp, stm, elseIfs, Just elseStmt, stelle, schleifeStelle) = 
    let
        code = elseIfCode(cp, vars, ((exp, stm) : elseIfs), stelle, schleifeStelle)
        codeLength = listLength(code)
        stmtCode = generateCode(elseStmt, vars, cp, stelle + codeLength, schleifeStelle)
        fullCode = joinCodeParts(code, stmtCode)
        end = listLength(fullCode) + stelle
        nopCode = joinCodeParts(fullCode, [0])
    in
        replaceJumpInElseIf(fullCode, stelle, end)

elseIfCode :: ([CP_Info], [(Type, String)], [(Expr,Stmt)], Int, Int) -> [Int]
elseIfCode(_, _, [], _, _) = []
elseIfCode(cp, vars, ((exp, stm) : cs),  stelle, schleifeStelle) = 
    let
        expCode = exprCode(cp, exp, vars)
        expCodeLength = listLength(expCode)
        stmtCode = generateCode(stm, vars, cp, stelle + expCodeLength + 3, schleifeStelle)
        next = listLength(stmtCode) + 6
        jmpToNext = [153, 0, next]
        jmpToEnd = [167, 0, -9]
        code1 = joinCodeParts(expCode, jmpToNext)
        code2 = joinCodeParts(code1, stmtCode)
        fullCode = joinCodeParts(code2, jmpToEnd)
        codeLength = listLength(fullCode)
        buffer = elseIfCode(cp, vars, cs, stelle + codeLength, schleifeStelle)
    in
        joinCodeParts(fullCode, buffer)

replaceJumpInElseIf :: ([Int], Int, Int) -> [Int]
replaceJumpInElseIf([], _, _) = []
replaceJumpInElseIf((c : cs), stelle, jmpTo) =
    let 
        buffer = replaceJumpInElseIf(cs, stelle + 1, jmpTo)
    in
        if c == -9 then (jmpTo - stelle + 2 : buffer)
        else (c : buffer)
        

joinCodeParts :: ([a], [a]) -> [a]
joinCodeParts(x, []) = x
joinCodeParts([], x) = x
joinCodeParts((c:cs), x) =
    let
        buffer = joinCodeParts(cs, x)
    in
        (c : buffer)

exprCode :: ([CP_Info], Expr, [(Type, String)]) -> [Int]        -- Missing: InstVar, Unary, Binary, StmtExprExpr, Char
exprCode(cp, This, _) = [42]
exprCode(cp, Jnull, _) = [1]
exprCode(cp, Bool(True), _) = [4]
exprCode(cp, Bool(False), _) = [3]
exprCode(cp, Integer(i), _)
    | i < 0 = [16, fromIntegral i]
    | i > 5 = [16, fromIntegral i]
    | otherwise = [(3 + (fromIntegral i))]
exprCode(cp, Char(c), _) = 
    let
        charNum = ord c
    in
       [16, charNum]

exprCode(cp, String(s), _) =
    let
        id = findStringInCP(cp, s)
    in
        [18, id]

exprCode(cp, LocalOrFieldVar(name), vars) =
    let
        id = searchLocalVar(name, vars, 1)
    in
        localFieldVarCode(cp, id, name)

exprCode(cp, TypedExpr(exp, _), vars) = exprCode(cp, exp, vars)

exprCode(cp, Binary(rec, x, y), vars) = 
    if(rec == "+" ||rec == "-" || rec == "*" || rec =="/" || rec == "%" || rec == "<<" || rec == ">>" || rec == "^" || rec == "&" || rec == "|") then
        arithmeticOperator(cp, Binary(rec, x, y), vars)
    else
        conditionalOperator(cp, Binary(rec, x, y), vars) 

exprCode(cp, Unary(rec, x), vars) = 
    let
        operator = exprCode(cp, x, vars)
        opCode = findUnaryOperator(rec)
    in
        joinCodeParts(operator, opCode)

exprCode(cp, InstVar(exp, name), vars) =
    let
        expCode = exprCode(cp, exp, vars)
        fieldId = findFieldRefByNT(cp, name)
        fieldCode = [180, 0, fieldId]
    in
        joinCodeParts(expCode, fieldCode)

exprCode(cp, StmtExprExpr(x), vars) = stmtExprCode(x, cp, vars, 1)

arithmeticOperator :: ([CP_Info], Expr, [(Type, String)]) -> [Int] 
arithmeticOperator(cp, Binary(rec, x, y), vars) = 
    let
        code1 = exprCode(cp, x, vars)
        code2 = exprCode(cp, y, vars)
        code = joinCodeParts(code1, code2)
        op = findBinaryOperator(rec)
    in
        joinCodeParts(code, [op])

conditionalOperator :: ([CP_Info], Expr, [(Type, String)]) -> [Int] 
conditionalOperator(cp, Binary(rec, TypedExpr(x, t1), TypedExpr(y, t2)), vars)
    | (rec == "&&") =
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            lengthAfterLoad1 = listLength(load2) + 9
            code1 = joinCodeParts(load1, [3, 159, 0, lengthAfterLoad1])
            code2 = joinCodeParts(code1, load2)
        in
            joinCodeParts(code2, [3, 159, 0, 7, 4, 167, 0, 4, 3]) --[load1, 3, 159, 0, lengthAfterLoad1, load2, 3, 159, 0, 7, 4, 167, 0, 4, 3]    
    | (rec == "||") = 
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            lengthAfterLoad1 = listLength(load2) + 11
            code1 = joinCodeParts(load1, [4, 159, 0, lengthAfterLoad1])
            code2 = joinCodeParts(code1, load2)
        in
            joinCodeParts(code2, [4, 159, 0, 7, 3, 167, 0, 4, 4]) --[load1, 4, 159, 0, lengthAfterLoad1, load2, 4, 159, 0, 7, 3, 167, 0, 4, 4]    
    | (rec == "==") =
        let
            load1 = exprCode(cp, TypedExpr(x, t1), vars)
            load2 = exprCode(cp, TypedExpr(y, t2), vars)
            loads = joinCodeParts(load1, load2)
        in
            if(t1 == "int" || t1 == "char" || t1 == "boolean") then
                joinCodeParts(loads, [159, 0, 7, 3, 167, 0, 4, 4])
            else
                joinCodeParts(loads, [165, 0, 7, 3, 167, 0, 4, 4])
    | (rec == "!=") =
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            loads = joinCodeParts(load1, load2)
        in
            if(t1 == "int" || t1 == "char" || t1 == "boolean") then
                joinCodeParts(loads, [160, 0, 7, 3, 167, 0, 4, 4])
            else
                joinCodeParts(loads, [166, 0, 7, 3, 167, 0, 4, 4])
    | (rec == ">") =
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            loads = joinCodeParts(load1, load2)
        in
            joinCodeParts(loads, [163, 0, 7, 3, 167, 0, 4, 4])
    | (rec == ">=") =
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            loads = joinCodeParts(load1, load2)
        in
            joinCodeParts(loads, [162, 0, 7, 3, 167, 0, 4, 4])
    | (rec == "<") =
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            loads = joinCodeParts(load1, load2)
        in
            joinCodeParts(loads, [161, 0, 7, 3, 167, 0, 4, 4])
    | (rec == "<=") =
        let
            load1 = exprCode(cp, x, vars)
            load2 = exprCode(cp, y, vars)
            loads = joinCodeParts(load1, load2)
        in
            joinCodeParts(loads, [164, 0, 7, 3, 167, 0, 4, 4])


findBinaryOperator :: String -> Int
findBinaryOperator("+") = 96
findBinaryOperator("-") = 100
findBinaryOperator("*") = 104
findBinaryOperator("/") = 108
findBinaryOperator("%") = 112
findBinaryOperator("&") = 126
findBinaryOperator("|") = 128
findBinaryOperator("^") = 130
findBinaryOperator("<<") = 120
findBinaryOperator(">>") = 122

findUnaryOperator :: String -> [Int]
findUnaryOperator("++") = [4, 96]
findUnaryOperator("--") = [4, 100]
findUnaryOperator("!") = [4, 130]
indUnaryOperator("^") = [4, 130]

localFieldVarCode :: ([CP_Info], Maybe (Int, Type), String) -> [Int]
localFieldVarCode(_, Just (id, t), _)
    | id > 3 = generalLoad(id, t)
    | otherwise = specificLoad(id, t)
localFieldVarCode(cp, Nothing, name) = 
    let
        id = findFieldRefByNT(cp, name)
    in
        [42, 180, 0, id]

generalLoad :: (Int, Type) -> [Int]
generalLoad(id, "int") = [21, id]
generalLoad(id, "boolean") = [21, id]
generalLoad(id, "char") = [21, id]
generalLoad(id, _) = [25, id]

specificLoad :: (Int, Type) -> [Int]
specificLoad(id, "int") = [26 + id]
specificLoad(id, "boolean") = [26 + id]
specificLoad(id, "char") = [26 + id]
specificLoad(id, _) = [42 + id]

searchLocalVar :: (String, [(Type, String)],  Int) -> Maybe (Int, Type)
searchLocalVar(_, [], _) = Nothing
searchLocalVar(name, ((t, c):cs), id)
    | name == c = Just (id, t)
    | otherwise = searchLocalVar(name, cs, id+1)


stmtExprCode :: (StmtExpr, [CP_Info], [(Type, String)], Int) -> [Int]
stmtExprCode(Assign(name, exp), cp, vars, stelle) = 
    let 
        var = searchLocalVar(name, vars, 1)
    in
        assignCode(var, name, exp, cp, vars)

stmtExprCode(TypedStmtExpr(MethodCall(TypedExpr(exp, t), name, params), retType), cp, vars, stelle) =
    let
        expCode = exprCode(cp, TypedExpr(exp, t), vars)
        loads = loadParamsCode(cp, vars, params)
        attributeList = getAttributeList(params)
        methodId = findMethodRef(cp, name, attributeList, retType, t)
        invokeCode = [182, 0, methodId]
        code = joinCodeParts(expCode, loads)
    in
        joinCodeParts(code, invokeCode)
stmtExprCode(TypedStmtExpr(x,y), cp, vars, stelle) = stmtExprCode(x, cp, vars, stelle)
stmtExprCode(New(name, params), cp, vars, stelle) =
    let
        classId = findClassIndex(cp, name)
        newCode = [187, 0, classId, 89]
        loads = loadParamsCode(cp, vars, params)
        attributeList = getAttributeList(params)
        methodID = findMethodRef(cp, "<init>", attributeList, "void", name)                    
        invokeCode = [183, 0, methodID]
        code = joinCodeParts(newCode, loads)
    in
        joinCodeParts(code, invokeCode)



getAttributeList :: [Expr] -> [String]
getAttributeList((TypedExpr(x,y) : ts)) = (y : getAttributeList(ts))
getAttributeList([]) = []

assignCode :: (Maybe (Int, Type), String, Expr, [CP_Info], [(Type, String)]) -> [Int]
assignCode (Nothing, name, exp, cp, vars) =
    let
        id = findFieldRefByNT(cp, name)
        storeCode = [181, 0, id]
        expCode = exprCode(cp, exp, vars)
        code = joinCodeParts(expCode, storeCode)
    in
        (42 : code)
assignCode(Just (id, typ), name, exp, cp, vars) = 
    let
        store = storeCode(id, typ)
        expCode = exprCode(cp, exp, vars)
    in 
        joinCodeParts(expCode, store)

storeCode :: (Int, Type) -> [Int]
storeCode(id, "int")
    | id > 3 = [54, id]
    | otherwise = [59+id]
storeCode(id, "boolean")
    | id > 3 = [54, id]
    | otherwise = [59+id]
storeCode(id, "char")
    | id > 3 = [54, id]
    | otherwise = [59+id]
storeCode(id, _)
    | id > 3 = [58, id]
    | otherwise = [75+id]

loadParamsCode :: ([CP_Info],  [(Type, String)], [Expr]) -> [Int]
loadParamsCode(_, _, []) = []
loadParamsCode(cp, vars, (c : cs)) =
    let
        buffer = loadParamsCode(cp, vars, cs)
        code = exprCode(cp, c, vars)
    in
        joinCodeParts(code, buffer)
