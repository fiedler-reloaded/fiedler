module TestTools where

import Control.Exception
import Test.HUnit
import Control.Monad
import GHC.Natural
import ClassFormat
import SemantikCheck
import ClassFile
import Parser

{-
    TestTools.hs
    Autor: Dominik Sauerer
    Beschreibung: Verschiedene Hilfsmethoden für den Einsatz in den Testfällen

    Hinweis: Die Ordner Data und Test enthalten weitere benötigte (fremde) Bibliotheken für die Tests: HUnit und weitere Datenstrukturen
-}

data MyException = ThisException | ThatException
    deriving (Show)

instance Exception MyException


{-
    myTestCorrected/myTestCorrectedTest
    Autor: Dominik Sauerer
    Beschreibung: Überprüft eine mitgegebene Funktion auf einen Error und fängt diesen ab.
    Wirft selbst eine nicht aufgefangene Exception, wenn der Error nicht auftritt
    Params:
        f => die durchzuführende Funktion
-}
myTestCorrected f = let
        x = Control.Exception.catch (action ) handler
        handler :: Control.Exception.ErrorCall -> IO()
        handler _ = putStrLn $ "\nSuccess"
        action = do
            print f
            throw ThisException
    in
        x

myTestCorrectedTest f = TestCase $ (myTestCorrected f)


{-
    Überschreibung von Eq des ClassFile
    Autor: Dominik Sauerer
    Beschreibung: Überschreibt Eq-Methode des ClassFiles, um diese in Tests effizient zu vergleichen.
    Folgende Aspekte werden dabei automatisch geprüft:
    - Vorhandensein der Magic-Konstante und richtige Versionsnummern
    - Anzahl von Methoden, Interfaces, Feldern und Konstantenpooleinträgen
    - Überprüfung, ob this und super auf wirkliche Class_Info-Objekte im Konstantenpool zeigen
    - Prüfbare Parameter bei Field-Infos sowie, ob Konstantenpoolreferenzen tatsächlich auf UT8-Info-Objekte zeigen
    Folgende Aspekte werden NICHT automatisiert geprüft und müssen manuell getestet werden:
    - Assemblercode, da sich dieser in Musterlösung und eigenem Classfile stark unterscheidet durch fehlende Compileroptimierungen
    und zudem automatisch keine semantische Prüfung möglich ist.
    - Der genaue Konstantenpool und die Referenzen untereinander
-}
instance Eq ClassFile where
    (ClassFile mag min max countcp arrcp acfg this super ci ai cf af cm am ca aa) ==
        (ClassFile mag1 min1 max1 countcp1 arrcp1 acfg1 this1 super1 ci1 ai1 cf1 af1 cm1 am1 ca1 aa1) = 
            and [mag == mag1, min == min1, max == max1, countcp >= countcp1, acfg == acfg1, findThisClassInCP this arrcp 1, 
            findSuperInCP super arrcp 1, ci == ci1, cf == cf1, cm == cm1, ca == ca1, 
            compareFieldInfos af af1 arrcp, compareMethodInfos(am, am1, arrcp)]


compareFieldInfos :: [Field_Info] -> [Field_Info] -> CP_Infos -> Bool
compareFieldInfos [] [] _ = True
compareFieldInfos (f : []) (fs : []) cp =
    if compareFieldInfo f fs cp then
        True
    else
        False
compareFieldInfos (f : fs) (fss : fsss) cp = 
    if compareFieldInfo f fss cp then
        compareFieldInfos fs fsss cp
    else
        False

compareFieldInfo :: Field_Info -> Field_Info -> CP_Infos -> Bool
compareFieldInfo (Field_Info a b c d e) (Field_Info a1 b1 c1 d1 e1) cp
    | (a == a1 && d == d1 && b /= -1 && b1 /= -1 && c /= -1 && c1 /= -1 &&
        findUTF8InfoInCP(b - 1, cp) && findUTF8InfoInCP(c - 1, cp)) = True
    | otherwise = False 


compareMethodInfos :: ([Method_Info], [Method_Info], CP_Infos) -> Bool
compareMethodInfos([],[],_) = True
compareMethodInfos((f : fs), (g : gs), cp) =
    if compareMethodInfo(f, g, cp) then
        compareMethodInfos(fs, gs, cp)
    else
        False
compareMethodInfos(_, _, _) = False

compareMethodInfo :: (Method_Info, Method_Info, CP_Infos) -> Bool
compareMethodInfo(Method_Info af_mi index_name_mi index_descr_mi tam_mi array_attr_mi , Method_Info af_mi1 index_name_mi1 index_descr_mi1 tam_mi1 array_attr_mi1, cp) =
    if af_mi == af_mi1 && tam_mi == tam_mi1 && findUTF8InfoInCP(index_name_mi - 1, cp) && findUTF8InfoInCP(index_descr_mi - 1, cp) then
        True
    else
        False

{-
    findThisClassInCP
    Autor: Dominik Sauerer
    Beschreibung: Überprüft, ob die bei ThisClass angegebene Konstantenpool-Referenz tatsächlich auf ein Class_Info-Objekt zeigt.
    Params:
        ThisClass => ThisClass-Objekt, dessen Gültigkeit überprüft werden soll
        CP_Infos => Konstantenpool
        Int => Counter für die Methode, um die Einträge durchzuzählen (beim ersten Aufruf 1 mitgeben)
    Returns: true, wenn ThisClass auf ein Class_Info-Objekt zeigt; false, sonst
-}
findThisClassInCP :: ThisClass -> CP_Infos -> Int -> Bool
findThisClassInCP _ [] _ = False
findThisClassInCP (ThisClass(x)) (Class_Info a b c : cs) (count)
    | (x == count) = True
    | otherwise = findThisClassInCP (ThisClass(x)) cs (count + 1)
findThisClassInCP (ThisClass(x)) (_ : cs) count = findThisClassInCP (ThisClass(x)) cs (count + 1)

{-
    findFieldInCP
    Autor: Dominik Sauerer
    Beschreibung: Überprüft, ob der angegebene Index tatsächlich auf ein FieldRef_Info-Objekt zeigt.
    Params:
        toFind => Index des Eintrags im Konstantenpool
        CP_Infos => Konstantenpool
    Returns: true, wenn der Index auf ein Field_Ref-Info-Objekt zeigt; false, sonst
-}
findFieldInCP :: (Int, CP_Infos) -> Bool
findFieldInCP(_, []) = False
findFieldInCP(toFind, (FieldRef_Info TagFieldRef b c d : cp))
    | (toFind == 0) = True
    | otherwise = findFieldInCP(toFind - 1, cp)
findFieldInCP(toFind, (c:cp)) = findFieldInCP(toFind - 1, cp)


{-
    findUTF8InfoInCP
    Autor: Dominik Sauerer
    Beschreibung: Überprüft, ob der angegebene Index tatsächlich auf ein UTF8-Info-Objekt zeigt.
    Params:
        toFind => Index des Eintrags im Konstantenpool
        CP_Infos => Konstantenpool
    Returns: true, wenn der Index auf ein UTF8-Info-Objekt zeigt; false, sonst
-}
findUTF8InfoInCP :: (Int, CP_Infos) -> Bool
findUTF8InfoInCP(_, []) = False
findUTF8InfoInCP(toFind, (Utf8_Info TagUtf8 b c d : cp))
    | (toFind == 0) = True
    | otherwise = findUTF8InfoInCP(toFind - 1, cp)
findUTF8InfoInCP(toFind, (c:cp)) = findUTF8InfoInCP(toFind - 1, cp)


{-
    findSuperInCP
    Autor: Dominik Sauerer
    Beschreibung: Überprüft, ob die bei SuperClass angegebene Konstantenpool-Referenz tatsächlich auf ein Class_Info-Objekt zeigt.
    Params:
        SuperClass => SuperClass-Objekt, dessen Gültigkeit überprüft werden soll
        CP_Infos => Konstantenpool
        Int => Counter für die Methode, um die Einträge durchzuzählen (beim ersten Aufruf 1 mitgeben)
    Returns: true, wenn SuperClass auf ein Class_Info-Objekt zeigt; false, sonst
-}
findSuperInCP :: SuperClass -> CP_Infos -> Int -> Bool
findSuperInCP (_) ([]) _ = False
findSuperInCP (SuperClass(x)) (Class_Info a b c : cs) count
    | (x == count) = True
    | otherwise = findSuperInCP (SuperClass(x)) cs (count + 1)
findSuperInCP (SuperClass(x)) (_ : cs) count = findSuperInCP (SuperClass(x)) cs (count + 1)


compiler :: String -> [ClassFile]
compiler = createPrg . typecheckPrg . parser
