public class InstanceVars {
    public boolean method() {
        Helper helper;
        helper = new Helper(5, true);
        return helper.z;
    }
}

private class Helper {
    public boolean z;
    public int xy;
    public Helper(int x, boolean a) {
        z = a;
        xy = x;
    }
}