public class MethodEndAfterIf {
    public void method(char x) {
        if(x == '3') {
            x = 'p';
        } else {
            x = 'o';
        }
    }
    public char otherMethod(char x) {
        if(x == '3') {
            return 'p';
        } else {
            return 'o';
        }
    }
}