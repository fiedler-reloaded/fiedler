public class BreakContinue {
    public void doBreak() {
        boolean x;
        x = true;
        while(x) {
            break;
        }
    }
    public void doContinue() {
        int x;
        x = 0;
        while(x < 2) {
            x = x + 1;
            continue;
        }
    }
}