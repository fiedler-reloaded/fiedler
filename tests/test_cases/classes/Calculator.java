public class Calculator {
    public int moduloCalculator_Java(int x, int y) {
        return x % y;
    }
    public int moduloCalculator_while(int x, int y) {
        while(x > 0) {
            x = x - y;
        }
        if(x == 0) {
            return x;
        }
        x = x + y;
        return x;
    }
    public int add(int a, int b) {
        return a + b;
    }
    public int sub(int a, int b) {
        return a - b;
    }
    public int mul(int a, int b) {
        return a * b;
    }
    public int div(int a, int b) {
        return a / b;
    }

    public int calculateDigitSum(int x) {
        int result;
        result = 0;
        while(x > 0) {
            if(x < 0) {
                return -1;
            }
            int rest;
            rest = this.moduloCalculator_while(x, 10);
            result = result + rest;
            x = this.div(x, 10);
        }
        return result;
    }

    public int calculateFibonacci_iterative(int x) {
        FibonacciCalculator calc;
        calc = new FibonacciCalculator();
        return calc.calculateFibonacci_iterative(x);
    }

    public int calculateFibonacci_recursive(int x) {
        FibonacciCalculator calc;
        calc = new FibonacciCalculator();
        return calc.calculateFibonacci_recursive(x);
    }
}

public class FibonacciCalculator {
    public int calculateFibonacci_iterative(int i) {
        int x;
        int y;
        int result;
        x = 0;
        y = 1;
        if(i < -1 || i == 0) {
            return x;
        }
        if(i == 1) {
            return y;
        }
        i = i - 1;
        while(true) {
            result = x + y;
            x = y;
            y = result;
            i = i - 1;
            if(i <= 0) {
                break;
            }
        }
        return result;
    }

    private int fibonacci_rek(int x, int y, int z) {
        int a;
        a = x + y;
        z = z--;
        if(z <= 0) {
            return a;
        }else{
            return this.fibonacci_rek(y, a, z);
        }
    }
    
    public int calculateFibonacci_recursive(int i) {
        if(i == 0) {
            return 0;
        }
        if(i == 1) {
            return 1;
        }
        i = i - 1;
        return this.fibonacci_rek(0, 1, i);
    }
    
}