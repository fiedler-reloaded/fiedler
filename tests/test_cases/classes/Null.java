public class Null {
    public Helper helper;
    public int x;
    public void correctNull() {
        x = 4;
        helper = null;
    }
    public Helper returnNull() {
        return null;
    }
    public int nullInCompare() {
        int x;
        if(helper == null) {
            x = 5;
        }
        if (helper != null) {
            x = 6;
        }
        return x;
    }
    public void falseNull() {
        x = null;
    }
}