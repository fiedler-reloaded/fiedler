public class InstanceCalls {
    private HelperClass helper;
    public InstanceCalls() {
        helper = new HelperClass();
    }
    public void method1() {
        helper.doSomething();
    }
    public void method2() {
        helper.doAnotherThing(5, "hello");
    }
    protected void method3() {
        int x;
        x = helper.doAThirdThing();
    }
}

public class HelperClass {
    public void doSomething() {

    }
    public void doAnotherThing(int x, String y) {
        
    }
    public int doAThirdThing() {
        return 10;
    }
}