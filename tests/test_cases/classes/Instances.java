public class Instances {
    public void method() {
        HelperClass helper;
        helper = new HelperClass();
    }

    public void otherMethod() {
        int x;
        x = 5;
        OtherHelperClass helperClass;
        helperClass = new OtherHelperClass(x);
    }
}

public class HelperClass {

}

public class OtherHelperClass {
    private int value;
    public OtherHelperClass(int x) {
        value = x;
    }
}