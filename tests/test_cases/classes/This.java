public class ThisCl {
    private ThisCl instance;
    public void method_thisAssign() {
        instance = this;
    }
    public void assign(ThisCl x) {
        instance = x;
    }
    public void method_thisCall() {
        this.assign(this);
    }
}