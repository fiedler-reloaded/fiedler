public class MethodsExist {
    public void test1() {
        Helper helper;
        helper.method();
        helper.method2(3);
        int abc;
        abc = helper.method3();
        abc = helper.xyz;
    }
    public void test2_NotWorking() {
        Helper helper;
        helper.xy();
    }
    public void test3_NotWorking() {
        Helper helper;
        helper.method(1);
    }
    public void test4_NotWorking() {
        Helper helper;
        helper.method2("Hallo");
    }
    public void test5_NotWorking() {
        Helper helper;
        char z;
        z = helper.method3();
    }
}

public class Helper {
    public int xyz;
    public void method() {

    }
    public void method2(int x) {

    }
    public int method3() {
        return 0;
    }
}