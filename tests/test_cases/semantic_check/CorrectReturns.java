public class CorrectReturns {
    String myString;
    public int a() {
        return 3;
    }
    public int b() {
        int x;
        x = 3;
        return x;
    }
    public String c() {
        return "Hallo";
    }
    private String e() {
        return myString;
    }
    public boolean d() {
        return true;
    }
}

public class FalseReturn1 {
    public int a() {
        return true;
    }
}

public class FalseReturn2 {
    public boolean a() {
        int x;
        x = 4;
        if(x < 5) {
            return true;
        }
    }
}

public class FalseReturn3 {
    public String a() {
        int x;
        x = 4;
    }
}

public class FalseReturn4 {
    public String a() {
        return 'x';
    }
}

public class FalseReturn5 {
    public boolean x;
    public String a() {
        if(x) {
            return "Hi";
        }
    }
}

public class FalseReturn6 {
    public boolean x;
    public String a() {
        if(x) {
            return;
        }
        return "Hi";
    }
}