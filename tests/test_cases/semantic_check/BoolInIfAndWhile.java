public class BoolInIfAndWhile {
    public void correctIfs() {
        if(true) {

        }
        boolean x;
        x = false;
        if(x) {

        }
        if(!x) {

        }
        if(5 == 5) {

        }
    }
    public void correctWhiles() {
        boolean x;
        x = false;
        while(x) {

        }
        while(!x) {

        }
        while(5 == 5) {
            
        }
    }
    public void falseIf1() {
        if(5) {

        }
    }
    public void falseIf2() {
        char z;
        z = 'x';
        if(z) {

        }
    }
    public void falseWhile1() {
        while(x = 5) {

        }
    }
    public void falseWhile2() {
        String x;
        while(x) {
            
        }
    }
    public void falseWhile3() {
        while(1) {
            
        }
    }
}