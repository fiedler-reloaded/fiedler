import Jvm.Data.ClassFormat
import Jvm.BinaryClass
import Data.ByteString.Lazy.Internal

main = do
	cf <- (decodeClassFile "Calculations.class")
	print cf 
	encodeClassFile "Calculations.class" cf
