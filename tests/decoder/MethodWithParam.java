public class MethodWithParam {
    protected boolean example(int i) {
        return true;
    }
    
    public int another(boolean x, int v) {
        return v;
    }
}