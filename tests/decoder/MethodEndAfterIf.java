public class MethodEndAfterIf {
    public char otherMethod(char x) {
        if(x == '3') {
            return 'p';
        } else {
            return 'o';
        }
    }
}