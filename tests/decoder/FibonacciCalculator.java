public class FibonacciCalculator {
    public int calculateFibonacci_iterative(int i) {
        int x;
        int y;
        int result;
        x = 0;
        y = 1;
        if(i < -1) {
            return x;
        }
        if(i == 0) {
            return x;
        }
        if(i == 1) {
            return y;
        }
        i = i - 1;
        while(true) {
            result = x + y;
            x = y;
            y = result;
            i = i - 1;
            if(i <= 0) {
                break;
            }
        }
        return result;
    }

    private int fibonacci_rek(int x, int y, int z) {
        int a;
        a = x + y;
        z = z--;
        if(z <= 0) {
            return a;
        }else{
            return this.fibonacci_rek(y, a, z);
        }
    }
    
    public int calculateFibonacci_recursive(int i) {
        if(i == 0) {
            return 0;
        }
        if(i == 1) {
            return 1;
        }
        i = i - 1;
        return this.fibonacci_rek(0, 1, i);
    }
    
}