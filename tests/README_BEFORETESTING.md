# README zur Testsuite
Autor: Dominik Sauerer  


## A. Inhaltsverzeichnis des Testordners:
- /bin: Ausgabeort der ausführbaren Testdatei sowie aller o-Dateien
- /include: Selbst geschriebene sowie recherchierte Hilfsmethoden für Tests
- /test_cases: Getestete Java-Klassen ohne weiteren Test-Code
- /test_files: Test Cases inklusive Test-Code mit Soll-Lösungen
- Makefile: Makefile zum Bauen der Testsuite
- Tests.hs: Ausgangsdatei mit main-Methode für Tests  
  
---
---
---
---
---  
  



  

## B. Anleitung für Build der Testsuite:
1. in Tests.hs => Bei allen durchzuführenden Tests Kommentare in main-Methode entfernen
2. make ausführen => Output liegt in bin (Vorgang ist plattformunabhängig)
3. Testsuite ausführen mit bin/Tests.exe bzw. bin/Tests
4. Zum Leeren des bin-Ordners: make del ausführen (nur Windows)

---
---
---
---
---  




## C. Aufbau eines Testcases:  
In jeder FiedlerTest*-Datei in test_files befinden sich 1-7 verschiedene Tests.
Ein Testcase besteht immer aus folgenden Elementen:
- TESTNAMEClass: Der Input in den Test in Form einer kleinen Java-Klasse
- TESTNAME_AbsSyntaxResult: Die erwartete ungetypte abstrakte Syntax
- TESTNAME_AbsSyntaxTypedResult: Die erwartete getypte abstrakte Syntax
- TESTNAME_BytecodeResult: Referenz-Classfile für diesen Test
Danach folgen die Test-Case-Definition für HUnit und die doTests-Funktion, die alle Testcases der Datei ausführt  

Kleiner Hinweis: nicht jeder Test hat je nach Testumfang alle dieser Elemente, z.B. Semantik-Check-Tests besitzen nur
eine abstrakte Syntax und evtl. eine getypte abstrakte Syntax.

---
---
---
---
---  



## D. Infos zum Testoutput:
Jeder Test erzeugt einen Konsolenoutput, der wie folgt aussieht:


z.B.  
```
"Testing Correct Returns (Semantik check test)"  
    ### Error in:   1  
    semantic check error
    CallStack (from HasCallStack):  
    error, called at ..\SemantikCheck.hs:230:7 in main:SemantikCheck  
    Cases: 14  Tried: 3  Errors: 1  Failures: 0  
```

Erläuterung:
- Am Anfang steht der Testcase, der im Folgenden überprüft wird (hier: Correct Returns, Semnatic check test)
- Wenn es Fehler gibt, werden diese mit der erhaltenen Exception/Error bzw. bei einem Testfehlschlag mit dem Soll-Ergebnis
    eingeblendet. Die Zahl (hier: 1) bezieht sich dabei auf die Nummer des Test cases in der allTests-Liste der jeweiligen Testdatei und bezieht sich somit auf einen einzelnen Compilerteil.
- Am Ende des Tests wird die Teststatistik eingeblendet:
    - Cases: Anzahl durchgeführter Vergleiche insgesamt
    - Tried: Versuchte Ausführungen/Vergleiche
    - Errors: Anzahl Fälle, bei denen Errors/Exceptions aufgetreten sind
    - Failures: Anzahl Fälle, die fehlschlugen, weil die Ergebnisse nicht mit dem Soll übereinstimmten
=> Somit: Anzahl erfolgreicher Tests = Tried - (Errors + Failures)  


Weiteres Beispiel:
```
"Testcase 12: Testing calls of attributes and methods of instances (FiedlerTestInstanceCalls.hs)"
"Test 12.1: method call without return or params"
Cases: 4  Tried: 4  Errors: 0  Failures: 0
```
In diesem Fall gab es keine Fehler im Test, alle vier Schritte des Tests (Parser, Semantikcheck, Classfile, gesamter Compiler) wurden erfolgreich ausgeführt.

---
---
---
---
---  





## E. Testverzeichnis:  
     
1. Leere Klasse (FiedlerTestEmptyJava.hs)
2. Attribute ohne Methoden (FiedlerTestAttributes.hs)
3. Einfache Methode, die 0 (int) zurückgibt (FiedlerTestIntMethod.hs)
4. Einfache void-Methode (FiedlerTestVoidMethod.hs)
5. Methoden mit Parametern (FiedlerTestMethodWithParam.hs)
6. Mehrere Klassen in einer Datei (FiedlerTestMultipleClasses.hs)
7. Test von if-else (FiedlerTestIfElse.hs)
8. while-Schleifen (FiedlerTestWhile.hs)
9. Zuweisungen (FiedlerTestAssignments.hs)  
    9.1. Zuweisungen auf Methoden-lokale Variablen (test_assignmentsMethod)  
    9.2 Zuweisungen auf Felder (test_assignmentsOtherMethod)  
10. Berechnungen (FiedlerTestCalculations.hs)  
    10.1 Addition und Zuweisung (test_calculationsMath)  
    10.2 Test aller möglichen Rechenoperatoren (test_calculationsMath2)  
    10.3 Rechnungen mit Klammern (test_calculationsMathWithBrackets)  
    10.4 Rechnungen mit bitwise Operatoren (and, or...) (test_calculationsBitwise)  
11. Objekte instanziieren (FiedlerTestInstances.hs)  
    11.1 Objekt instanziieren ohne Parameter (test_instancesMethod)  
    11.2 Objekt instanziieren mit Parameter (test_instancesOtherMethod)  
12. Methoden aufrufen von Instanzen (FiedlerTestInstanceCalls.hs)  
    12.1 ...Methode ohne Parameter, ohne Rückgabewert (test_instanceCallMethod1)  
    12.2 ...Methode mit Parametern, ohne Rückgabewert (test_instanceCallMethod2)  
    12.3 ...Methode mit Rückgabewert (test_instanceCallMethod3)  
13. Felder von Instanzen direkt lesen/schreiben (FiedlerTestInstanceVars.hs)
14. eigener Konstruktor (FiedlerTestConstructor.hs)
15. Break/Continue in Schleifen (FiedlerTestBreakContinue.hs)  
    15.1 break (test_break)  
    15.2 continue (test_continue)  
16. Test des Methodenendes nach einem if-Konstrukt (FiedlerTestMethodEndAfterIf.hs)  
    16.1 void-Methode (test_methodEndAfterIf)  
    16.2 Methode mit Rückgabewert (test_otherMethodEndAfterIf)
17. Korrekte Verwendung von Java-Null möglich (FiedlerTestNull.hs)  
    17.1 Korrekte Zuweisung von null auf komplexen Typ (test_correctNull)  
    17.2 Falsche Zuweisung von null auf primitiven Typ (test_falseNull)  
    17.3 Test von null als Rückgabewerte von Methoden (test_returnNull)  
    17.4 Test von null als Vergleichswerte  
18. Viele Parameter in einer Methode (FiedlerTestManyParams.hs)
19. This (FiedlerTestThis.hs)  
    19.1 this in Zuweisungen (test_thisThisAssign)  
    19.2 this in Methodenaufrufen (test_thisThisCall)  
20. Überprüfen auf richtigen Zugriffsmodifier (FiedlerTestAccessFlags.hs)  
    20.1 Zugriff möglich (public) (test_accessFlagsMethod1)  
    20.2 Zugriff nicht möglich (private) (test_accessFlagsMethod2NotWorking)  
    20.3 Zugriff nicht möglich (protected) (test_accessFlagsMethod3NotWorking)  
21. Überprüfen auf richtigen Zugriffsmodifier bei Methoden (FiedlerTestAccessFlagsMethods.hs)  
    21.1 Zugriff möglich (public) (test_accessFlagsMethodsTest1)  
    21.2 Zugriff nicht möglich (private) (test_accessFlagsMethodsTest2NotWorking)  
    21.3 Zugriff nicht möglich (protected) (test_accessFlagsMethodsTest3NotWorking)  

    ### Semantik-Check-Tests:  
22. Überprüfen auf richtige Return-Werte von Methoden (FiedlerSemCheckTestCorrectReturns.hs)  
    22.1 korrekte Return-Typen (int, String, boolean) (test_correctReturns)  
    22.2 falscher Rückgabetyp (boolean statt int) (test_falseReturnsOne)  
    22.3 gar kein Rückgabewert (void statt boolean) (test_falseReturnsTwo)  
    22.4 gar kein Rückgabewert (void statt String) (test_falseReturnsThree)  
    22.5 falscher Rückgabetyp (char statt String) (test_falseReturnsFour)  
    22.6 nicht überall richtiger Return-Typ auf jedem Ausführungsstrang   (test_falseReturnsFive)
    22.7 nicht überall richtiger Return-Typ auf jedem Ausführungsstrang   (test_falseReturnsSix)  
23. Überprüfen auf richtige Typen bei Zuweisungen (FiedlerSemCheckTestCorrectAssignments.hs)  
    23.1 Korrekte Zuweisungen (test_correctAssignment)  
    23.2 Falsche Zuweisung (Boolean statt Integer) (test_incorrectAssignment1)  
    23.3 Falsche Zuweisung (Integer statt Boolean) (test_incorrectAssignment2)  
    23.4 Falsche Zuweisung (String statt char) (test_incorrectAssignment3)  
    23.5 Falsche Zuweisung (Integer statt komplexer Typ) (test_incorrectAssignment4)  
    23.6 Falsche Zuweisung (Char statt String) (test_incorrectAssignment5)
24. Semantikcheck-Test auf "falsche" Konstruktoren (FiedlerSemCheckTestFalseConstructor.hs)
25. Semantikcheck-Test auf nicht existierende/falsch angegebene Methoden (FiedlerSemCheckTestMethodsExist.hs)  
    25.1 korrekte Methodenaufrufe (test_methodsExistTest1)  
    25.2 nicht existierende Methode (test_methodsExistTest2NotWorking)  
    25.3 Methodenaufruf mit falschen Parametern (test_methodsExistTest3NotWorking)  
    25.4 Methodenaufruf mit falschen Parametern (test_methodsExistTest4NotWorking)  
    25.5 Methodenaufruf mit falschen Parametern (test_methodsExistTest5NotWorking)  
26. Semantikcheck-Test auf boolean-Expressions in if/while (FiedlerSemCheckTestBoolInIfAndWhile.hs)  
    26.1 richtige boolean-Ausdrücke in ifs (test_correctIfs)  
    26.2 richtige boolean-Ausdrücke in whiles (test_correctWhiles)  
    26.3 int in if (test_falseIf1)  
    26.4 char in if (test_falseIf2)  
    26.5 Assignment in while (test_falseWhile1)  
    26.6 String in while (test_falseWhile2)  
    26.7 in in while (test_falseWhile3)  

    ### Parser-Tests:
27. Erlaubte Identifier-Bezeichnungen (FiedlerTestIdentifier.hs)  
    27.1 korrekte Identifier (nach Java-Definition) (test_correctIdentifier_AbsSyntax)  
    27.2 verbotene Identifier (nach Java-Definition) (test_falseIdentifier_AbsSyntax)  

= insgesamt 66 Tests in 27 Testdateien