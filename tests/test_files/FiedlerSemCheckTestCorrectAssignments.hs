module FiedlerSemCheckTestCorrectAssignments where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile
import Control.Exception

import TestTools

{-
        Testcase 23
        Autor: Dominik Sauerer
        Beschreibung: Semantikcheck: Test von Typkorrektheit bei Zuweisungen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 23.1
        Autor: Dominik Sauerer
        Beschreibung: korrekte Zuweisungen => Klasse soll kompilieren
        Testumfang: Parser, Semantikcheck
-}

correctAssignmentClass = "public class CorrectAssignments {\
  \  public Helper helper;\
  \  public void correctAssignment() {\
 \       int x;\
 \       x = 5;\
  \      char y;\
 \       y = '7';\
  \      boolean z;\
  \      z = false;\
  \      z = true;\
 \       Helper help;\
\        help = null;\
 \       help = helper;\
  \      String abc;\
 \       abc = \"Hallo\";\
 \   }\
\}"

correctAssignment_AbsSyntaxResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "correctAssignment", [], Block([
        LocalVarDecl("int", "x"),
        StmtExprStmt(Assign("x", Integer(5))),
        LocalVarDecl("char", "y"),
        StmtExprStmt(Assign("y", Char('7'))),
        LocalVarDecl("boolean", "z"),
        StmtExprStmt(Assign("z", Bool(False))),
        StmtExprStmt(Assign("z", Bool(True))),
        LocalVarDecl("Helper", "help"),
        StmtExprStmt(Assign("help", Jnull)),
        StmtExprStmt(Assign("help", LocalOrFieldVar("helper"))),
        LocalVarDecl("String", "abc"),
        StmtExprStmt(Assign("abc", String("Hallo")))
    ]))
    ]))]

correctAssignment_AbsSyntaxTypedResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "correctAssignment", [], TypedStmt(Block([
        TypedStmt(LocalVarDecl("int", "x"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(5), "int")), "void")), "void"),
        TypedStmt(LocalVarDecl("char", "y"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Char('7'), "char")), "void")), "void"),
        TypedStmt(LocalVarDecl("boolean", "z"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("z", TypedExpr(Bool(False), "boolean")), "void")), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("z", TypedExpr(Bool(True), "boolean")), "void")), "void"),
        TypedStmt(LocalVarDecl("Helper", "help"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("help", TypedExpr(Jnull, "NullType")), "void")), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("help", TypedExpr(LocalOrFieldVar("helper"), "Helper")), "void")), "void"),
        TypedStmt(LocalVarDecl("String", "abc"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("abc", TypedExpr(String("Hallo"), "String")), "void")), "void")
    ]), "void"))
    ]))]

test_correctAssignment_AbsSyntax = TestCase (assertEqual "Abstract Syntax" correctAssignment_AbsSyntaxResult (parser(correctAssignmentClass)))
test_correctAssignment_SemanticCheck = TestCase (assertEqual "Semantic check" correctAssignment_AbsSyntaxTypedResult (typecheckPrg(correctAssignment_AbsSyntaxResult)))






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 23.2
        Autor: Dominik Sauerer
        Beschreibung: falsche Zuweisung (boolean statt int)
        Testumfang: Parser, Semantikcheck
-}

incorrectAssignment1Class = "public class CorrectAssignments {\
 \   public Helper helper;\
 \   public void incorrectAssignment1() {\
 \       int x;\
  \      x = true;\
  \  }\
\}"

incorrectAssignment1_AbsSyntaxResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "incorrectAssignment1", [], Block([
        LocalVarDecl("int", "x"),
        StmtExprStmt(Assign("x", Bool(True)))
    ]))
    ]))]

test_incorrectAssignment1_AbsSyntax = TestCase (assertEqual "Abstract Syntax" incorrectAssignment1_AbsSyntaxResult (parser(incorrectAssignment1Class)))







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 23.3
        Autor: Dominik Sauerer
        Beschreibung: falsche Zuweisung (int statt boolean)
        Testumfang: Parser, Semantikcheck
-}

incorrectAssignment2Class = "public class CorrectAssignments {\
  \  public Helper helper;\
  \  public void incorrectAssignment2() {\
  \      boolean x;\
  \      x = 3;\
  \  }\
  \}"

incorrectAssignment2_AbsSyntaxResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "incorrectAssignment2", [], Block([
        LocalVarDecl("boolean", "x"),
        StmtExprStmt(Assign("x", Integer(3)))
    ]))
    ]))]

test_incorrectAssignment2_AbsSyntax = TestCase (assertEqual "Abstract Syntax" incorrectAssignment2_AbsSyntaxResult (parser(incorrectAssignment2Class)))





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 23.4
        Autor: Dominik Sauerer
        Beschreibung: falsche Zuweisung (String statt char)
        Testumfang: Parser, Semantikcheck
-}

incorrectAssignment3Class = "public class CorrectAssignments {\
 \   public Helper helper;\
 \   public void incorrectAssignment3() {\
  \      char x;\
  \      x = \"Hallo\";\
  \  }\
 \ }"

incorrectAssignment3_AbsSyntaxResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "incorrectAssignment3", [], Block([
        LocalVarDecl("char", "x"),
        StmtExprStmt(Assign("x", String("Hallo")))
    ]))
    ]))]


test_incorrectAssignment3_AbsSyntax = TestCase (assertEqual "Abstract Syntax" incorrectAssignment3_AbsSyntaxResult (parser(incorrectAssignment3Class)))







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 23.5
        Autor: Dominik Sauerer
        Beschreibung: falsche Zuweisung (int statt komplexem Typ)
        Testumfang: Parser, Semantikcheck
-}

incorrectAssignment4Class = "public class CorrectAssignments {\
  \  public Helper helper;\
 \   public void incorrectAssignment4() {\
  \      Helper x;\
    \    x = 42;\
    \}\
\}"

incorrectAssignment4_AbsSyntaxResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "incorrectAssignment4", [], Block([
        LocalVarDecl("Helper", "x"),
        StmtExprStmt(Assign("x", Integer(42)))
    ]))
    ]))]

test_incorrectAssignment4_AbsSyntax = TestCase (assertEqual "Abstract Syntax" incorrectAssignment4_AbsSyntaxResult (parser(incorrectAssignment4Class)))






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 23.6
        Autor: Dominik Sauerer
        Beschreibung: falsche Zuweisung (char statt String)
        Testumfang: Parser, Semantikcheck
-}

incorrectAssignment5Class = "public class CorrectAssignments {\
  \  public Helper helper;\
  \  public void incorrectAssignment5() {\
  \      String x;\
  \      x = 'H';\
 \   }\
\}"

incorrectAssignment5_AbsSyntaxResult = [Class(Just Public, "CorrectAssignments", ClassBody([
    FieldDecl(Just Public, "Helper", "helper")], [
    MethodDecl(Just Public, "void", "incorrectAssignment5", [], Block([
        LocalVarDecl("String", "x"),
        StmtExprStmt(Assign("x", Char('H')))
    ]))
    ]))]

test_incorrectAssignment5_AbsSyntax = TestCase (assertEqual "Abstract Syntax" incorrectAssignment5_AbsSyntaxResult (parser(incorrectAssignment5Class)))





allTests_correctAssignments = TestList [test_correctAssignment_AbsSyntax, test_correctAssignment_SemanticCheck, 
                                    test_incorrectAssignment1_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(incorrectAssignment1_AbsSyntaxResult)),
                                    test_incorrectAssignment2_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(incorrectAssignment2_AbsSyntaxResult)),
                                    test_incorrectAssignment3_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(incorrectAssignment3_AbsSyntaxResult)),
                                    test_incorrectAssignment4_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(incorrectAssignment4_AbsSyntaxResult)),
                                    test_incorrectAssignment5_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(incorrectAssignment5_AbsSyntaxResult))]

correctAssignments_doTests = do
    print "Testcase 23: Testing Correct Assignments (Semantik check test) (FiedlerSemCheckTestCorrectAssignments.hs)"
    runTestTT allTests_correctAssignments
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")