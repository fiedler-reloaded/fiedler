module FiedlerTestAccessFlags where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 20: AccessFlags
        Autor: Dominik Sauerer
        Beschreibung: Test von Access flags bei Attributen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 20.1
        Autor: Dominik Sauerer
        Beschreibung: public => Test soll funktionieren
        Testumfang: gesamter Compiler
-}

accessFlagsMethod1Class = "public class AccessFlags {\
 \   public Helper helper;\
\    private int i;\
\    private boolean j;\
\    private char z;\
\    public void method1() {\
 \       i = helper.i;\
 \   }\
\}\
\\
\public class Helper {\
  \  public int i;\
  \  private boolean j;\
  \  protected char z;\
\}"

accessFlagsMethod1_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "i"),
    FieldDecl(Just Private, "boolean", "j"),
    FieldDecl(Just Protected, "char", "z")
    ], [])),
    Class(Just Public, "AccessFlags", ClassBody([
        FieldDecl(Just Public, "Helper", "helper"),
        FieldDecl(Just Private, "int", "i"),
        FieldDecl(Just Private, "boolean", "j"),
        FieldDecl(Just Private, "char", "z")
    ], [
        MethodDecl(Just Public, "void", "method1", [], StmtExprStmt(Assign("i", InstVar(LocalOrFieldVar("helper"), "i"))))
        ]))]

accessFlagsMethod1_AbsSyntaxTypedResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "i"),
    FieldDecl(Just Private, "boolean", "j"),
    FieldDecl(Just Protected, "char", "z")
    ], [])),
    Class(Just Public, "AccessFlags", ClassBody([
        FieldDecl(Just Public, "Helper", "helper"),
        FieldDecl(Just Private, "int", "i"),
        FieldDecl(Just Private, "boolean", "j"),
        FieldDecl(Just Private, "char", "z")
    ], [
        MethodDecl(Just Public, "void", "method1", [], TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("i", TypedExpr(InstVar(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "i"), "int")), "void")), "void"))
        ]))]

accessFlagsMethod1_BytecodeResult = [
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 16,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "i", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "j", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "Z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "C", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "Helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 2},
        super = SuperClass {index_sp = 3},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 3,
        array_fields = [Field_Info {af_fi = AccessFlags [1], index_name_fi = 4, index_descr_fi = 5, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [2], index_name_fi = 6, index_descr_fi = 7, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [4], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}], count_methods = 1, array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []},
    ClassFile {
        magic = Magic, 
        minver = MinorVersion {numMinVer = 0}, 
        maxver = MajorVersion {numMaxVer = 49}, 
        count_cp = 26, 
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 6, index_nameandtype_cp = 19, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 5, index_nameandtype_cp = 20, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 21, index_nameandtype_cp = 22, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 5, index_nameandtype_cp = 22, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 23, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 24, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "LHelper;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "i", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "j", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "Z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "C", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "method1", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 15, index_descr_cp = 16, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 25, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 9, index_descr_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "AccessFlags", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "Helper", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 5},
        super = SuperClass {index_sp = 6},
        count_interfaces = 0, 
        array_interfaces = [], 
        count_fields = 4, 
        array_fields = [Field_Info {af_fi = AccessFlags [1], index_name_fi = 7, index_descr_fi = 8, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [2], index_name_fi = 9, index_descr_fi = 10, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [2], index_name_fi = 11, index_descr_fi = 12, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [2], index_name_fi = 13, index_descr_fi = 14, tam_fi = 0, array_attr_fi = []}], count_methods = 2, array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 15, index_descr_mi = 16, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 17, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 18, index_descr_mi = 16, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 17, tam_len_attr = 24, len_stack_attr = 2, len_local_attr = 1, tam_code_attr = 12, array_code_attr = [42,42,180,0,2,180,0,3,181,0,4,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0, 
        array_attributes = []}]

test_accessFlagsMethod1_AbsSyntax = TestCase (assertEqual "Abstract Syntax" accessFlagsMethod1_AbsSyntaxResult (parser(accessFlagsMethod1Class)))
test_accessFlagsMethod1_SemanticCheck = TestCase (assertEqual "Semantic check" accessFlagsMethod1_AbsSyntaxTypedResult (typecheckPrg(accessFlagsMethod1_AbsSyntaxResult)))
test_accessFlagsMethod1_Codegen = TestCase (assertEqual "Codegen" accessFlagsMethod1_BytecodeResult (createPrg(accessFlagsMethod1_AbsSyntaxTypedResult)))
test_accessFlagsMethod1_complete  = TestCase (assertEqual "complete compiler"  accessFlagsMethod1_BytecodeResult (compiler(accessFlagsMethod1Class)))
allTests_accessFlagsMethod1 = TestList [test_accessFlagsMethod1_AbsSyntax, test_accessFlagsMethod1_SemanticCheck, test_accessFlagsMethod1_Codegen, test_accessFlagsMethod1_complete]






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 20.2
        Autor: Dominik Sauerer
        Beschreibung: private => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}


accessFlagsMethod2NotWorkingClass = "public class AccessFlags {\
 \   public Helper helper;\
\    private int i;\
\    private boolean j;\
\    private char z;\
\    public void method2_NotWorking() {\
 \       j = helper.j;\
 \   }\
\}\
\\
\public class Helper {\
  \  public int i;\
  \  private boolean j;\
  \  protected char z;\
\}"

accessFlagsMethod2NotWorking_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "i"),
    FieldDecl(Just Private, "boolean", "j"),
    FieldDecl(Just Protected, "char", "z")
    ], [])),
    Class(Just Public, "AccessFlags", ClassBody([
        FieldDecl(Just Public, "Helper", "helper"),
        FieldDecl(Just Private, "int", "i"),
        FieldDecl(Just Private, "boolean", "j"),
        FieldDecl(Just Private, "char", "z")
    ], [
        MethodDecl(Just Public, "void", "method2_NotWorking", [], StmtExprStmt(Assign("j", InstVar(LocalOrFieldVar("helper"), "j"))))
        ]))]

test_accessFlagsMethod2NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" accessFlagsMethod2NotWorking_AbsSyntaxResult (parser(accessFlagsMethod2NotWorkingClass)))
test_accessFlagsMethod2NotWorking_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(accessFlagsMethod2NotWorking_AbsSyntaxResult))
allTests_accessFlagsMethod2NotWorking = TestList [test_accessFlagsMethod2NotWorking_AbsSyntax, test_accessFlagsMethod2NotWorking_SemanticCheck]





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 20.3
        Autor: Dominik Sauerer
        Beschreibung: protected => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

accessFlagsMethod3NotWorkingClass = "public class AccessFlags {\
 \   public Helper helper;\
\    private int i;\
\    private boolean j;\
\    private char z;\
\    public void method3_NotWorking() {\
  \      z = helper.z;\
 \   }\
\}\
\\
\public class Helper {\
  \  public int i;\
  \  private boolean j;\
  \  protected char z;\
\}"

accessFlagsMethod3NotWorking_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "i"),
    FieldDecl(Just Private, "boolean", "j"),
    FieldDecl(Just Protected, "char", "z")
    ], [])),
    Class(Just Public, "AccessFlags", ClassBody([
        FieldDecl(Just Public, "Helper", "helper"),
        FieldDecl(Just Private, "int", "i"),
        FieldDecl(Just Private, "boolean", "j"),
        FieldDecl(Just Private, "char", "z")
    ], [
        MethodDecl(Just Public, "void", "method3_NotWorking", [], StmtExprStmt(Assign("z", InstVar(LocalOrFieldVar("helper"), "z"))))
        ]))]

test_accessFlagsMethod3NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" accessFlagsMethod3NotWorking_AbsSyntaxResult (parser(accessFlagsMethod3NotWorkingClass)))
test_accessFlagsMethod3NotWorking_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(accessFlagsMethod3NotWorking_AbsSyntaxResult))
allTests_accessFlagsMethod3NotWorking = TestList [test_accessFlagsMethod3NotWorking_AbsSyntax, test_accessFlagsMethod3NotWorking_SemanticCheck]





accessFlags_doTests = do
    print "Testcase 20: Testing correct effect of access flags (FiedlerTest"
    print "Test 20.1: Test of public flag"
    runTestTT allTests_accessFlagsMethod1
    print "Test 20.2: Test of private flag"
    runTestTT allTests_accessFlagsMethod2NotWorking
    print "Test 20.3: Test of protected flag"
    runTestTT allTests_accessFlagsMethod3NotWorking
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")