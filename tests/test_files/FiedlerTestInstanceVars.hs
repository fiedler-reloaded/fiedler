module FiedlerTestInstanceVars where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

instanceVarsClass = "public class InstanceVars {\
\    public boolean method() {\
 \       Helper helper;\
  \      helper = new Helper(5, true);\
 \       return helper.z;\
 \   }\
\}\
\\
\public class Helper {\
 \   public boolean z;\
 \   public int xy;\
 \   public Helper(int x, boolean a) {\
  \      z = a;\
 \       xy = x;\
   \ }\
\}"

{-
        Testcase 13: InstanceVars
        Autor: Dominik Sauerer
        Beschreibung: Test von Zugriffen auf Felder von Instanzen
        Testumfang: gesamter Compiler
-}

instanceVars_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "boolean", "z"),
    FieldDecl(Just Public, "int", "xy")
    ], [
    ConstructorDecl(Just Public, "Helper", [("int", "x"), ("boolean", "a")], Block([
        StmtExprStmt(Assign("z", LocalOrFieldVar("a"))),
        StmtExprStmt(Assign("xy", LocalOrFieldVar("x")))
    ]))
    ])),
    Class(Just Public, "InstanceVars", ClassBody([], [
        MethodDecl(Just Public, "boolean", "method", [], Block([
            LocalVarDecl("Helper", "helper"),
            StmtExprStmt(Assign("helper", StmtExprExpr(New("Helper", [Integer(5), Bool(True)])))),
            Return(InstVar(LocalOrFieldVar("helper"), "z"))
        ]))
        ]))]

instanceVars_AbsSyntaxTypedResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "boolean", "z"),
    FieldDecl(Just Public, "int", "xy")
    ], [
    ConstructorDecl(Just Public, "Helper", [("int", "x"), ("boolean", "a")], TypedStmt(Block([
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("z", TypedExpr(LocalOrFieldVar("a"), "boolean")), "void")), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("xy", TypedExpr(LocalOrFieldVar("x"), "int")), "void")), "void")
    ]), "void"))
    ])),
    Class(Just Public, "InstanceVars", ClassBody([], [
        MethodDecl(Just Public, "boolean", "method", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("Helper", "helper"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(StmtExprExpr(TypedStmtExpr(New("Helper", [TypedExpr(Integer(5), "int"), TypedExpr(Bool(True), "boolean")]), "Helper")), "Helper")), "void")), "void"),
            TypedStmt(Return(TypedExpr(InstVar(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "z"), "boolean")), "boolean")
        ]), "boolean"))
        ]))]

instanceVars_BytecodeResult = [
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 19,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 13, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 4, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 4, index_nameandtype_cp = 15, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 16, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 17, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "Z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 2, cad_cp = "xy", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "(IZ)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 18, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 6, index_descr_cp = 7, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "Helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 4},
        super = SuperClass {index_sp = 5},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 2,
        array_fields = [Field_Info {af_fi = AccessFlags [1], index_name_fi = 6, index_descr_fi = 7, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [1], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}], count_methods = 1, array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 27, len_stack_attr = 2, len_local_attr = 3, tam_code_attr = 15, array_code_attr = [42,183,0,1,42,28,181,0,2,42,27,181,0,3,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 21,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 6, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 2, index_nameandtype_cp = 15, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 16, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 17, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "method", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()Z", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "Helper", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 18, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 19, index_descr_cp = 20, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 12, cad_cp = "InstanceVars", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "(IZ)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "Z", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 5},
        super = SuperClass {index_sp = 6},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 0,
        array_fields = [],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 27, len_stack_attr = 4, len_local_attr = 2, tam_code_attr = 15, array_code_attr = [187,0,2,89,8,4,183,0,3,76,43,180,0,4,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0, 
        array_attributes = []}]
    

test_instanceVars_AbsSyntax = TestCase (assertEqual "Abstract Syntax" instanceVars_AbsSyntaxResult (parser(instanceVarsClass)))
test_instanceVars_SemanticCheck = TestCase (assertEqual "Semantic check" instanceVars_AbsSyntaxTypedResult (typecheckPrg(instanceVars_AbsSyntaxResult)))
test_instanceVars_Codegen = TestCase (assertEqual "Codegen" instanceVars_BytecodeResult (createPrg(instanceVars_AbsSyntaxTypedResult)))
test_instanceVars_complete  = TestCase (assertEqual "complete compiler"  instanceVars_BytecodeResult (compiler(instanceVarsClass)))
allTests_instanceVars = TestList [test_instanceVars_AbsSyntax, test_instanceVars_SemanticCheck, test_instanceVars_Codegen, test_instanceVars_complete]

instanceVars_doTests = do
    print "Testcase 13: Testing calls of attributes of instances (FiedlerTestInstanceVars.hs)"
    runTestTT allTests_instanceVars
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")