module FiedlerTestAssignments where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 9: Assignments
        Autor: Dominik Sauerer
        Beschreibung: Test von Zuweisungen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 9.1
        Autor: Dominik Sauerer
        Beschreibung: Test von Assignments auf lokale Variablen
        Testumfang: gesamter Compiler
-}

assignmentsMethodClass = "public class Assignments {\
   \ public void method() {\
   \     int x;\
   \     x = 0;\
  \      boolean y;\
  \      boolean z;\
  \      y = false;\
  \      z = y;\
  \  }\
\}"

assignmentsMethod_AbsSyntaxResult = [Class(Just Public, "Assignments", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [], Block([
            LocalVarDecl("int", "x"),
            StmtExprStmt(Assign("x", Integer(0))),
            LocalVarDecl("boolean", "y"),
            LocalVarDecl("boolean", "z"),
            StmtExprStmt(Assign("y", Bool(False))),
            StmtExprStmt(Assign("z", LocalOrFieldVar("y")))
        ]))]))]

assignmentsMethod_AbsSyntaxTypedResult = [Class(Just Public, "Assignments", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(0), "int")), "void")), "void"),
            TypedStmt(LocalVarDecl("boolean", "y"), "void"),
            TypedStmt(LocalVarDecl("boolean", "z"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Bool(False), "boolean")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("z", TypedExpr(LocalOrFieldVar("y"), "boolean")), "void")), "void")
        ]), "void"))]))]

assignmentsMethod_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 11,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "method", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "Assignments", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 2,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 19, len_stack_attr = 1, len_local_attr = 4, tam_code_attr = 7, array_code_attr = [3,60,3,61,28,62,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_assignmentsMethod_AbsSyntax = TestCase (assertEqual "Abstract Syntax" assignmentsMethod_AbsSyntaxResult (parser(assignmentsMethodClass)))
test_assignmentsMethod_SemanticCheck = TestCase (assertEqual "Semantic check" assignmentsMethod_AbsSyntaxTypedResult (typecheckPrg(assignmentsMethod_AbsSyntaxResult)))
test_assignmentsMethod_Codegen = TestCase (assertEqual "Codegen" assignmentsMethod_BytecodeResult (createPrg(assignmentsMethod_AbsSyntaxTypedResult)))
test_assignmentsMethod_complete  = TestCase (assertEqual "complete compiler"  assignmentsMethod_BytecodeResult (compiler(assignmentsMethodClass)))
allTests_assignmentsMethod = TestList [test_assignmentsMethod_AbsSyntax, test_assignmentsMethod_SemanticCheck, test_assignmentsMethod_Codegen, test_assignmentsMethod_complete]


------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 9.2
        Autor: Dominik Sauerer
        Beschreibung: Test von Assignments auf Felder
        Testumfang: gesamter Compiler
-}

assignmentsOtherMethodClass = "public class Assignments {\
    \private char attribute;\
    \protected int otherMethod(char z) {\
   \     attribute = z;\
  \      return 0;\
  \  }\
\}"

assignmentsOtherMethod_AbsSyntaxResult = [Class(Just Public, "Assignments", ClassBody([
    FieldDecl(Just Private, "char", "attribute")
    ], [
        MethodDecl(Just Protected, "int", "otherMethod", [("char", "z")], Block([
            StmtExprStmt(Assign("attribute", LocalOrFieldVar("z"))),
            Return(Integer(0))
        ]))]))]

assignmentsOtherMethod_AbsSyntaxTypedResult = [Class(Just Public, "Assignments", ClassBody([
    FieldDecl(Just Private, "char", "attribute")
    ], [
        MethodDecl(Just Protected, "int", "otherMethod", [("char", "z")], TypedStmt(Block([
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("attribute", TypedExpr(LocalOrFieldVar("z"), "char")), "void")), "void"),
            TypedStmt(Return(TypedExpr(Integer(0), "int")), "int")
        ]), "int"))]))]

assignmentsOtherMethod_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 16,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 4, index_nameandtype_cp = 12, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 3, index_nameandtype_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "attribute", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "C", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "otherMethod", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(C)I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 5, index_descr_cp = 6, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "Assignments", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 3},
    super = SuperClass {index_sp = 4},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 1,
    array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 5, index_descr_fi = 6, tam_fi = 0, array_attr_fi = []}],
    count_methods = 2,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [4], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 19, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 7, array_code_attr = [42,27,181,0,2,3,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_assignmentsOtherMethod_AbsSyntax = TestCase (assertEqual "Abstract Syntax" assignmentsOtherMethod_AbsSyntaxResult (parser(assignmentsOtherMethodClass)))
test_assignmentsOtherMethod_SemanticCheck = TestCase (assertEqual "Semantic check" assignmentsOtherMethod_AbsSyntaxTypedResult (typecheckPrg(assignmentsOtherMethod_AbsSyntaxResult)))
test_assignmentsOtherMethod_Codegen = TestCase (assertEqual "Codegen" assignmentsOtherMethod_BytecodeResult (createPrg(assignmentsOtherMethod_AbsSyntaxTypedResult)))
test_assignmentsOtherMethod_complete  = TestCase (assertEqual "complete compiler"  assignmentsOtherMethod_BytecodeResult (compiler(assignmentsOtherMethodClass)))
allTests_assignmentsOtherMethod = TestList [test_assignmentsOtherMethod_AbsSyntax, test_assignmentsOtherMethod_SemanticCheck, test_assignmentsOtherMethod_Codegen, test_assignmentsOtherMethod_complete]




assignments_doTests = do
    print "Testcase 9: Testing assignments (FiedlerTestAssignments.hs)"
    --print (compiler(calculationsClass))
    print "Test 9.1: assignments with local variables"
    runTestTT allTests_assignmentsMethod
    print "Test 9.2: assignments with field variables"
    runTestTT allTests_assignmentsOtherMethod
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")