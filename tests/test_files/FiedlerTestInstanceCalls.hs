module FiedlerTestInstanceCalls where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools


{-
        Testcase 12: InstanceCalls
        Autor: Dominik Sauerer
        Beschreibung: Test von Methodenaufrufen von Instanzen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 12.1
        Autor: Dominik Sauerer
        Beschreibung: Test von Methoden ohne Parameter und Rückgabewert
        Testumfang: gesamter Compiler
-}

instanceCallMethod1Class = "public class InstanceCalls {\
 \   private HelperClass helper;\
 \   public InstanceCalls() {\
 \       helper = new HelperClass();\
 \   }\
 \   public void method1() {\
 \       helper.doSomething();\
 \   }\
\}\
\\
\public class HelperClass {\
  \  public void doSomething() {\
\\
    \}\
    \public void doAnotherThing(int x, String y) {\
   \     \
    \}\
  \  public int doAThirdThing() {\
  \      return 10;\
  \  }\
\}"

instanceCallMethod1_AbsSyntaxResult = [Class(Just Public, "HelperClass", ClassBody([], [
    MethodDecl(Just Public, "void", "doSomething", [], Empty),
    MethodDecl(Just Public, "void", "doAnotherThing", [("int", "x"), ("String", "y")], Empty),
    MethodDecl(Just Public, "int", "doAThirdThing", [], Return(Integer(10)))
    ])),
    Class(Just Public, "InstanceCalls", ClassBody([
        FieldDecl(Just Private, "HelperClass", "helper")
    ], [
        ConstructorDecl(Just Public, "InstanceCalls", [], StmtExprStmt(Assign("helper", StmtExprExpr(New("HelperClass", []))))),
        MethodDecl(Just Public, "void", "method1", [], StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "doSomething", [])))
        ]))]

instanceCallMethod1_AbsSyntaxTypedResult = [Class(Just Public, "HelperClass", ClassBody([], [
    MethodDecl(Just Public, "void", "doSomething", [], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "void", "doAnotherThing", [("int", "x"), ("String", "y")], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "int", "doAThirdThing", [], TypedStmt(Return(TypedExpr(Integer(10), "int")), "int"))
    ])),
    Class(Just Public, "InstanceCalls", ClassBody([
        FieldDecl(Just Private, "HelperClass", "helper")
    ], [
        ConstructorDecl(Just Public, "InstanceCalls", [], TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(StmtExprExpr(TypedStmtExpr(New("HelperClass", []), "HelperClass")), "HelperClass")), "void")), "void")),
        MethodDecl(Just Public, "void", "method1", [], TypedStmt(StmtExprStmt(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "HelperClass"), "doSomething", []), "void")), "void"))
        ]))]

instanceCallMethod1_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 15,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 22, cad_cp = "(ILjava/lang/String;)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "doAThirdThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 4,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 3, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 3, array_code_attr = [16,10,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 21,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 7, index_nameandtype_cp = 14, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 6, index_nameandtype_cp = 16, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 17, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 19, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "LHelperClass;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "method1", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 20, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "InstanceCalls", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 6},
        super = SuperClass {index_sp = 7},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 1,
        array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 28, len_stack_attr = 3, len_local_attr = 1, tam_code_attr = 16, array_code_attr = [42,183,0,1,42,187,0,2,89,183,0,3,181,0,4,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 13, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 20, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 8, array_code_attr = [42,180,0,4,182,0,5,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

test_instanceCallMethod1_AbsSyntax = TestCase (assertEqual "Abstract Syntax" instanceCallMethod1_AbsSyntaxResult (parser(instanceCallMethod1Class)))
test_instanceCallMethod1_SemanticCheck = TestCase (assertEqual "Semantic check" instanceCallMethod1_AbsSyntaxTypedResult (typecheckPrg(instanceCallMethod1_AbsSyntaxResult)))
test_instanceCallMethod1_Codegen = TestCase (assertEqual "Codegen" instanceCallMethod1_BytecodeResult (createPrg(instanceCallMethod1_AbsSyntaxTypedResult)))
test_instanceCallMethod1_complete  = TestCase (assertEqual "complete compiler"  instanceCallMethod1_BytecodeResult (compiler(instanceCallMethod1Class)))
allTests_instanceCallMethod1 = TestList [test_instanceCallMethod1_AbsSyntax, test_instanceCallMethod1_SemanticCheck, test_instanceCallMethod1_Codegen, test_instanceCallMethod1_complete]



------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 12.2
        Autor: Dominik Sauerer
        Beschreibung: Test von Methoden mit Parameter und ohne Rückgabewert
        Testumfang: gesamter Compiler
-}

instanceCallMethod2Class = "public class InstanceCalls {\
 \   private HelperClass helper;\
 \   public InstanceCalls() {\
 \       helper = new HelperClass();\
 \   }\
 \   public void method2() {\
 \       helper.doAnotherThing(5, \"hello\");\
  \  }\
\}\
\\
\public class HelperClass {\
  \  public void doSomething() {\
\\
    \}\
    \public void doAnotherThing(int x, String y) {\
   \     \
    \}\
  \  public int doAThirdThing() {\
  \      return 10;\
  \  }\
\}"

instanceCallMethod2_AbsSyntaxResult = [Class(Just Public, "HelperClass", ClassBody([], [
    MethodDecl(Just Public, "void", "doSomething", [], Empty),
    MethodDecl(Just Public, "void", "doAnotherThing", [("int", "x"), ("String", "y")], Empty),
    MethodDecl(Just Public, "int", "doAThirdThing", [], Return(Integer(10)))
    ])),
    Class(Just Public, "InstanceCalls", ClassBody([
        FieldDecl(Just Private, "HelperClass", "helper")
    ], [
        ConstructorDecl(Just Public, "InstanceCalls", [], StmtExprStmt(Assign("helper", StmtExprExpr(New("HelperClass", []))))),
        MethodDecl(Just Public, "void", "method2", [], StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "doAnotherThing", [Integer(5), String("hello")])))
        ]))]


instanceCallMethod2_AbsSyntaxTypedResult = [Class(Just Public, "HelperClass", ClassBody([], [
    MethodDecl(Just Public, "void", "doSomething", [], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "void", "doAnotherThing", [("int", "x"), ("String", "y")], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "int", "doAThirdThing", [], TypedStmt(Return(TypedExpr(Integer(10), "int")), "int"))
    ])),
    Class(Just Public, "InstanceCalls", ClassBody([
        FieldDecl(Just Private, "HelperClass", "helper")
    ], [
        ConstructorDecl(Just Public, "InstanceCalls", [], TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(StmtExprExpr(TypedStmtExpr(New("HelperClass", []), "HelperClass")), "HelperClass")), "void")), "void")),
        MethodDecl(Just Public, "void", "method2", [], TypedStmt(StmtExprStmt(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "HelperClass"), "doAnotherThing", [TypedExpr(Integer(5), "int"), TypedExpr(String("hello"), "String")]), "void")), "void"))
        ]))]

instanceCallMethod2_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 15,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 22, cad_cp = "(ILjava/lang/String;)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "doAThirdThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 4,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 3, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 3, array_code_attr = [16,10,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 24,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 8, index_nameandtype_cp = 15, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 16, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 15, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 7, index_nameandtype_cp = 17, desc = ""},String_Info {tag_cp = TagString, index_cp = 18, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 19, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 20, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 21, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "LHelperClass;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "method2", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 11, index_descr_cp = 12, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 9, index_descr_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "hello", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 22, index_descr_cp = 23, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "InstanceCalls", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 22, cad_cp = "(ILjava/lang/String;)V", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 7},
        super = SuperClass {index_sp = 8},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 1,
        array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 9, index_descr_fi = 10, tam_fi = 0, array_attr_fi = []}],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 11, index_descr_mi = 12, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 13, tam_len_attr = 28, len_stack_attr = 3, len_local_attr = 1, tam_code_attr = 16, array_code_attr = [42,183,0,1,42,187,0,2,89,183,0,3,181,0,4,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 14, index_descr_mi = 12, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 13, tam_len_attr = 23, len_stack_attr = 3, len_local_attr = 1, tam_code_attr = 11, array_code_attr = [42,180,0,4,8,18,5,182,0,6,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

test_instanceCallMethod2_AbsSyntax = TestCase (assertEqual "Abstract Syntax" instanceCallMethod2_AbsSyntaxResult (parser(instanceCallMethod2Class)))
test_instanceCallMethod2_SemanticCheck = TestCase (assertEqual "Semantic check" instanceCallMethod2_AbsSyntaxTypedResult (typecheckPrg(instanceCallMethod2_AbsSyntaxResult)))
test_instanceCallMethod2_Codegen = TestCase (assertEqual "Codegen" instanceCallMethod2_BytecodeResult (createPrg(instanceCallMethod2_AbsSyntaxTypedResult)))
test_instanceCallMethod2_complete  = TestCase (assertEqual "complete compiler"  instanceCallMethod2_BytecodeResult (compiler(instanceCallMethod2Class)))
allTests_instanceCallMethod2 = TestList [test_instanceCallMethod2_AbsSyntax, test_instanceCallMethod2_SemanticCheck, test_instanceCallMethod2_Codegen, test_instanceCallMethod2_complete]


------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 12.3
        Autor: Dominik Sauerer
        Beschreibung: Test von Methoden mit Rückgabewert
        Testumfang: gesamter Compiler
-}

instanceCallMethod3Class = "public class InstanceCalls {\
 \   private HelperClass helper;\
 \   public InstanceCalls() {\
 \       helper = new HelperClass();\
 \   }\
 \   protected void method3() {\
     \   int x;\
     \   x = helper.doAThirdThing();\
   \  }\
\}\
\\
\public class HelperClass {\
  \  public void doSomething() {\
\\
    \}\
    \public void doAnotherThing(int x, String y) {\
   \     \
    \}\
  \  public int doAThirdThing() {\
  \      return 10;\
  \  }\
\}"

instanceCallMethod3_AbsSyntaxResult = [Class(Just Public, "HelperClass", ClassBody([], [
    MethodDecl(Just Public, "void", "doSomething", [], Empty),
    MethodDecl(Just Public, "void", "doAnotherThing", [("int", "x"), ("String", "y")], Empty),
    MethodDecl(Just Public, "int", "doAThirdThing", [], Return(Integer(10)))
    ])),
    Class(Just Public, "InstanceCalls", ClassBody([
        FieldDecl(Just Private, "HelperClass", "helper")
    ], [
        ConstructorDecl(Just Public, "InstanceCalls", [], StmtExprStmt(Assign("helper", StmtExprExpr(New("HelperClass", []))))),
        MethodDecl(Just Protected, "void", "method3", [], Block([
            LocalVarDecl("int", "x"),
            StmtExprStmt(Assign("x", StmtExprExpr(MethodCall(LocalOrFieldVar("helper"), "doAThirdThing", []))))
        ]))
        ]))]

instanceCallMethod3_AbsSyntaxTypedResult = [Class(Just Public, "HelperClass", ClassBody([], [
    MethodDecl(Just Public, "void", "doSomething", [], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "void", "doAnotherThing", [("int", "x"), ("String", "y")], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "int", "doAThirdThing", [], TypedStmt(Return(TypedExpr(Integer(10), "int")), "int"))
    ])),
    Class(Just Public, "InstanceCalls", ClassBody([
        FieldDecl(Just Private, "HelperClass", "helper")
    ], [
        ConstructorDecl(Just Public, "InstanceCalls", [], TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(StmtExprExpr(TypedStmtExpr(New("HelperClass", []), "HelperClass")), "HelperClass")), "void")), "void")),
        MethodDecl(Just Protected, "void", "method3", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(StmtExprExpr(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "HelperClass"), "doAThirdThing", []), "int")), "int")), "void")), "void")
        ]), "void"))
        ]))]

instanceCallMethod3_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 15,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 22, cad_cp = "(ILjava/lang/String;)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "doAThirdThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 4,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 3, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 3, array_code_attr = [16,10,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 22,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 7, index_nameandtype_cp = 14, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 6, index_nameandtype_cp = 16, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 17, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 19, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "LHelperClass;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "method3", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 20, index_descr_cp = 21, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "InstanceCalls", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "doAThirdThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 6},
        super = SuperClass {index_sp = 7},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 1,
        array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 28, len_stack_attr = 3, len_local_attr = 1, tam_code_attr = 16, array_code_attr = [42,183,0,1,42,187,0,2,89,183,0,3,181,0,4,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [4], index_name_mi = 13, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 21, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 9, array_code_attr = [42,180,0,4,182,0,5,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

test_instanceCallMethod3_AbsSyntax = TestCase (assertEqual "Abstract Syntax" instanceCallMethod3_AbsSyntaxResult (parser(instanceCallMethod3Class)))
test_instanceCallMethod3_SemanticCheck = TestCase (assertEqual "Semantic check" instanceCallMethod3_AbsSyntaxTypedResult (typecheckPrg(instanceCallMethod3_AbsSyntaxResult)))
test_instanceCallMethod3_Codegen = TestCase (assertEqual "Codegen" instanceCallMethod3_BytecodeResult (createPrg(instanceCallMethod3_AbsSyntaxTypedResult)))
test_instanceCallMethod3_complete  = TestCase (assertEqual "complete compiler"  instanceCallMethod3_BytecodeResult (compiler(instanceCallMethod3Class)))
allTests_instanceCallMethod3 = TestList [test_instanceCallMethod3_AbsSyntax, test_instanceCallMethod3_SemanticCheck, test_instanceCallMethod3_Codegen, test_instanceCallMethod3_complete]




instanceCalls_doTests = do
    print "Testcase 12: Testing calls of attributes and methods of instances (FiedlerTestInstanceCalls.hs)"
    --print (compiler(calculationsClass))
    print "Test 12.1: method call without return or params"
    runTestTT allTests_instanceCallMethod1
    print "Test 12.2: method call with params"
    runTestTT allTests_instanceCallMethod2
    print "Test 12.3: method call with return value"
    runTestTT allTests_instanceCallMethod3
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")