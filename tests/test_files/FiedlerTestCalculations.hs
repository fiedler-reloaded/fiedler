module FiedlerTestCalculations where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 10: Calculations
        Autor: Dominik Sauerer
        Beschreibung: Test von Berechnungen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 10.1
        Autor: Dominik Sauerer
        Beschreibung: Test von einfachen Additionen
        Testumfang: gesamter Compiler
-}

calculationsMathClass = "public class Calculations {\
 \   public void math() {\
 \       int x;\
 \       x = 2 + 1;\
 \       x = -4 + 7;\
 \   }\
\}"

calculationsMath_AbsSyntaxResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "math", [], Block([
            LocalVarDecl("int", "x"),
            StmtExprStmt(Assign("x", Binary("+", Integer(2), Integer(1)))),
            StmtExprStmt(Assign("x", Binary("+", Integer(-4), Integer(7))))
        ]))]))]

calculationsMath_AbsSyntaxTypedResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "math", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("+", TypedExpr(Integer(2), "int"), TypedExpr(Integer(1), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("+", TypedExpr(Integer(-4), "int"), TypedExpr(Integer(7), "int")), "int")), "void")), "void")
        ]), "void"))]))]

calculationsMath_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 11,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "math", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 12, cad_cp = "Calculations", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 5, array_code_attr = [6,60,6,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0,
    array_attributes = []}]

test_calculationsMath_AbsSyntax = TestCase (assertEqual "Abstract Syntax" calculationsMath_AbsSyntaxResult (parser(calculationsMathClass)))
test_calculationsMath_SemanticCheck = TestCase (assertEqual "Semantic check" calculationsMath_AbsSyntaxTypedResult (typecheckPrg(calculationsMath_AbsSyntaxResult)))
test_calculationsMath_Codegen = TestCase (assertEqual "Codegen" calculationsMath_BytecodeResult (createPrg(calculationsMath_AbsSyntaxTypedResult)))
test_calculationsMath_complete  = TestCase (assertEqual "complete compiler"  calculationsMath_BytecodeResult (compiler(calculationsMathClass)))
allTests_calculationsMath = TestList [test_calculationsMath_AbsSyntax, test_calculationsMath_SemanticCheck, test_calculationsMath_Codegen, test_calculationsMath_complete]




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 10.2
        Autor: Dominik Sauerer
        Beschreibung: Test von allen möglichen arithmetischen Rechenoperatoren
        Testumfang: gesamter Compiler
-}

calculationsMath2Class = "public class Calculations {\
  \  public void math2() {\
  \      int x;\
  \      x = 2 + 1;\
  \      int y;\
 \       y = x - x;\
  \      y = x - 2;\
 \       y = 2 * 5;\
  \      x = 8 / 2;\
 \       x = 6 % 2;\
 \       y = x++;\
 \       y = x--;\
 \   }\
\}"

calculationsMath2_AbsSyntaxResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "math2", [], Block([
            LocalVarDecl("int", "x"),
            StmtExprStmt(Assign("x", Binary("+", Integer(2), Integer(1)))),
            LocalVarDecl("int", "y"),
            StmtExprStmt(Assign("y", Binary("-", LocalOrFieldVar("x"), LocalOrFieldVar("x")))),
            StmtExprStmt(Assign("y", Binary("-", LocalOrFieldVar("x"), Integer(2)))),
            StmtExprStmt(Assign("y", Binary("*", Integer(2), Integer(5)))),
            StmtExprStmt(Assign("x", Binary("/", Integer(8), Integer(2)))),
            StmtExprStmt(Assign("x", Binary("%", Integer(6), Integer(2)))),
            StmtExprStmt(Assign("y", Unary("++", LocalOrFieldVar("x")))),
            StmtExprStmt(Assign("y", Unary("--", LocalOrFieldVar("x"))))
        ]))]))]

calculationsMath2_AbsSyntaxTypedResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "math2", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("+", TypedExpr(Integer(2), "int"), TypedExpr(Integer(1), "int")), "int")), "void")), "void"),
            TypedStmt(LocalVarDecl("int", "y"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Binary("-", TypedExpr(LocalOrFieldVar("x"), "int"), TypedExpr(LocalOrFieldVar("x"), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Binary("-", TypedExpr(LocalOrFieldVar("x"), "int"), TypedExpr(Integer(2), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Binary("*", TypedExpr(Integer(2), "int"), TypedExpr(Integer(5), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("/", TypedExpr(Integer(8), "int"), TypedExpr(Integer(2), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("%", TypedExpr(Integer(6), "int"), TypedExpr(Integer(2), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Unary("++", TypedExpr(LocalOrFieldVar("x"), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Unary("--", TypedExpr(LocalOrFieldVar("x"), "int")), "int")), "void")), "void")
        ]), "void"))]))]

calculationsMath2_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 11,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "math2", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 12, cad_cp = "Calculations", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 2,
    array_methods = [
        Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},
        Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 41, len_stack_attr = 3, len_local_attr = 3, tam_code_attr = 29, array_code_attr = [6,60,27,27,100,61,27,5,100,61,16,10,61,7,60,3,60,27,132,1,1,61,27,89,4,100,60,61,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_calculationsMath2_AbsSyntax = TestCase (assertEqual "Abstract Syntax" calculationsMath2_AbsSyntaxResult (parser(calculationsMath2Class)))
test_calculationsMath2_SemanticCheck = TestCase (assertEqual "Semantic check" calculationsMath2_AbsSyntaxTypedResult (typecheckPrg(calculationsMath2_AbsSyntaxResult)))
test_calculationsMath2_Codegen = TestCase (assertEqual "Codegen" calculationsMath2_BytecodeResult (createPrg(calculationsMath2_AbsSyntaxTypedResult)))
test_calculationsMath2_complete  = TestCase (assertEqual "complete compiler"  calculationsMath2_BytecodeResult (compiler(calculationsMath2Class)))
allTests_calculationsMath2 = TestList [test_calculationsMath2_AbsSyntax, test_calculationsMath2_SemanticCheck, test_calculationsMath2_Codegen, test_calculationsMath2_complete]



------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 10.3
        Autor: Dominik Sauerer
        Beschreibung: Test von Rechnungen mit Klammern
        Testumfang: gesamter Compiler
-}

calculationsMathWithBracketsClass = "public class Calculations {\
  \  public void mathWithBrackets() {\
 \       int x;\
 \       int y;\
  \      x = 2 + 4;\
 \       y = 2 + (3 * 4);\
 \       x = (2 + 3) + 1;\
 \   }\
\}"

calculationsMathWithBrackets_AbsSyntaxResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "mathWithBrackets", [], Block([
            LocalVarDecl("int", "x"),
            LocalVarDecl("int", "y"),
            StmtExprStmt(Assign("x", Binary("+", Integer(2), Integer(4)))),
            StmtExprStmt(Assign("y", Binary("+", Integer(2), Binary("*", Integer(3), Integer(4))))),
            StmtExprStmt(Assign("x", Binary("+", Binary("+", Integer(2), Integer(3)), Integer(1))))
        ]))]))]

calculationsMathWithBrackets_AbsSyntaxTypedResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "mathWithBrackets", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(LocalVarDecl("int", "y"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("+", TypedExpr(Integer(2), "int"), TypedExpr(Integer(4), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Binary("+", TypedExpr(Integer(2), "int"), TypedExpr(Binary("*", TypedExpr(Integer(3), "int"), TypedExpr(Integer(4), "int")), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("+", TypedExpr(Binary("+", TypedExpr(Integer(2), "int"), TypedExpr(Integer(3), "int")), "int"), TypedExpr(Integer(1), "int")), "int")), "void")), "void")
        ]), "void"))]))]

calculationsMathWithBrackets_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 11,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "mathWithBrackets", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 12, cad_cp = "Calculations", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 2,
    array_methods = [
        Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},
        Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 22, len_stack_attr = 1, len_local_attr = 3, tam_code_attr = 10, array_code_attr = [16,6,60,16,14,61,16,6,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_calculationsMathWithBrackets_AbsSyntax = TestCase (assertEqual "Abstract Syntax" calculationsMathWithBrackets_AbsSyntaxResult (parser(calculationsMathWithBracketsClass)))
test_calculationsMathWithBrackets_SemanticCheck = TestCase (assertEqual "Semantic check" calculationsMathWithBrackets_AbsSyntaxTypedResult (typecheckPrg(calculationsMathWithBrackets_AbsSyntaxResult)))
test_calculationsMathWithBrackets_Codegen = TestCase (assertEqual "Codegen" calculationsMathWithBrackets_BytecodeResult (createPrg(calculationsMathWithBrackets_AbsSyntaxTypedResult)))
test_calculationsMathWithBrackets_complete  = TestCase (assertEqual "complete compiler"  calculationsMathWithBrackets_BytecodeResult (compiler(calculationsMathWithBracketsClass)))
allTests_calculationsMathWithBrackets = TestList [test_calculationsMathWithBrackets_AbsSyntax, test_calculationsMathWithBrackets_SemanticCheck, test_calculationsMathWithBrackets_Codegen, test_calculationsMathWithBrackets_complete]




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 10.4
        Autor: Dominik Sauerer
        Beschreibung: Test von bitwise Rechenoperatoren
        Testumfang: gesamter Compiler
-}

calculationsBitwiseClass = "public class Calculations {\
 \   public void bitwiseOperators() {\
  \      int x;\
  \      int y;\
   \     x = 2;\
   \     y = 1;\ 
    \    x = ~y;\
    \    x = y >> 2;\
    \    x = y << 2;\
   \     x = y ^ x;\
   \     x = x | y;\
   \     x = x & y;\
  \  }\
 \ }"

calculationsBitwise_AbsSyntaxResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "bitwiseOperators", [], Block([
            LocalVarDecl("int", "x"),
            LocalVarDecl("int", "y"),
            StmtExprStmt(Assign("x", Integer(2))),
            StmtExprStmt(Assign("y", Integer(1))),
            StmtExprStmt(Assign("x", Unary("~", LocalOrFieldVar("y")))),
            StmtExprStmt(Assign("x", Binary(">>", LocalOrFieldVar("y"), Integer(2)))),
            StmtExprStmt(Assign("x", Binary("<<", LocalOrFieldVar("y"), Integer(2)))),
            StmtExprStmt(Assign("x", Binary("^", LocalOrFieldVar("y"), LocalOrFieldVar("x")))),
            StmtExprStmt(Assign("x", Binary("|", LocalOrFieldVar("x"), LocalOrFieldVar("y")))),
            StmtExprStmt(Assign("x", Binary("&", LocalOrFieldVar("x"), LocalOrFieldVar("y"))))
        ]))]))]

calculationsBitwise_AbsSyntaxTypedResult = [Class(Just Public, "Calculations", ClassBody([], [
        MethodDecl(Just Public, "void", "bitwiseOperators", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(LocalVarDecl("int", "y"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(2), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("y", TypedExpr(Integer(1), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Unary("~", TypedExpr(LocalOrFieldVar("y"), "int")), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary(">>", TypedExpr(LocalOrFieldVar("y"), "int"), TypedExpr(Integer(2), "int")), "int")), "void")),"void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("<<", TypedExpr(LocalOrFieldVar("y"), "int"), TypedExpr(Integer(2), "int")), "int")),"void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("^", TypedExpr(LocalOrFieldVar("y"), "int"), TypedExpr(LocalOrFieldVar("x"), "int")), "int")), "void")),"void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("|", TypedExpr(LocalOrFieldVar("x"), "int"), TypedExpr(LocalOrFieldVar("y"), "int")), "int")), "void")),"void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("&", TypedExpr(LocalOrFieldVar("x"), "int"), TypedExpr(LocalOrFieldVar("y"), "int")), "int")), "void")), "void")
            ]), "void"))]))]

calculationsBitwise_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 11, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "bitwiseOperators", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 12, cad_cp = "Calculations", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [
        Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},
        Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 41, len_stack_attr = 2, len_local_attr = 3, tam_code_attr = 29, array_code_attr = [5,60,4,61,28,2,130,60,28,5,122,60,28,5,120,60,28,27,130,60,27,28,128,60,27,28,126,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_calculationsBitwise_AbsSyntax = TestCase (assertEqual "Abstract Syntax" calculationsBitwise_AbsSyntaxResult (parser(calculationsBitwiseClass)))
test_calculationsBitwise_SemanticCheck = TestCase (assertEqual "Semantic check" calculationsBitwise_AbsSyntaxTypedResult (typecheckPrg(calculationsBitwise_AbsSyntaxResult)))
test_calculationsBitwise_Codegen = TestCase (assertEqual "Codegen" calculationsBitwise_BytecodeResult (createPrg(calculationsBitwise_AbsSyntaxTypedResult)))
test_calculationsBitwise_complete  = TestCase (assertEqual "complete compiler"  calculationsBitwise_BytecodeResult (compiler(calculationsBitwiseClass)))
allTests_calculationsBitwise = TestList [test_calculationsBitwise_AbsSyntax, test_calculationsBitwise_SemanticCheck, test_calculationsBitwise_Codegen, test_calculationsBitwise_complete]

calculations_doTests = do
    print "Testcase 10: Testing calculations (FiedlerTestCalculations.hs)"
    --print (compiler(calculationsMathCla))
    print "Test 10.1: Normal Addition"
    runTestTT allTests_calculationsMath
    --print (createPrg(calculationsMath_AbsSyntaxTypedResult))
    print "Test 10.2: All arithmetic operations"
    runTestTT allTests_calculationsMath2
    --print (createPrg(calculationsMath2_AbsSyntaxTypedResult))
    print "Test 10.3: expressions with brackets"
    runTestTT allTests_calculationsMathWithBrackets
    --print (createPrg(calculationsMathWithBrackets_AbsSyntaxTypedResult))
    print "Test 10.4: bitwise operators"
    runTestTT allTests_calculationsBitwise
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")