module FiedlerSemCheckTestFalseConstructor where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile
import Control.Exception

import TestTools

{-
        Testcase 24: FalseConstructor
        Autor: Dominik Sauerer
        Beschreibung: Semantikcheck-Test auf "falsche" Konstruktoren (Konstruktoren, die den falschen Klassennamen haben)
        Testumfang: Parser, Semantikcheck
-}

falseConstructorClass = "public class FalseConstructor {\
  \  private int x;\
  \  public HereIsTheError() {\
   \     x = 666;\
  \  }\
\}"

falseConstructor_AbsSyntaxResult = [Class(Just Public, "FalseConstructor", ClassBody([
    FieldDecl(Just Private, "int", "x")], [
    ConstructorDecl(Just Public, "HereIsTheError", [], StmtExprStmt(Assign("x", Integer(666))))
    ]))]


test_falseConstructor_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseConstructor_AbsSyntaxResult (parser(falseConstructorClass)))
test_falseConstructor_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseConstructor_AbsSyntaxResult))
allTests_falseConstructor = TestList [test_falseConstructor_AbsSyntax, test_falseConstructor_SemanticCheck]

falseConstructor_doTests = do
    print "Testcase 24: Testing exception, when constructor with false name is declared (FiedlerSemCheckTestFalseConstructor.hs)"
    runTestTT allTests_falseConstructor
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")