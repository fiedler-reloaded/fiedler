module FiedlerTestNull where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 17: Null
        Autor: Dominik Sauerer
        Beschreibung: Test von null
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 17.1
        Autor: Dominik Sauerer
        Beschreibung: Test von null bei Zuweisungen auf Instanzvariablen
        Testumfang: gesamter Compiler
-}

correctNullClass = "public class Null {\
   \ public Helper helper;\
   \ public int x;\
  \  public void correctNull() {\
  \      x = 4;\
  \      helper = null;\
 \   }\
\}"

correctNull_AbsSyntaxResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "void", "correctNull", [], Block([
        StmtExprStmt(Assign("x", Integer(4))),
        StmtExprStmt(Assign("helper", Jnull))
    ]))
    ]))]

correctNull_AbsSyntaxTypedResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "void", "correctNull", [], TypedStmt(Block([
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(4), "int")), "void")), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(Jnull, "NullType")), "void")), "void")
    ]), "void"))
    ]))]

correctNull_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 19, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 4, index_nameandtype_cp = 15, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 4, index_nameandtype_cp = 16, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 17, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "LHelper;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "x", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "correctNull", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 6, index_descr_cp = 7, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Null", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 4}, 
    super = SuperClass {index_sp = 5}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 2, 
    array_fields = [Field_Info {af_fi = AccessFlags [1], index_name_fi = 6, index_descr_fi = 7, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [1], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 13, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 23, len_stack_attr = 2, len_local_attr = 1, tam_code_attr = 11, array_code_attr = [42,7,181,0,2,42,1,181,0,3,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0, 
    array_attributes = []}]

test_correctNull_AbsSyntax = TestCase (assertEqual "Abstract Syntax" correctNull_AbsSyntaxResult (parser(correctNullClass)))
test_correctNull_SemanticCheck = TestCase (assertEqual "Semantic check" correctNull_AbsSyntaxTypedResult (typecheckPrg(correctNull_AbsSyntaxResult)))
test_correctNull_Codegen = TestCase (assertEqual "Codegen" correctNull_BytecodeResult (createPrg(correctNull_AbsSyntaxTypedResult)))
test_correctNull_complete  = TestCase (assertEqual "complete compiler"  correctNull_BytecodeResult (compiler(correctNullClass)))
allTests_correctNull = TestList [test_correctNull_AbsSyntax, test_correctNull_SemanticCheck, test_correctNull_Codegen, test_correctNull_complete]




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 17.2
        Autor: Dominik Sauerer
        Beschreibung: Test von null bei Zuweisungen auf Variablen elementarer Typen. Soll Exception werfen
        Testumfang: Parser, Semantikcheck
-}

falseNullClass = "public class Null {\
   \ public Helper helper;\
   \ public int x;\
  \ public void falseNull() {\
 \       x = null;\
 \   }\
\}"

falseNull_AbsSyntaxResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "void", "falseNull", [], StmtExprStmt(Assign("x", Jnull)))
    ]))]

test_falseNull_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseNull_AbsSyntaxResult (parser(falseNullClass)))
test_falseNull_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseNull_AbsSyntaxResult))
allTests_falseNull = TestList [test_falseNull_AbsSyntax, test_falseNull_SemanticCheck]





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 17.3
        Autor: Dominik Sauerer
        Beschreibung: Test von null als Rückgabewerte von Methoden
        Testumfang: Parser, Semantikcheck
-}

returnNullClass = "public class Null {\
   \ public Helper helper;\
   \ public int x;\
  \  public Helper returnNull() {\
      \  return null;\
  \  }\
\}"

compareNullClass = "public class Null {\
   \ public Helper helper;\
   \ public int x;\
  \  public int nullInCompare() {\
     \   int x;\
     \   if(helper == null) {\
        \    x = 5;\
       \ }\
      \  if (helper != null) {\
            \x = 6;\
        \}\
     \   return x;\
  \  }\
\}"

returnNull_AbsSyntaxResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "Helper", "returnNull", [], Return(Jnull))
    ]))]

returnNull_AbsSyntaxTypedResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "Helper", "returnNull", [], TypedStmt(Return(TypedExpr(Jnull, "NullType")), "NullType"))
    ]))]

test_returnNull_AbsSyntax = TestCase (assertEqual "Abstract Syntax" returnNull_AbsSyntaxResult (parser(returnNullClass)))
test_returnNull_SemanticCheck = TestCase (assertEqual "Semantic check" returnNull_AbsSyntaxTypedResult (typecheckPrg(returnNull_AbsSyntaxResult)))
allTests_returnNull = TestList [test_returnNull_AbsSyntax, test_returnNull_SemanticCheck]





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 17.4
        Autor: Dominik Sauerer
        Beschreibung: Test von null als Vergleichswerte
        Testumfang: Parser, Semantikcheck
-}

compareNull_AbsSyntaxResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "int", "nullInCompare", [], Block([
        LocalVarDecl("int", "x"),
        If(Binary("==", LocalOrFieldVar("helper"), Jnull), 
            StmtExprStmt(Assign("x", (Integer(5)))), Nothing),
        If(Binary("!=", LocalOrFieldVar("helper"), Jnull), 
            StmtExprStmt(Assign("x", (Integer(6)))), Nothing),
        Return(LocalOrFieldVar("x"))    
    ]))
    ]))]

compareNull_AbsSyntaxTypedResult = [Class(Just Public, "Null", ClassBody([
    FieldDecl(Just Public, "Helper", "helper"),
    FieldDecl(Just Public, "int", "x")
    ], [
    MethodDecl(Just Public, "int", "nullInCompare", [], TypedStmt(Block([
        TypedStmt(LocalVarDecl("int", "x"), "void"),
        TypedStmt(If(TypedExpr(Binary("==", TypedExpr(LocalOrFieldVar("helper"), "Helper"), TypedExpr(Jnull, "NullType")), "boolean"), 
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", (TypedExpr(Integer(5), "int"))), "void")), "void"), Nothing), "void"),
        TypedStmt(If(TypedExpr(Binary("!=", TypedExpr(LocalOrFieldVar("helper"), "Helper"), TypedExpr(Jnull, "NullType")), "boolean"), 
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", (TypedExpr(Integer(6), "int"))), "void")), "void"), Nothing), "void"),
        TypedStmt(Return(TypedExpr(LocalOrFieldVar("x"), "int")), "int")    
    ]), "int"))
    ]))]

test_compareNull_AbsSyntax = TestCase (assertEqual "Abstract Syntax" compareNull_AbsSyntaxResult (parser(compareNullClass)))
test_compareNull_SemanticCheck = TestCase (assertEqual "Semantic check" compareNull_AbsSyntaxTypedResult (typecheckPrg(compareNull_AbsSyntaxResult)))
allTests_compareNull = TestList [test_compareNull_AbsSyntax, test_compareNull_SemanticCheck]




null_doTests = do
    print "Testcase 17: Testing Java null (FiedlerTestNull.hs)"
    --print (compiler(calculationsClass))
    print "Test 17.1: correct use of null"
    runTestTT allTests_correctNull
    print "Test 17.2: incorrect use of null at int type"
    runTestTT allTests_falseNull
    print "Test 17.3: can return null for complex type"
    runTestTT allTests_returnNull
    print "Test 17.4: can compare instances with null"
    runTestTT allTests_compareNull
    --print (typecheckPrg(falseNull_AbsSyntaxResult))
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")