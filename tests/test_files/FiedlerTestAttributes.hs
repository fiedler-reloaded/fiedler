module FiedlerTestAttributes where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck

import TestTools

{-
        Testcase 2: Attributes
        Autor: Dominik Sauerer
        Beschreibung: Test einer einfachen Klasse mit drei Attributen mit primitiven Typen
        Testumfang: gesamter Compiler
-}

attributesClass = "public class Attributes { \
   \ public int x;\
   \ private String name;\
  \  protected boolean abc;\
\ }"

attributes_AbsSyntaxResult = [Class(Just Public, "Attributes",
    ClassBody([
        FieldDecl(Just Public, "int", "x"),
        FieldDecl(Just Private, "String", "name"),
        FieldDecl(Just Protected, "boolean", "abc")], 
    []))]



attributes_AbsSyntaxTypedResult = [Class(Just Public, "Attributes",
    ClassBody([
        FieldDecl(Just Public, "int", "x"),
        FieldDecl(Just Private, "String", "name"),
        FieldDecl(Just Protected, "boolean", "abc")], 
    []))]

attributes_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 16, 
    array_cp = [
        MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 13, desc = ""},
        Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},
        Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "x", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "name", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 18, cad_cp = "Ljava/lang/String;", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "abc", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "Z", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
        NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "Attributes", desc = ""},
        Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 3,
    array_fields = [Field_Info {af_fi = AccessFlags [1], index_name_fi = 4, index_descr_fi = 5, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [2], index_name_fi = 6, index_descr_fi = 7, tam_fi = 0, array_attr_fi = []},Field_Info {af_fi = AccessFlags [4], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}], count_methods = 1, array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_Attributes_AbsSyntax = TestCase (assertEqual "Abstract Syntax" (parser(attributesClass)) attributes_AbsSyntaxResult)
test_Attributes_SemanticCheck = TestCase (assertEqual "Semantic check" attributes_AbsSyntaxTypedResult (typecheckPrg(attributes_AbsSyntaxResult)))
test_Attributes_complete  = TestCase (assertEqual "complete compiler"  attributes_BytecodeResult (compiler(attributesClass)))
allTests_Attributes = TestList [test_Attributes_AbsSyntax, test_Attributes_SemanticCheck, test_Attributes_complete]

attributes_doTests = do
    print "Testcase 2: Testing Attributes (FiedlerTestAttributes.hs)"
    runTestTT allTests_Attributes
    --print(compiler(attributesClass))
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")