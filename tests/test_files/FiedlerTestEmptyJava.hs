module FiedlerTestEmptyJava where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat

import TestTools

{-
    Testcase 1: emptyJava
    Autor: Dominik Sauerer
    Beschreibung: Test einer einfachen, leeren Klasse ohne Attribute/Methoden
    Testumfang: gesamter Compiler
-}

emptyJavaClass = "public class EmptyJava { \
\ \
\ }"

emptyJava_AbsSyntaxResult = [Class(Just Public, "EmptyJava", ClassBody([], []))]

emptyJava_AbsSyntaxTypedResult = [Class(Just Public, "EmptyJava", ClassBody([], []))]

emptyJava_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 10, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "EmptyJava", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 1, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0, 
    array_attributes = []}]


test_EmptyJava_AbsSyntax = TestCase (assertEqual "Abstract Syntax" (parser(emptyJavaClass)) emptyJava_AbsSyntaxResult)
test_EmptyJava_complete  = TestCase (assertEqual "Semantic check" emptyJava_BytecodeResult (compiler(emptyJavaClass)))
allTests_EmptyJava = TestList [test_EmptyJava_AbsSyntax, test_EmptyJava_complete]

emptyJava_doTests = do
    print "Testcase 1: Testing EmptyJava (FiedlerTestEmptyJava.hs)"
    runTestTT allTests_EmptyJava
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")