module FiedlerTestVoidMethod where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 4: VoidMethod
        Autor: Dominik Sauerer
        Beschreibung: Test einer einfachen Klasse mit zwei void-Methoden, die nichts machen
        Testumfang: gesamter Compiler
-}

voidMethodClass = "public class VoidMethod { \
   \ public void doNothing(){\
   \    }\
  \  private void x(){}\
\ }"

voidMethod_AbsSyntaxResult = [Class(Just Public, "VoidMethod", ClassBody([], [
        MethodDecl(Just Public, "void", "doNothing", [], Empty),
        MethodDecl(Just Private, "void", "x", [], Empty)
        ]))
        ]

voidMethod_AbsSyntaxTypedResult = [Class(Just Public, "VoidMethod", ClassBody([], [
        MethodDecl(Just Public, "void", "doNothing", [], TypedStmt(Empty, "void")),
        MethodDecl(Just Private, "void", "x", [], TypedStmt(Empty, "void"))
        ]))
        ]

voidMethod_BytecodeResult = [ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 12,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "doNothing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "x", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "VoidMethod", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 2},
        super = SuperClass {index_sp = 3},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 0,
        array_fields = [],
        count_methods = 3,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [2], index_name_mi = 8, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], count_attributes = 0,
        array_attributes = []}]

test_voidMethod_AbsSyntax = TestCase (assertEqual "Abstract Syntax" voidMethod_AbsSyntaxResult (parser(voidMethodClass)))
test_voidMethod_SemanticCheck = TestCase (assertEqual "Semantic check" voidMethod_AbsSyntaxTypedResult (typecheckPrg(voidMethod_AbsSyntaxResult)))
test_voidMethod_Codegen = TestCase (assertEqual "Codegen" voidMethod_BytecodeResult (createPrg(voidMethod_AbsSyntaxTypedResult)))
test_voidMethod_complete  = TestCase (assertEqual "complete compiler"  voidMethod_BytecodeResult (compiler(voidMethodClass)))
allTests_voidMethod = TestList [test_voidMethod_AbsSyntax, test_voidMethod_SemanticCheck, test_voidMethod_Codegen, test_voidMethod_complete]

voidMethod_doTests = do
    print "Testcase 4: Testing Void Methods (FiedlerTestVoidMethod.hs)"
    --print (compiler(voidMethodClass))
    runTestTT allTests_voidMethod
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")