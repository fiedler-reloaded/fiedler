module FiedlerTestMethodWithParam where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 5: MethodWithParam
        Autor: Dominik Sauerer
        Beschreibung: Test einer Klasse mit Methoden, die jeweils 1+ Parameter haben
        Testumfang: gesamter Compiler
-}

methodWithParamClass = "public class MethodWithParam { \
  \  protected boolean example(int i) {\
    \    return true;\
 \   }\
    \ \
   \ public int another(boolean x, int v) {\
  \      return 1;\
   \ }\
\}"

methodWithParam_AbsSyntaxResult = [Class(Just Public, "MethodWithParam", ClassBody([], [
        MethodDecl(Just Protected, "boolean", "example", [("int", "i")], Return(Bool(True))),
        MethodDecl(Just Public, "int", "another", [("boolean", "x"), ("int", "v") ], Return(Integer(1)))
        ]))]
        

methodWithParam_AbsSyntaxTypedResult = [Class(Just Public, "MethodWithParam", ClassBody([], [
        MethodDecl(Just Protected, "boolean", "example", [("int", "i")], TypedStmt(Return(TypedExpr(Bool(True), "boolean")), "boolean")),
        MethodDecl(Just Public, "int", "another", [("boolean", "x"), ("int", "v")], TypedStmt(Return(TypedExpr(Integer(1), "int")), "int"))
        ]))]
        

methodWithParam_BytecodeResult = [ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 14,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 11, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "example", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(I)Z", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "another", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "(ZI)I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 15, cad_cp = "MethodWithParam", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 2},
        super = SuperClass {index_sp = 3},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 0,
        array_fields = [],
        count_methods = 3,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [4], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 14, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 2, array_code_attr = [4,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 9, index_descr_mi = 10, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 14, len_stack_attr = 1, len_local_attr = 3, tam_code_attr = 2, array_code_attr = [28,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

test_methodWithParam_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodWithParam_AbsSyntaxResult (parser(methodWithParamClass)))
test_methodWithParam_SemanticCheck = TestCase (assertEqual "Semantic check" methodWithParam_AbsSyntaxTypedResult (typecheckPrg(methodWithParam_AbsSyntaxResult)))
test_methodWithParam_Codegen = TestCase (assertEqual "Codegen" methodWithParam_BytecodeResult (createPrg(methodWithParam_AbsSyntaxTypedResult)))
test_methodWithParam_complete  = TestCase (assertEqual "complete compiler"  methodWithParam_BytecodeResult (compiler(methodWithParamClass)))
allTests_methodWithParam = TestList [test_methodWithParam_AbsSyntax, test_methodWithParam_SemanticCheck, test_methodWithParam_Codegen, test_methodWithParam_complete]

methodWithParam_doTests = do
    print "Testcase 5: Testing Methods with Parameters (FiedlerTestMethodWithParam.hs)"
    --print (compiler(methodWithParamClass))
    runTestTT allTests_methodWithParam
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")