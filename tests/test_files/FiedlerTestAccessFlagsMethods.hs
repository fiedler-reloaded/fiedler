module FiedlerTestAccessFlagsMethods where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 21: AccessFlagsMethods
        Autor: Dominik Sauerer
        Beschreibung: Test von Access flags bei Methoden
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 21.1
        Autor: Dominik Sauerer
        Beschreibung: public => Test soll funktionieren
        Testumfang: gesamter Compiler
-}

accessFlagsMethodsTest1Class = "public class AccessFlagsMethods {\
\    Helper helper;\
 \   public AccessFlagsMethods() {\
 \       helper = new Helper();\
 \   }\
 \   public char test1() {\
 \       return helper.doAnotherThing('p');\
 \   }\
\}\
\\
\public class Helper {\
  \  private void doSomething() {\
\\
   \ }\
  \  protected int doIt(int x) {\
  \      return 1;\
 \   }\
 \   public char doAnotherThing(char z) {\
 \       return z;\
 \   }\
\}"

accessFlagsMethodsTest1_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([], [
    MethodDecl(Just Private, "void", "doSomething", [], Empty),
    MethodDecl(Just Protected, "int", "doIt", [("int", "x")], Return(Integer(1))),
    MethodDecl(Just Public, "char", "doAnotherThing", [("char", "z")], Return(LocalOrFieldVar("z")))
    ])),
    Class(Just Public, "AccessFlagsMethods", ClassBody([
        FieldDecl(Nothing, "Helper", "helper")
    ], [
        ConstructorDecl(Just Public, "AccessFlagsMethods", [], StmtExprStmt(Assign("helper", StmtExprExpr(New("Helper", []))))),
        MethodDecl(Just Public, "char", "test1", [], 
            Return(StmtExprExpr(MethodCall(LocalOrFieldVar("helper"), "doAnotherThing", [Char('p')]))))
        ]))]

accessFlagsMethodsTest1_AbsSyntaxTypedResult = [Class(Just Public, "Helper", ClassBody([], [
    MethodDecl(Just Private, "void", "doSomething", [], TypedStmt(Empty, "void")),
    MethodDecl(Just Protected, "int", "doIt", [("int", "x")], TypedStmt(Return(TypedExpr(Integer(1), "int")), "int")),
    MethodDecl(Just Public, "char", "doAnotherThing", [("char", "z")], TypedStmt(Return(TypedExpr(LocalOrFieldVar("z"), "char")), "char"))
    ])),
    Class(Just Public, "AccessFlagsMethods", ClassBody([
        FieldDecl(Nothing, "Helper", "helper")
    ], [
        ConstructorDecl(Just Public, "AccessFlagsMethods", [], TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(StmtExprExpr(TypedStmtExpr(New("Helper", []), "Helper")), "Helper")), "void")), "void")),
        MethodDecl(Just Public, "char", "test1", [], 
            TypedStmt(Return(TypedExpr(StmtExprExpr(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "doAnotherThing", [TypedExpr(Char('p'), "char")]), "char")), "char")), "char"))
        ]))]

accessFlagsMethodsTest1_BytecodeResult = [
    ClassFile {
        magic = Magic, 
        minver = MinorVersion {numMinVer = 0}, 
        maxver = MajorVersion {numMaxVer = 49}, 
        count_cp = 15, 
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "doIt", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(I)I", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(C)C", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "Helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
        acfg = AccessFlags [1,32], 
        this = ThisClass {index_th = 2}, 
        super = SuperClass {index_sp = 3}, 
        count_interfaces = 0, 
        array_interfaces = [], 
        count_fields = 0, 
        array_fields = [], 
        count_methods = 4, 
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [2], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [4], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 14, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 2, array_code_attr = [4,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 14, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 2, array_code_attr = [27,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], count_attributes = 0, 
        array_attributes = []},
    ClassFile {
        magic = Magic, 
        minver = MinorVersion {numMinVer = 0}, 
        maxver = MajorVersion {numMaxVer = 49}, 
        count_cp = 23, 
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 7, index_nameandtype_cp = 15, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 16, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 15, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 6, index_nameandtype_cp = 17, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 18, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 19, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 20, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "LHelper;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "test1", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()C", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "Helper", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 21, index_descr_cp = 22, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 18, cad_cp = "AccessFlagsMethods", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(C)C", desc = ""}], 
        acfg = AccessFlags [1,32], 
        this = ThisClass {index_th = 6}, 
        super = SuperClass {index_sp = 7}, 
        count_interfaces = 0, 
        array_interfaces = [], 
        count_fields = 1, 
        array_fields = [Field_Info {af_fi = AccessFlags [], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}], 
        count_methods = 2, 
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 28, len_stack_attr = 3, len_local_attr = 1, tam_code_attr = 16, array_code_attr = [42,183,0,1,42,187,0,2,89,183,0,3,181,0,4,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 13, index_descr_mi = 14, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 22, len_stack_attr = 2, len_local_attr = 1, tam_code_attr = 10, array_code_attr = [42,180,0,4,16,112,182,0,5,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
        count_attributes = 0, 
        array_attributes = []}]
    
test_accessFlagsMethodsTest1_AbsSyntax = TestCase (assertEqual "Abstract Syntax" accessFlagsMethodsTest1_AbsSyntaxResult (parser(accessFlagsMethodsTest1Class)))
test_accessFlagsMethodsTest1_SemanticCheck = TestCase (assertEqual "Semantic check" accessFlagsMethodsTest1_AbsSyntaxTypedResult (typecheckPrg(accessFlagsMethodsTest1_AbsSyntaxResult)))
test_accessFlagsMethodsTest1_Codegen = TestCase (assertEqual "Codegen" accessFlagsMethodsTest1_BytecodeResult (createPrg(accessFlagsMethodsTest1_AbsSyntaxTypedResult)))
test_accessFlagsMethodsTest1_complete  = TestCase (assertEqual "complete compiler"  accessFlagsMethodsTest1_BytecodeResult (compiler(accessFlagsMethodsTest1Class)))
allTests_accessFlagsMethodsTest1 = TestList [test_accessFlagsMethodsTest1_AbsSyntax, test_accessFlagsMethodsTest1_SemanticCheck, test_accessFlagsMethodsTest1_Codegen, test_accessFlagsMethodsTest1_complete]







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 21.2
        Autor: Dominik Sauerer
        Beschreibung: private => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

accessFlagsMethodsTest2NotWorkingClass = "public class AccessFlagsMethods {\
\    Helper helper;\
 \   public AccessFlagsMethods() {\
 \       helper = new Helper();\
 \   }\
 \   public void test2_NotWorking() { \
     \   helper.doSomething();\
   \ }\
\}\
\\
\public class Helper {\
  \  private void doSomething() {\
\\
   \ }\
  \  protected int doIt(int x) {\
  \      return 1;\
 \   }\
 \   public char doAnotherThing(char z) {\
 \       return z;\
 \   }\
\}"

accessFlagsMethodsTest2NotWorking_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([], [
    MethodDecl(Just Private, "void", "doSomething", [], Empty),
    MethodDecl(Just Protected, "int", "doIt", [("int", "x")], Return(Integer(1))),
    MethodDecl(Just Public, "char", "doAnotherThing", [("char", "z")], Return(LocalOrFieldVar("z")))
    ])),
    Class(Just Public, "AccessFlagsMethods", ClassBody([
        FieldDecl(Nothing, "Helper", "helper")
    ], [
        ConstructorDecl(Just Public, "AccessFlagsMethods", [], StmtExprStmt(Assign("helper", StmtExprExpr(New("Helper", []))))),
        MethodDecl(Just Public, "void", "test2_NotWorking", [], 
            StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "doSomething", [])))
        ]))]

test_accessFlagsMethodsTest2NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" accessFlagsMethodsTest2NotWorking_AbsSyntaxResult (parser(accessFlagsMethodsTest2NotWorkingClass)))
test_accessFlagsMethodsTest2NotWorking_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(accessFlagsMethodsTest2NotWorking_AbsSyntaxResult))
allTests_accessFlagsMethodsTest2NotWorking = TestList [test_accessFlagsMethodsTest2NotWorking_AbsSyntax, test_accessFlagsMethodsTest2NotWorking_SemanticCheck]





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 21.3
        Autor: Dominik Sauerer
        Beschreibung: protected => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

accessFlagsMethodsTest3NotWorkingClass = "public class AccessFlagsMethods {\
\    Helper helper;\
 \   public AccessFlagsMethods() {\
 \       helper = new Helper();\
 \   }\
 \   public int test3_NotWorking() {\
    \    return helper.doIt(4);\
  \  }\
\}\
\\
\public class Helper {\
  \  private void doSomething() {\
\\
   \ }\
  \  protected int doIt(int x) {\
  \      return 1;\
 \   }\
 \   public char doAnotherThing(char z) {\
 \       return z;\
 \   }\
\}"

accessFlagsMethodsTest3NotWorking_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([], [
    MethodDecl(Just Private, "void", "doSomething", [], Empty),
    MethodDecl(Just Protected, "int", "doIt", [("int", "x")], Return(Integer(1))),
    MethodDecl(Just Public, "char", "doAnotherThing", [("char", "z")], Return(LocalOrFieldVar("z")))
    ])),
    Class(Just Public, "AccessFlagsMethods", ClassBody([
        FieldDecl(Nothing, "Helper", "helper")
    ], [
        ConstructorDecl(Just Public, "AccessFlagsMethods", [], StmtExprStmt(Assign("helper", StmtExprExpr(New("Helper", []))))),
        MethodDecl(Just Public, "int", "test3_NotWorking", [], 
            Return(StmtExprExpr(MethodCall(LocalOrFieldVar("helper"), "doIt", [(Integer(4))]))))
        ]))]

test_accessFlagsMethodsTest3NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" accessFlagsMethodsTest3NotWorking_AbsSyntaxResult (parser(accessFlagsMethodsTest3NotWorkingClass)))
test_accessFlagsMethodsTest3NotWorking_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(accessFlagsMethodsTest3NotWorking_AbsSyntaxResult))
allTests_accessFlagsMethodsTest3NotWorking = TestList [test_accessFlagsMethodsTest3NotWorking_AbsSyntax, test_accessFlagsMethodsTest3NotWorking_SemanticCheck]





accessFlagsMethods_doTests = do
    print "Testcase 21: Testing correct effect of access flags in methods (FiedlerTestAccessFlagsMethods.hs)"
    print "Test 21.1: Test of public flag (should work)"
    runTestTT allTests_accessFlagsMethodsTest1
    print "Test 21.2: Test of private flag (should not work)"
    runTestTT allTests_accessFlagsMethodsTest2NotWorking
    print "Test 21.3: Test of protected flag (should not work)"
    runTestTT allTests_accessFlagsMethodsTest3NotWorking
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")