module FiedlerTestIfElse where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 7: IfElse
        Autor: Dominik Sauerer
        Beschreibung: Test von Methoden mit if-else-Konstrukten sowie if-elseif-else-Konstrukten
        Testumfang: gesamter Compiler
-}

ifElseClass = "public class IfElse { \
\    public void ifE() {\
 \       int i;\
 \       if(true == true) {\
 \           i = 5;\
 \       }else{\
 \           i = 9;\
 \       }\
 \   }\
 \   public void ifIEE() {\
 \       int i;\
  \      if(true == true) {\
 \           i = 5;\
 \       }else if(true == false){\
 \           i = 9;\
 \       }else {\
 \           i = 10;\
 \       }\
 \   }\
\}"

ifElse_AbsSyntaxResult = [
    Class(Just Public, "IfElse", ClassBody(
        [], [MethodDecl(
            Just Public, "void", "ifE", [], Block([
                LocalVarDecl("int", "i"),
                If(Binary("==", Bool(True), Bool(True)), StmtExprStmt(Assign("i", Integer(5))), Just (StmtExprStmt(Assign("i", Integer(9)))))
            ])
        ), MethodDecl(
            Just Public, "void", "ifIEE", [], Block([
                LocalVarDecl("int", "i"),
                ElseIf(Binary("==", Bool(True), Bool(True)), StmtExprStmt(Assign("i", Integer(5))), [
                    (Binary("==", Bool(True), Bool(False)), StmtExprStmt(Assign("i", Integer(9))))
                ], Just (StmtExprStmt(Assign("i", Integer(10)))))
            ])
        )]
    ))
    ]
        

ifElse_AbsSyntaxTypedResult = [
    Class(Just Public, "IfElse", ClassBody(
        [], [MethodDecl(
            Just Public, "void", "ifE", [], TypedStmt(Block([
                TypedStmt(LocalVarDecl("int", "i"), "void"),
                TypedStmt(If(
                    TypedExpr(Binary("==", TypedExpr(Bool(True), "boolean"), TypedExpr(Bool(True), "boolean")), "boolean"),
                    TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("i", TypedExpr(Integer(5), "int")), "void")), "void"),
                    Just (TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("i", TypedExpr(Integer(9), "int")), "void")), "void")
                )), "void")
            ]), "void")
        ), MethodDecl(
            Just Public, "void", "ifIEE", [], TypedStmt(Block([
                TypedStmt(LocalVarDecl("int", "i"), "void"),
                TypedStmt(ElseIf(
                    TypedExpr(Binary("==", TypedExpr(Bool(True), "boolean"), TypedExpr(Bool(True), "boolean")), "boolean"),
                    TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("i", TypedExpr(Integer(5), "int")), "void")), "void"), [
                    (TypedExpr(Binary("==", TypedExpr(Bool(True), "boolean"), TypedExpr(Bool(False), "boolean")), "boolean"),
                    TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("i", TypedExpr(Integer(9), "int")), "void")), "void"))], 
                    Just (TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("i", TypedExpr(Integer(10), "int")), "void")), "void")
                )), "void")
            ]), "void")
        )]
    ))
    ]       

ifElse_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 12,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "ifE", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "ifIEE", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "IfElse", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 3,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 3, array_code_attr = [8,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 3, array_code_attr = [8,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_ifElse_AbsSyntax = TestCase (assertEqual "Abstract Syntax" ifElse_AbsSyntaxResult (parser(ifElseClass)))
test_ifElse_SemanticCheck = TestCase (assertEqual "Semantic check" ifElse_AbsSyntaxTypedResult (typecheckPrg(ifElse_AbsSyntaxResult)))
test_ifElse_Codegen = TestCase (assertEqual "Codegen" ifElse_BytecodeResult (createPrg(ifElse_AbsSyntaxTypedResult)))
test_ifElse_complete  = TestCase (assertEqual "complete compiler"  ifElse_BytecodeResult (compiler(ifElseClass)))
allTests_ifElse = TestList [test_ifElse_AbsSyntax, test_ifElse_SemanticCheck, test_ifElse_Codegen, test_ifElse_complete]

ifElse_doTests = do
    print "Testcase 7: Testing if else (FiedlerTestIfElse.hs)"
    --print (createPrg(ifElse_AbsSyntaxTypedResult))
    runTestTT allTests_ifElse
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")