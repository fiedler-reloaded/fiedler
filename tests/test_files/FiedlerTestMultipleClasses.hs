module FiedlerTestMultipleClasses where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 6: MultipleClasses
        Autor: Dominik Sauerer
        Beschreibung: Test einer Datei mit mehreren Klassen
        Testumfang: Parser, Semantikcheck
-}

multipleClassesClass = "public class MultipleClasses { \
\    private int x;\
\}\
\\
\public class otherClass {\
 \   \
\}\
\\
\public class yetAnotherClass {\
\    public boolean myboolean;\
\}"

multipleClasses_AbsSyntaxResult = [
    Class(
        Just Public, "yetAnotherClass", ClassBody(
            [FieldDecl(Just Public, "boolean", "myboolean")],
            []
        )
    ),
    Class(
        Just Public, "otherClass", ClassBody(
            [],
            []
        )
    ),
    Class(
        Just Public, "MultipleClasses", ClassBody(
            [FieldDecl(Just Private, "int", "x")],
            []
        )
    )
    ]

multipleClasses_AbsSyntaxTypedResult = [
    Class(
        Just Public, "yetAnotherClass", ClassBody(
            [FieldDecl(Just Public, "boolean", "myboolean")],
            []
        )
    ),
    Class(
        Just Public, "otherClass", ClassBody(
            [],
            []
        )
    ),
    Class(
        Just Public, "MultipleClasses", ClassBody(
            [FieldDecl(Just Private, "int", "x")],
            []
        )
    )
    ]

multipleClasses_BytecodeResult = []

test_multipleClasses_AbsSyntax = TestCase (assertEqual "Abstract Syntax" multipleClasses_AbsSyntaxResult (parser(multipleClassesClass)))
test_multipleClasses_SemanticCheck = TestCase (assertEqual "Semantic check" multipleClasses_AbsSyntaxTypedResult (typecheckPrg(multipleClasses_AbsSyntaxResult)))
test_multipleClasses_Codegen = TestCase (assertEqual "Codegen" multipleClasses_BytecodeResult (createPrg(multipleClasses_AbsSyntaxTypedResult)))
test_multipleClasses_complete  = TestCase (assertEqual "complete compiler"  multipleClasses_BytecodeResult (compiler(multipleClassesClass)))
allTests_multipleClasses = TestList [test_multipleClasses_AbsSyntax, test_multipleClasses_SemanticCheck]

multipleClasses_doTests = do
    print "Testcase 6: Testing multiple classes (FiedlerTestMultipleClasses.hs)"
    runTestTT allTests_multipleClasses
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")