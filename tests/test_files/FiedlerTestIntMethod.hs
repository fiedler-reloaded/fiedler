module FiedlerTestIntMethod where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 3: IntMethod
        Autor: Dominik Sauerer
        Beschreibung: Test einer einfachen Klasse mit einer Int-Methode, die nur 0 zurückgibt
        Testumfang: gesamter Compiler
-}

intMethodClass = "public class IntMethod { \
    \private int getNumber() {\
   \     return 0;\
   \ }\
\}"

intMethod_AbsSyntaxResult = [Class(Just Public, "IntMethod", ClassBody([], [
        MethodDecl(Just Private, "int", "getNumber", [], Return(Integer(0)))]))]

intMethod_AbsSyntaxTypedResult = [Class(Just Public, "IntMethod", ClassBody([], [
        MethodDecl(Just Private, "int", "getNumber", [], 
            TypedStmt(Return(TypedExpr(Integer(0), "int")), "int"))]))]

intMethod_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 12,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "getNumber", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "IntMethod", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 2,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [2], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 14, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 2, array_code_attr = [3,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_intMethod_AbsSyntax = TestCase (assertEqual "Abstract Syntax" intMethod_AbsSyntaxResult (parser(intMethodClass)))
test_intMethod_SemanticCheck = TestCase (assertEqual "Semantic check" intMethod_AbsSyntaxTypedResult (typecheckPrg(intMethod_AbsSyntaxResult)))
test_intMethod_Codegen = TestCase (assertEqual "Codegen" intMethod_BytecodeResult (createPrg(intMethod_AbsSyntaxTypedResult)))
test_intMethod_complete  = TestCase (assertEqual "complete compiler"  intMethod_BytecodeResult (compiler(intMethodClass)))
allTests_intMethod = TestList [test_intMethod_AbsSyntax, test_intMethod_SemanticCheck, test_intMethod_Codegen, test_intMethod_complete]

intMethod_doTests = do
    print "Testcase 3: Testing Int Methods (FiedlerTestIntMethod.hs)"
    --print (compiler(intMethodClass))
    runTestTT allTests_intMethod
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")