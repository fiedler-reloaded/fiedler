module FiedlerTestThis where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 19: This
        Autor: Dominik Sauerer
        Beschreibung: Test von this in Zuweisungen, Methodenaufrufen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 19.1
        Autor: Dominik Sauerer
        Beschreibung: Test von this in Zuweisungen
        Testumfang: gesamter Compiler
-}

thisThisAssignClass = "public class ThisCl {\
\    private ThisCl instance;\
 \   public void method_thisAssign() {\
 \       instance = this;\
 \   }\
\}"

thisThisAssign_AbsSyntaxResult = [Class(Just Public, "ThisCl", ClassBody([
    FieldDecl(Just Private, "ThisCl", "instance")
    ], [
        MethodDecl(Just Public, "void", "method_thisAssign", [],  StmtExprStmt(Assign("instance", This))
        )]))]

thisThisAssign_AbsSyntaxTypedResult = [Class(Just Public, "ThisCl", ClassBody([
    FieldDecl(Just Private, "ThisCl", "instance")
    ], [
        MethodDecl(Just Public, "void", "method_thisAssign", [],  
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("instance", TypedExpr(This, "ThisCl")), "void")), "void")
        )]))]

thisThisAssign_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 15, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 4, index_nameandtype_cp = 11, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 3, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "instance", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "LThisCl;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 17, cad_cp = "method_thisAssign", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 5, index_descr_cp = 6, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "ThisCl", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 3}, 
    super = SuperClass {index_sp = 4}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 1, 
    array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 5, index_descr_fi = 6, tam_fi = 0, array_attr_fi = []}], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 18, len_stack_attr = 2, len_local_attr = 1, tam_code_attr = 6, array_code_attr = [42,42,181,0,2,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_thisThisAssign_AbsSyntax = TestCase (assertEqual "Abstract Syntax" thisThisAssign_AbsSyntaxResult (parser(thisThisAssignClass)))
test_thisThisAssign_SemanticCheck = TestCase (assertEqual "Semantic check" thisThisAssign_AbsSyntaxTypedResult (typecheckPrg(thisThisAssign_AbsSyntaxResult)))
test_thisThisAssign_Codegen = TestCase (assertEqual "Codegen" thisThisAssign_BytecodeResult (createPrg(thisThisAssign_AbsSyntaxTypedResult)))
test_thisThisAssign_complete  = TestCase (assertEqual "complete compiler"  thisThisAssign_BytecodeResult (compiler(thisThisAssignClass)))
allTests_thisThisAssign = TestList [test_thisThisAssign_AbsSyntax, test_thisThisAssign_SemanticCheck, test_thisThisAssign_Codegen, test_thisThisAssign_complete]





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 19.2
        Autor: Dominik Sauerer
        Beschreibung: Test von this in Methodenaufrufen
        Testumfang: gesamter Compiler
-}

thisThisCallClass = "public class ThisCl {\
 \   private ThisCl instance;\
 \   public void assign(ThisCl x) {\
 \       instance = x;\
  \  }\
  \  public void method_thisCall() {\
  \      this.assign(this);\
  \  }\
\}"

thisThisCall_AbsSyntaxResult = [Class(Just Public, "ThisCl", ClassBody([
    FieldDecl(Just Private, "ThisCl", "instance")
    ], [
        MethodDecl(Just Public, "void", "assign", [("ThisCl", "x")],  StmtExprStmt(Assign("instance", LocalOrFieldVar("x")))),
        MethodDecl(Just Public, "void", "method_thisCall", [], StmtExprStmt(MethodCall(This, "assign", [This])) )
        ]))]

thisThisCall_AbsSyntaxTypedResult = [Class(Just Public, "ThisCl", ClassBody([
    FieldDecl(Just Private, "ThisCl", "instance")
    ], [
        MethodDecl(Just Public, "void", "assign", [("ThisCl", "x")],  
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("instance", TypedExpr(LocalOrFieldVar("x"), "ThisCl")), "void")), "void")),
        MethodDecl(Just Public, "void", "method_thisCall", [], 
            TypedStmt(StmtExprStmt(TypedStmtExpr(MethodCall(TypedExpr(This, "ThisCl"), "assign", [TypedExpr(This, "ThisCl")]), "void")), "void"))
        ]))]

thisThisCall_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 19, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 4, index_nameandtype_cp = 15, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 4, index_nameandtype_cp = 16, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 17, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "instance", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "LThisCl;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "assign", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "(LThisCl;)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 15, cad_cp = "method_thisCall", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 6, index_descr_cp = 7, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 11, index_descr_cp = 12, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "ThisCl", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 4}, 
    super = SuperClass {index_sp = 5}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 1, 
    array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 6, index_descr_fi = 7, tam_fi = 0, array_attr_fi = []}], 
    count_methods = 3, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 10, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 11, index_descr_mi = 12, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 10, tam_len_attr = 18, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 6, array_code_attr = [42,43,181,0,2,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 13, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 10, tam_len_attr = 18, len_stack_attr = 2, len_local_attr = 1, tam_code_attr = 6, array_code_attr = [42,42,182,0,3,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_thisThisCall_AbsSyntax = TestCase (assertEqual "Abstract Syntax" thisThisCall_AbsSyntaxResult (parser(thisThisCallClass)))
test_thisThisCall_SemanticCheck = TestCase (assertEqual "Semantic check" thisThisCall_AbsSyntaxTypedResult (typecheckPrg(thisThisCall_AbsSyntaxResult)))
test_thisThisCall_Codegen = TestCase (assertEqual "Codegen" thisThisCall_BytecodeResult (createPrg(thisThisCall_AbsSyntaxTypedResult)))
test_thisThisCall_complete  = TestCase (assertEqual "complete compiler"  thisThisCall_BytecodeResult (compiler(thisThisCallClass)))
allTests_thisThisCall = TestList [test_thisThisCall_AbsSyntax, test_thisThisCall_SemanticCheck, test_thisThisCall_Codegen, test_thisThisCall_complete]




this_doTests = do
    print "Testcase 19: Testing this (FiedlerTestThis.hs)"
    --print (compiler(thisClass))
    print "Test 19.1: Testing this as variable"
    runTestTT allTests_thisThisAssign
    print "Test 19.2: Testing this in method calls"
    runTestTT allTests_thisThisCall
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")