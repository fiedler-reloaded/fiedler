module FiedlerTestMethodEndAfterIf where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 16: MethodEndAfterIf
        Autor: Dominik Sauerer
        Beschreibung: Test eines Methodenendes nach einem if-Konstrukt (Zusatztest wegen Fehler im Assemblercode)
        Testumfang: gesamter Compiler
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 16.1
        Autor: Dominik Sauerer
        Beschreibung: void-Methode
        Testumfang: gesamter Compiler
-}


methodEndAfterIfClass = "public class MethodEndAfterIf {\
  \  public void method(char x) {\
  \      if(x == '3') {\
  \          x = 'p';\
  \      } else {\
 \           x = 'o';\
 \       }\
\    }\
\}"

methodEndAfterIf_AbsSyntaxResult = [Class(Just Public, "MethodEndAfterIf", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [("char", "x")], 
            If(Binary("==", LocalOrFieldVar("x"), Char('3')), 
                StmtExprStmt(Assign("x", Char('p'))),
                Just(StmtExprStmt(Assign("x", Char('o')))))
        )]))]

methodEndAfterIf_AbsSyntaxTypedResult = [Class(Just Public, "MethodEndAfterIf", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [("char", "x")],
            TypedStmt(If(TypedExpr(Binary("==", TypedExpr(LocalOrFieldVar("x"), "char"), TypedExpr(Char('3'), "char")), "boolean"), 
                TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Char('p'), "char")), "void")), "void"),
                Just(TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Char('o'), "char")), "void")), "void"))), "void")
        )]))]

methodEndAfterIf_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 12, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "method", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(C)V", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "MethodEndAfterIf", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 28, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 16, array_code_attr = [27,16,51,160,0,9,16,112,60,167,0,6,16,111,60,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_methodEndAfterIf_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodEndAfterIf_AbsSyntaxResult (parser(methodEndAfterIfClass)))
test_methodEndAfterIf_SemanticCheck = TestCase (assertEqual "Semantic check" methodEndAfterIf_AbsSyntaxTypedResult (typecheckPrg(methodEndAfterIf_AbsSyntaxResult)))
test_methodEndAfterIf_Codegen = TestCase (assertEqual "Codegen" methodEndAfterIf_BytecodeResult (createPrg(methodEndAfterIf_AbsSyntaxTypedResult)))
test_methodEndAfterIf_complete  = TestCase (assertEqual "complete compiler"  methodEndAfterIf_BytecodeResult (compiler(methodEndAfterIfClass)))
allTests_methodEndAfterIf = TestList [test_methodEndAfterIf_AbsSyntax, test_methodEndAfterIf_SemanticCheck, test_methodEndAfterIf_Codegen, test_methodEndAfterIf_complete]



------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 16.2
        Autor: Dominik Sauerer
        Beschreibung: Methode mit Rückgabewert
        Testumfang: gesamter Compiler
-}

otherMethodEndAfterIfClass = "public class MethodEndAfterIf {\
  \  public char otherMethod(char x) {\
     \   if(x == '3') {\
      \      return 'p';\
     \   } else {\
    \        return 'o';\
    \    }\
 \   }\
\}"

otherMethodEndAfterIf_AbsSyntaxResult = [Class(Just Public, "MethodEndAfterIf", ClassBody([], [
        MethodDecl(Just Public, "char", "otherMethod", [("char", "x")], 
            If(Binary("==", LocalOrFieldVar("x"), Char('3')), 
                Return(Char('p')),
                Just(Return(Char('o'))))
        )]))]

otherMethodEndAfterIf_AbsSyntaxTypedResult = [Class(Just Public, "MethodEndAfterIf", ClassBody([], [
        MethodDecl(Just Public, "char", "otherMethod", [("char", "x")],
            TypedStmt(If(TypedExpr(Binary("==", TypedExpr(LocalOrFieldVar("x"), "char"), TypedExpr(Char('3'), "char")), "boolean"), 
                TypedStmt(Return(TypedExpr(Char('p'), "char")), "char"),
                Just(TypedStmt(Return(TypedExpr(Char('o'), "char")), "char"))), "char")
        )]))]

otherMethodEndAfterIf_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 12, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "otherMethod", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(C)C", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "MethodEndAfterIf", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 24, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 12, array_code_attr = [27,16,51,160,0,6,16,112,172,16,111,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_otherMethodEndAfterIf_AbsSyntax = TestCase (assertEqual "Abstract Syntax" otherMethodEndAfterIf_AbsSyntaxResult (parser(otherMethodEndAfterIfClass)))
test_otherMethodEndAfterIf_SemanticCheck = TestCase (assertEqual "Semantic check" otherMethodEndAfterIf_AbsSyntaxTypedResult (typecheckPrg(otherMethodEndAfterIf_AbsSyntaxResult)))
test_otherMethodEndAfterIf_Codegen = TestCase (assertEqual "Codegen" otherMethodEndAfterIf_BytecodeResult (createPrg(otherMethodEndAfterIf_AbsSyntaxTypedResult)))
test_otherMethodEndAfterIf_complete  = TestCase (assertEqual "complete compiler"  otherMethodEndAfterIf_BytecodeResult (compiler(otherMethodEndAfterIfClass)))
allTests_otherMethodEndAfterIf = TestList [test_otherMethodEndAfterIf_AbsSyntax, test_otherMethodEndAfterIf_SemanticCheck, test_otherMethodEndAfterIf_Codegen, test_otherMethodEndAfterIf_complete]




methodEndAfterIf_doTests = do
    print "Testcase 16: Testing method end after if (FiedlerTestMethodEndAfterIs.hs)"
    --print (compiler(methodEndAfterIfClass))
    print "Test 16.1: void-Method"
    runTestTT allTests_methodEndAfterIf
    print "Test 16.2: method with return value"
    runTestTT allTests_otherMethodEndAfterIf
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")