module FiedlerTestBreakContinue where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 15: BreakContinue 
        Autor: Dominik Sauerer
        Beschreibung: Test von break und continue in while-Schleifen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 15.1
        Autor: Dominik Sauerer
        Beschreibung: Test von break
        Testumfang: gesamter Compiler
-}

breakClass = "public class BreakContinue {\
   \ public void doBreak() {\
   \     boolean x;\
   \     x = true;\
   \     while(x) {\
  \          break;\
  \      }\
 \   }\
\}"

break_AbsSyntaxResult = [Class(Just Public, "BreakContinue", ClassBody([], [
        MethodDecl(Just Public, "void", "doBreak", [], Block([
            LocalVarDecl("boolean", "x"),
            StmtExprStmt(Assign("x", Bool(True))),
            While(LocalOrFieldVar("x"), Break)
        ]) )
        ]))]

break_AbsSyntaxTypedResult = [Class(Just Public, "BreakContinue", ClassBody([], [
        MethodDecl(Just Public, "void", "doBreak", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("boolean", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Bool(True), "boolean")), "void")), "void"),
            TypedStmt(While(TypedExpr(LocalOrFieldVar("x"), "boolean"), TypedStmt(Break, "void")), "void")
        ]) , "void"))
        ]))]

break_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 11, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "doBreak", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "BreakContinue", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3},
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 19, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 7, array_code_attr = [4,60,27,153,0,3,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_break_AbsSyntax = TestCase (assertEqual "Abstract Syntax" break_AbsSyntaxResult (parser(breakClass)))
test_break_SemanticCheck = TestCase (assertEqual "Semantic check" break_AbsSyntaxTypedResult (typecheckPrg(break_AbsSyntaxResult)))
test_break_Codegen = TestCase (assertEqual "Codegen" break_BytecodeResult (createPrg(break_AbsSyntaxTypedResult)))
test_break_complete  = TestCase (assertEqual "complete compiler"  break_BytecodeResult (compiler(breakClass)))
allTests_break = TestList [test_break_AbsSyntax, test_break_SemanticCheck, test_break_Codegen, test_break_complete]




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 15.2
        Autor: Dominik Sauerer
        Beschreibung: Test von continue
        Testumfang: gesamter Compiler
-}

continueClass = "public class BreakContinue {\
 \   public void doContinue() {\
 \       int x;\
 \       x = 0;\
   \     while(x < 2) {\
 \           x = x + 1;\
   \         continue;\
  \      }\
  \  }\
\}"

continue_AbsSyntaxResult = [Class(Just Public, "BreakContinue", ClassBody([], [
        MethodDecl(Just Public, "void", "doContinue", [], Block([
            LocalVarDecl("int", "x"),
            StmtExprStmt(Assign("x", Integer(0))),
            While(Binary("<", LocalOrFieldVar("x"), Integer(2)), Block([
                StmtExprStmt(Assign("x", Binary("+", LocalOrFieldVar("x"), Integer(1)))),
                Continue
            ]))
        ]) )
        ]))]

continue_AbsSyntaxTypedResult = [Class(Just Public, "BreakContinue", ClassBody([], [
        MethodDecl(Just Public, "void", "doContinue", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(0), "int")), "void")), "void"),
            TypedStmt(While(TypedExpr(Binary("<", TypedExpr(LocalOrFieldVar("x"), "int"), TypedExpr(Integer(2), "int")), "boolean"), TypedStmt(Block([
                TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Binary("+", TypedExpr(LocalOrFieldVar("x"), "int"), TypedExpr(Integer(1), "int")), "int")), "void")), "void"),
                TypedStmt(Continue, "void")
            ]), "void")), "void")
        ]), "void") )
        ]))]

continue_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 11, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "doContinue", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "BreakContinue", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 27, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 15, array_code_attr = [3,60,27,5,162,0,10,27,4,96,60,167,255,247,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_continue_AbsSyntax = TestCase (assertEqual "Abstract Syntax" continue_AbsSyntaxResult (parser(continueClass)))
test_continue_SemanticCheck = TestCase (assertEqual "Semantic check" continue_AbsSyntaxTypedResult (typecheckPrg(continue_AbsSyntaxResult)))
test_continue_Codegen = TestCase (assertEqual "Codegen" continue_BytecodeResult (createPrg(continue_AbsSyntaxTypedResult)))
test_continue_complete  = TestCase (assertEqual "complete compiler"  continue_BytecodeResult (compiler(continueClass)))
allTests_continue = TestList [test_continue_AbsSyntax, test_continue_SemanticCheck, test_continue_Codegen, test_continue_complete]



breakContinue_doTests = do
    print "Testcase 15: Testing break and continue in while loops (FiedlerTestBreakContinue.hs)"
    print "Test 15.1: Testing break"
    --print (compiler(breakClass))
    runTestTT allTests_break
    print "Test 15.2: Testing continue"
    runTestTT allTests_continue
    --print (compiler(continueClass))
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")