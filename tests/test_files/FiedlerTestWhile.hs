module FiedlerTestWhile where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 8: while
        Autor: Dominik Sauerer
        Beschreibung: Test einer while-Schleife mit Prüfwert und Body
        Testumfang: gesamter Compiler
-}

whileClass = "public class While {\
 \   public void method() {\
  \      while(true) {\
        \\
 \      }\
 \   }\
\}"

while_AbsSyntaxResult = [Class(Just Public, "While", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [], While(Bool(True), Empty))]))]

while_AbsSyntaxTypedResult = [Class(Just Public, "While", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [], TypedStmt(While(TypedExpr(Bool(True), "boolean"), TypedStmt (Empty,"void")), "void"))]))]

while_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 11,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "method", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "While", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 2,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 3, array_code_attr = [167,0,0], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []}]

test_while_AbsSyntax = TestCase (assertEqual "Abstract Syntax" while_AbsSyntaxResult (parser(whileClass)))
test_while_SemanticCheck = TestCase (assertEqual "Semantic check" while_AbsSyntaxTypedResult (typecheckPrg(while_AbsSyntaxResult)))
test_while_Codegen = TestCase (assertEqual "Codegen" while_BytecodeResult (createPrg(while_AbsSyntaxTypedResult)))
test_while_complete  = TestCase (assertEqual "complete compiler"  while_BytecodeResult (compiler(whileClass)))
allTests_while = TestList [test_while_AbsSyntax, test_while_SemanticCheck, test_while_Codegen, test_while_complete]

while_doTests = do
    print "Testcase 8: Testing While loop (FiedlerTestWhile.hs)"
    --print (compiler(whileClass))
    runTestTT allTests_while
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")