module FiedlerTestIdentifier where
import Control.Exception
import Test.HUnit
import Parser
import Control.Monad
import GHC.Natural
import AbstrakteSyntax
import ClassFormat
import TestTools
import ClassFile
import SemantikCheck

{-
        Testcase 27: Identifier
        Autor: Dominik Sauerer
        Beschreibung: Test von gültigen Identifier-Bezeichnungen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 27.1
        Autor: Dominik Sauerer
        Beschreibung: Korrekte Identifier
        Testumfang: Parser
-}

correctIdentifier = "public class Identifier  { \
    \ public int jfdskljdjslfj124_$__; \
  \  private int $382JkdjfldJU3; \
  \  public int _test2; \
\ }"

correctIdentifier_Solution = [Class(Just Public, "Identifier", ClassBody([
    FieldDecl(Just Public, "int", "jfdskljdjslfj124_$__"),
    FieldDecl(Just Private, "int", "$382JkdjfldJU3"),
    FieldDecl(Just Public, "int", "_test2")], []))]

identifier_CorrectIdentifiers_AbsSyntax = TestCase (assertEqual "Abstract Syntax" (parser correctIdentifier) correctIdentifier_Solution)




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 27.2
        Autor: Dominik Sauerer
        Beschreibung: Verbotene Identifier
        Testumfang: Parser
-}

falseIdentifierNumber = "public class Identifier { \
    \ public int 3testxyz; \
\ }"

identifier_FalseIdentifiers = (myTestCorrectedTest $ parser(falseIdentifierNumber))


test_correctIdentifier_AbsSyntax = TestList [identifier_CorrectIdentifiers_AbsSyntax]
test_falseIdentifier_AbsSyntax = TestList [identifier_FalseIdentifiers]


identifier_doTests = do
    print "Testcase 27: Testing correct naming of identifiers"
    print "Test 27.1: Testing correct identifiers"
    runTestTT test_correctIdentifier_AbsSyntax
    print "Test 27.2: Testing false identifiers"
    runTestTT test_falseIdentifier_AbsSyntax
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")