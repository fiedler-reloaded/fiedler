module FiedlerSemCheckTestCorrectReturns where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile
import Control.Exception

import TestTools

{-
        Testcase 22: CorrectReturns
        Autor: Dominik Sauerer
        Beschreibung: Semantikcheck-Test von Rückgabewerten: Akzeptieren richtiger Typen, Error bei falschen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.1
        Autor: Dominik Sauerer
        Beschreibung: korrekte Returntypen => Test soll durchlaufen
        Testumfang: Parser, Semantikcheck
-}

correctReturnsClass = "public class CorrectReturns {\
\    String myString;\
 \   public int a() {\
\        return 3;\
\    }\
\    public int b() {\
\        int x;\
\        x = 3;\
\        return x;\
\    }\
\    public String c() {\
\        return \"Hallo\";\
 \   }\
 \   private String e() {\
 \       return myString;\
\    }\
\    public boolean d() {\
\        return true;\
\    }\
\}"

correctReturns_AbsSyntaxResult = [Class(Just Public, "CorrectReturns", ClassBody([
    FieldDecl(Nothing, "String", "myString")], [
    MethodDecl(Just Public, "int", "a", [], Return(Integer(3))),
    MethodDecl(Just Public, "int", "b", [], Block([
        LocalVarDecl("int", "x"),
        StmtExprStmt(Assign("x", Integer(3))),
        Return(LocalOrFieldVar("x"))
    ])),
    MethodDecl(Just Public, "String", "c", [], Return(String("Hallo"))),
    MethodDecl(Just Private, "String", "e", [], Return(LocalOrFieldVar("myString"))),
    MethodDecl(Just Public, "boolean", "d", [], Return(Bool(True)))
    
    
    ]))]

correctReturns_AbsSyntaxTypedResult = [Class(Just Public, "CorrectReturns", ClassBody([
    FieldDecl(Nothing, "String", "myString")],
    [
    MethodDecl(Just Public, "int", "a", [], TypedStmt(Return(TypedExpr(Integer(3), "int")), "int")),
    MethodDecl(Just Public, "int", "b", [], TypedStmt(Block([
        TypedStmt(LocalVarDecl("int", "x"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(3), "int")), "void")), "void"),
        TypedStmt(Return(TypedExpr(LocalOrFieldVar("x"), "int")), "int")
    ]), "int")),
    MethodDecl(Just Public, "String", "c", [], TypedStmt(Return(TypedExpr(String("Hallo"), "String")), "String")),
    MethodDecl(Just Private, "String", "e", [], TypedStmt(Return(TypedExpr(LocalOrFieldVar("myString"), "String")), "String")),
    MethodDecl(Just Public, "boolean", "d", [], TypedStmt(Return(TypedExpr(Bool(True), "boolean")), "boolean"))
    
    ]
    ))]

test_correctReturns_AbsSyntax = TestCase (assertEqual "Abstract Syntax" correctReturns_AbsSyntaxResult (parser(correctReturnsClass)))
test_correctReturns_SemanticCheck = TestCase (assertEqual "Semantic check" correctReturns_AbsSyntaxTypedResult (typecheckPrg(correctReturns_AbsSyntaxResult)))






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.2
        Autor: Dominik Sauerer
        Beschreibung: falsche Returntypen (boolean statt int) => Error durch Semantikcheck
        Testumfang: Parser, Semantikcheck
-}

falseReturnsOneClass = "public class FalseReturn1 {\
 \   public int a() {\
 \       return true;\
 \   }\
\}"

falseReturnsOne_AbsSyntaxResult = [Class(Just Public, "FalseReturn1", ClassBody([],[
    MethodDecl(Just Public, "int", "a", [], Return(Bool(True)))
    ]))]

test_falseReturnsOne_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseReturnsOne_AbsSyntaxResult (parser(falseReturnsOneClass)))
test_falseReturnsOne_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseReturnsOne_AbsSyntaxResult))




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.3
        Autor: Dominik Sauerer
        Beschreibung: falsche Returntypen (void statt boolean) => Error durch Semantikcheck
        Testumfang: Parser, Semantikcheck
-}

falseReturnsTwoClass = "\
\public class FalseReturn2 {\
 \   public boolean a() {\
 \       int x;\
 \       x = 4;\
 \       if(x < 5) {\
 \           return true;\
 \       }\
 \   }\
\}"

falseReturnsTwo_AbsSyntaxResult = [Class(Just Public, "FalseReturn2", ClassBody([],[
    MethodDecl(Just Public, "boolean", "a", [], Block([
        LocalVarDecl("int", "x"),
        StmtExprStmt(Assign("x", Integer(4))),
        If(Binary("<", LocalOrFieldVar("x"), Integer(5)), Return(Bool(True)), Nothing)
    ]))
    ]))]

test_falseReturnsTwo_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseReturnsTwo_AbsSyntaxResult (parser(falseReturnsTwoClass)))
test_falseReturnsTwo_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseReturnsTwo_AbsSyntaxResult))





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.4
        Autor: Dominik Sauerer
        Beschreibung: falsche Returntypen (void statt String) => Error durch Semantikcheck
        Testumfang: Parser, Semantikcheck
-}
falseReturnsThreeClass = "public class FalseReturn3 {\
 \   public String a() {\
 \       int x;\
  \      x = 4;\
 \   }\
\}"

falseReturnsThree_AbsSyntaxResult = [Class(Just Public, "FalseReturn3", ClassBody([], [
    MethodDecl(Just Public, "String", "a", [], Block([
        LocalVarDecl("int", "x"),
        StmtExprStmt(Assign("x", Integer(4)))
    ]))
    ]))]

test_falseReturnsThree_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseReturnsThree_AbsSyntaxResult (parser(falseReturnsThreeClass)))
test_falseReturnsThree_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseReturnsThree_AbsSyntaxResult))





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.5
        Autor: Dominik Sauerer
        Beschreibung: falsche Returntypen (char statt String) => Error durch Semantikcheck
        Testumfang: Parser, Semantikcheck
-}

falseReturnsFourClass = "public class FalseReturn4 {\
  \  public String a() {\
  \      return 'x';\
  \  }\
\}"

falseReturnsFour_AbsSyntaxResult = [Class(Just Public, "FalseReturn4", ClassBody([], [
    MethodDecl(Just Public, "String", "a", [], Return(Char('x')))
    ]))]

test_falseReturnsFour_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseReturnsFour_AbsSyntaxResult (parser(falseReturnsFourClass)))
test_falseReturnsFour_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseReturnsFour_AbsSyntaxResult))





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.6
        Autor: Dominik Sauerer
        Beschreibung: nicht überall richtige Returntypen auf jedem Ausführungsstrang
            => Error durch Semantikcheck
        Testumfang: Parser, Semantikcheck
-}

falseReturnsFiveClass = "public class FalseReturn5 {\
 \   public boolean x;\
 \   public String a() {\
 \       if(x) {\
  \          return \"Hi\";\
  \      }\
  \  }\
\}"

falseReturnsFive_AbsSyntaxResult = [Class(Just Public, "FalseReturn5", ClassBody([
    FieldDecl(Just Public, "boolean", "x")
    ], [
    MethodDecl(Just Public, "String", "a", [], If(LocalOrFieldVar("x"), Return(String("Hi")), Nothing))
    ]))]

test_falseReturnsFive_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseReturnsFive_AbsSyntaxResult (parser(falseReturnsFiveClass)))
test_falseReturnsFive_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseReturnsFive_AbsSyntaxResult))






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 22.7
        Autor: Dominik Sauerer
        Beschreibung: nicht überall richtige Returntypen auf jedem Ausführungsstrang
            => Error durch Semantikcheck
        Testumfang: Parser, Semantikcheck
-}

falseReturnsSixClass = "public class FalseReturn6 {\
 \   public boolean x;\
 \   public String a() {\
  \      if(x) {\
  \          return;\
  \      }\
  \      return \"Hi\";\
  \  }\
\}"

falseReturnsSix_AbsSyntaxResult = [Class(Just Public, "FalseReturn6", ClassBody([
    FieldDecl(Just Public, "boolean", "x")
    ], [
    MethodDecl(Just Public, "String", "a", [], Block([
        If(LocalOrFieldVar("x"), ReturnVoid, Nothing),
        Return(String("Hi"))
    ]))
    ]))]

test_falseReturnsSix_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseReturnsSix_AbsSyntaxResult (parser(falseReturnsSixClass)))
test_falseReturnsSix_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseReturnsSix_AbsSyntaxResult))





allTests_correctReturns = TestList [test_correctReturns_AbsSyntax, test_correctReturns_SemanticCheck, 
                                    test_falseReturnsOne_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(falseReturnsOne_AbsSyntaxResult)),
                                    test_falseReturnsTwo_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(falseReturnsTwo_AbsSyntaxResult)),
                                    test_falseReturnsThree_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(falseReturnsThree_AbsSyntaxResult)),
                                    test_falseReturnsFour_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(falseReturnsFour_AbsSyntaxResult)),
                                    test_falseReturnsFive_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(falseReturnsFive_AbsSyntaxResult)),
                                    test_falseReturnsSix_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(falseReturnsSix_AbsSyntaxResult)) ]


correctReturns_doTests = do
    print "Testing Correct Returns (Semantik check test) (FiedlerSemCheckTestCorrectReturns.hs)"
    runTestTT allTests_correctReturns
    --print myTest
    --runTestTT (myTestCorrectedTest $ typecheckPrg(falseReturnsFour_AbsSyntaxResult))
   -- myTestCorrected (print(typecheckPrg(falseReturnsTwo_AbsSyntaxResult)))

    --print(typecheckPrg(falseReturnsOne_AbsSyntaxResult))
    --print(typecheckPrg(falseReturnsTwo_AbsSyntaxResult))
    --print(typecheckPrg(falseReturnsThree_AbsSyntaxResult))
    --print(typecheckPrg(falseReturnsFour_AbsSyntaxResult))
    --print(typecheckPrg(falseReturnsFive_AbsSyntaxResult))
    --print(typecheckPrg(falseReturnsSix_AbsSyntaxResult)) 



    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")