module FiedlerTestManyParams where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 18: ManyParams
        Autor: Dominik Sauerer
        Beschreibung: Test einer Methode mit vielen Parametern und lokalen Variablen
        Testumfang: gesamter Compiler
-}

manyParamsClass = "public class ManyParams {\
  \  public int method(int a, int b, char x, String j, boolean c, int d) {\
 \       a = 5;\
 \       b = 3;\
 \       x = '9';\
 \       j = \"hello\";\
 \       c = true;\
\        d = 33;\
 \       int ax;\
\        ax = 65;\
 \       return a; \
\    }\
\}"

manyParams_AbsSyntaxResult = [Class(Just Public, "ManyParams", ClassBody([], [
        MethodDecl(Just Public, "int", "method", [
            ("int", "a"),("int", "b"),("char", "x"),("String", "j"),("boolean", "c"),("int", "d")
        ], Block([
            StmtExprStmt(Assign("a", Integer(5))),
            StmtExprStmt(Assign("b", Integer(3))),
            StmtExprStmt(Assign("x", Char('9'))),
            StmtExprStmt(Assign("j", String("hello"))),
            StmtExprStmt(Assign("c", Bool(True))),
            StmtExprStmt(Assign("d", Integer(33))),
            LocalVarDecl("int", "ax"),
            StmtExprStmt(Assign("ax", Integer(65))),
            Return(LocalOrFieldVar("a"))
        ])
        )]))]

manyParams_AbsSyntaxTypedResult = [Class(Just Public, "ManyParams", ClassBody([], [
        MethodDecl(Just Public, "int", "method", [
            ("int", "a"),("int", "b"),("char", "x"),("String", "j"),("boolean", "c"),("int", "d")
        ], TypedStmt(Block([
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("a", TypedExpr(Integer(5), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("b", TypedExpr(Integer(3), "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Char('9'), "char")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("j", TypedExpr(String("hello"), "String")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("c", TypedExpr(Bool(True), "boolean")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("d", TypedExpr(Integer(33), "int")), "void")), "void"),
            TypedStmt(LocalVarDecl("int", "ax"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("ax", TypedExpr(Integer(65), "int")), "void")), "void"),
            TypedStmt(Return(TypedExpr(LocalOrFieldVar("a"), "int")), "int")
        ])
        , "int"))]))]

manyParams_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 14, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 4, index_nameandtype_cp = 10, desc = ""},String_Info {tag_cp = TagString, index_cp = 11, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "method", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 26, cad_cp = "(IICLjava/lang/String;ZI)I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 5, index_descr_cp = 6, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "hello", 
        desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "ManyParams", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 3}, 
    super = SuperClass {index_sp = 4}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 5, index_descr_mi = 6, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 7, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 
        0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 7, tam_len_attr = 36, len_stack_attr = 1, len_local_attr = 8, tam_code_attr = 24, array_code_attr = [8,60,6,61,16,57,62,18,2,58,4,4,54,5,16,33,54,6,16,65,54,7,27,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_manyParams_AbsSyntax = TestCase (assertEqual "Abstract Syntax" manyParams_AbsSyntaxResult (parser(manyParamsClass)))
test_manyParams_SemanticCheck = TestCase (assertEqual "Semantic check" manyParams_AbsSyntaxTypedResult (typecheckPrg(manyParams_AbsSyntaxResult)))
test_manyParams_Codegen = TestCase (assertEqual "Codegen" manyParams_BytecodeResult (createPrg(manyParams_AbsSyntaxTypedResult)))
test_manyParams_complete  = TestCase (assertEqual "complete compiler"  manyParams_BytecodeResult (compiler(manyParamsClass)))
allTests_manyParams = TestList [test_manyParams_AbsSyntax, test_manyParams_SemanticCheck, test_manyParams_Codegen, test_manyParams_complete]

manyParams_doTests = do
    print "Testcase 18: Testing methods with many params (FiedlerTestManyParams.hs)"
    --print (compiler(manyParamsClass))
    runTestTT allTests_manyParams
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")