module FiedlerSemCheckTestBoolInIfAndWhile where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile
import Control.Exception

import TestTools

{-
        Testcase 26
        Autor: Dominik Sauerer
        Beschreibung: Semantikcheck: Test auf boolean-Expressions in if/while
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.1
        Autor: Dominik Sauerer
        Beschreibung: richtige boolean-Ausdrücke in ifs => Klasse soll kompilieren
        Testumfang: Parser, Semantikcheck
-}


correctIfsClass = "public class BoolInIfAndWhile {\
  \  public void correctIfs() {\
   \     if(true) {\
\\
  \      }\
  \      boolean x;\
  \      x = false;\
  \      if(x) {\
\\
  \      }\
  \      if(!x) {\
\\
   \     }\
   \     if(5 == 5) {\
\\
   \     }\
  \  }\
\}"

correctIfs_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "correctIfs", [], Block([
        If(Bool(True), Empty, Nothing),
        LocalVarDecl("boolean", "x"),
        StmtExprStmt(Assign("x", Bool(False))),
        If(LocalOrFieldVar("x"), Empty, Nothing),
        If(Unary("!", LocalOrFieldVar("x")), Empty, Nothing),
        If(Binary("==", Integer(5), Integer(5)), Empty, Nothing)
    ]))
    ]))]

correctIfs_AbsSyntaxTypedResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "correctIfs", [], TypedStmt(Block([
        TypedStmt(If(TypedExpr(Bool(True), "boolean"), TypedStmt(Empty, "void"), Nothing), "void"),
        TypedStmt(LocalVarDecl("boolean", "x"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Bool(False), "boolean")), "void")), "void"),
        TypedStmt(If(TypedExpr(LocalOrFieldVar("x"), "boolean"), TypedStmt(Empty, "void"), Nothing), "void"),
        TypedStmt(If(TypedExpr(Unary("!", TypedExpr(LocalOrFieldVar("x"), "boolean")), "boolean"), TypedStmt(Empty, "void"), Nothing), "void"),
        TypedStmt(If(TypedExpr(Binary("==", TypedExpr(Integer(5), "int"), TypedExpr(Integer(5), "int")), "boolean"), TypedStmt(Empty, "void"), Nothing), "void")
    ]), "void"))
    ]))]

correctIfs_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 11, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "correctIfs", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "BoolInIfAndWhile", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 23, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 11, array_code_attr = [3,60,27,153,0,3,27,154,0,3,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_correctIfs_AbsSyntax = TestCase (assertEqual "Abstract Syntax" correctIfs_AbsSyntaxResult (parser(correctIfsClass)))
test_correctIfs_SemanticCheck = TestCase (assertEqual "Semantic check" correctIfs_AbsSyntaxTypedResult (typecheckPrg(correctIfs_AbsSyntaxResult)))
test_correctIfs_Codegen = TestCase (assertEqual "Codegen" correctIfs_BytecodeResult (createPrg(correctIfs_AbsSyntaxTypedResult)))
test_correctIfs_complete  = TestCase (assertEqual "complete compiler"  correctIfs_BytecodeResult (compiler(correctIfsClass)))
allTests_correctIfs = TestList [test_correctIfs_AbsSyntax, test_correctIfs_SemanticCheck, test_correctIfs_Codegen, test_correctIfs_complete]







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.2
        Autor: Dominik Sauerer
        Beschreibung: richtige boolean-Ausdrücke in whiles => Klasse soll kompilieren
        Testumfang: Parser, Semantikcheck
-}

correctWhilesClass = "public class BoolInIfAndWhile {\
  \  public void correctWhiles() {\
  \      boolean x;\
  \      x = false;\
  \      while(x) {\
\\
  \      }\
  \      while(!x) {\
\\
   \     }\
   \     while(5 == 5) {\
\\
   \     }\
  \  }\
\}"

correctWhiles_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "correctWhiles", [], Block([
        LocalVarDecl("boolean", "x"),
        StmtExprStmt(Assign("x", Bool(False))),
        While(LocalOrFieldVar("x"), Empty),
        While(Unary("!", LocalOrFieldVar("x")), Empty),
        While(Binary("==", Integer(5), Integer(5)), Empty)
    ]))
    ]))]

correctWhiles_AbsSyntaxTypedResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "correctWhiles", [], TypedStmt(Block([
        TypedStmt(LocalVarDecl("boolean", "x"), "void"),
        TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Bool(False), "boolean")), "void")), "void"),
        TypedStmt(While(TypedExpr(LocalOrFieldVar("x"), "boolean"), TypedStmt(Empty, "void")), "void"),
        TypedStmt(While(TypedExpr(Unary("!", TypedExpr(LocalOrFieldVar("x"), "boolean")), "boolean"), TypedStmt(Empty, "void")), "void"),
        TypedStmt(While(TypedExpr(Binary("==", TypedExpr(Integer(5), "int"), TypedExpr(Integer(5), "int")), "boolean"), TypedStmt(Empty, "void")), "void")
    ]), "void"))
    ]))]

correctWhiles_BytecodeResult = [ClassFile {
    magic = Magic, 
    minver = MinorVersion {numMinVer = 0}, 
    maxver = MajorVersion {numMaxVer = 49}, 
    count_cp = 11, 
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "correctWhiles", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "BoolInIfAndWhile", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}], 
    acfg = AccessFlags [1,32], 
    this = ThisClass {index_th = 2}, 
    super = SuperClass {index_sp = 3}, 
    count_interfaces = 0, 
    array_interfaces = [], 
    count_fields = 0, 
    array_fields = [], 
    count_methods = 2, 
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 31, len_stack_attr = 1, len_local_attr = 2, tam_code_attr = 19, array_code_attr = [3,60,27,153,0,6,167,255,252,27,154,0,6,167,255,252,167,0,0], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], 
    count_attributes = 0, 
    array_attributes = []}]

test_correctWhiles_AbsSyntax = TestCase (assertEqual "Abstract Syntax" correctWhiles_AbsSyntaxResult (parser(correctWhilesClass)))
test_correctWhiles_SemanticCheck = TestCase (assertEqual "Semantic check" correctWhiles_AbsSyntaxTypedResult (typecheckPrg(correctWhiles_AbsSyntaxResult)))
test_correctWhiles_Codegen = TestCase (assertEqual "Codegen" correctWhiles_BytecodeResult (createPrg(correctWhiles_AbsSyntaxTypedResult)))
test_correctWhiles_complete  = TestCase (assertEqual "complete compiler"  correctWhiles_BytecodeResult (compiler(correctWhilesClass)))
allTests_correctWhiles = TestList [test_correctWhiles_AbsSyntax, test_correctWhiles_SemanticCheck, test_correctWhiles_Codegen, test_correctWhiles_complete]






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.3
        Autor: Dominik Sauerer
        Beschreibung: int in if => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

falseIf1Class = "public class BoolInIfAndWhile {\
 \   public void falseIf1() {\
 \       if(5) {\
     \   \
      \  }\
   \ }\
\}"

falseIf1_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "falseIf1", [], If(Integer(5), Empty, Nothing))
    ]))]

test_falseIf1_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseIf1_AbsSyntaxResult (parser(falseIf1Class)))
test_falseIf1_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseIf1_AbsSyntaxResult))
allTests_falseIf1 = TestList [test_falseIf1_AbsSyntax, test_falseIf1_SemanticCheck]





------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.4
        Autor: Dominik Sauerer
        Beschreibung: char in if => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

falseIf2Class = "public class BoolInIfAndWhile {\
 \   public void falseIf2() {\
       \ char z;\
     \   z = 'x';\
 \       if(z) {\
     \   \
      \  }\
   \ }\
\}"

falseIf2_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "falseIf2", [], Block([
        LocalVarDecl("char", "z"),
        StmtExprStmt(Assign("z", Char('x'))),
        If(LocalOrFieldVar("z"), Empty, Nothing)
    ]))
    ]))]


test_falseIf2_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseIf2_AbsSyntaxResult (parser(falseIf2Class)))
test_falseIf2_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseIf2_AbsSyntaxResult))
allTests_falseIf2 = TestList [test_falseIf2_AbsSyntax, test_falseIf2_SemanticCheck]







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.5
        Autor: Dominik Sauerer
        Beschreibung: Assignment in while => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

falseWhile1Class = "public class BoolInIfAndWhile {\
 \   public void falseWhile1() {\
      \  while(x = 5) {\
\\
       \ }\
 \   }\
\}"

falseWhile1_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "falseWhile1", [], While(StmtExprExpr(Assign("x", Integer(5))), Empty))
    ]))]

test_falseWhile1_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseWhile1_AbsSyntaxResult))
allTests_falseWhile1 = TestList [test_falseWhile1_SemanticCheck]






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.6
        Autor: Dominik Sauerer
        Beschreibung: Assignment in while => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}
falseWhile2Class = "public class BoolInIfAndWhile {\
 \   public void falseWhile2() {\
      \  String x;\
      \  while(x) {\
         \ \  
       \ }\
  \  }\
\}"

falseWhile2_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "falseWhile2", [], Block([
        LocalVarDecl("String", "x"),
        While(LocalOrFieldVar("x"), Empty)
    ]))
    ]))]

test_falseWhile2_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseWhile2_AbsSyntaxResult (parser(falseWhile2Class)))
test_falseWhile2_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseWhile2_AbsSyntaxResult))
allTests_falseWhile2 = TestList [test_falseWhile2_AbsSyntax, test_falseWhile2_SemanticCheck]






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 26.7
        Autor: Dominik Sauerer
        Beschreibung: int in while => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}
falseWhile3Class = "public class BoolInIfAndWhile {\
 \   public void falseWhile3() {\
      \  while(1) {\
         \   \
      \  }\
\    }\
\}"

falseWhile3_AbsSyntaxResult = [Class(Just Public, "BoolInIfAndWhile", ClassBody([], [
    MethodDecl(Just Public, "void", "falseWhile3", [], While(Integer(1), Empty))
    ]))]

test_falseWhile3_AbsSyntax = TestCase (assertEqual "Abstract Syntax" falseWhile3_AbsSyntaxResult (parser(falseWhile3Class)))
test_falseWhile3_SemanticCheck = (myTestCorrectedTest $ typecheckPrg(falseWhile3_AbsSyntaxResult))
allTests_falseWhile3 = TestList [test_falseWhile3_AbsSyntax, test_falseWhile3_SemanticCheck]


boolInIfAndWhile_doTests = do
    print "Testcase 26: Testing boolean expressions in ifs and whiles"
    print "Test 26.1: Testing boolean expression in ifs"
    runTestTT allTests_correctIfs
    print "Test 26.2: Testing boolean expression in whiles"
    runTestTT allTests_correctWhiles
    print "Test 26.3: Should throw error: Integer in if check"
    runTestTT allTests_falseIf1
    print "Test 26.3: Should throw error: Char in if check"
    runTestTT allTests_falseIf2
    print "Test 26.4: Should throw error: Assign in while check"
    runTestTT allTests_falseWhile1
    print "Test 26.5: Should throw error: String in while check"
    runTestTT allTests_falseWhile2
    print "Test 26.6: Should throw error: Integer in while check"
    runTestTT allTests_falseWhile3
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")