module FiedlerSemCheckTestMethodsExist where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile
import Control.Exception

import TestTools

{-
        Testcase 25: MethodsExist
        Autor: Dominik Sauerer
        Beschreibung: Semantikcheck-Test auf nicht existierende/falsch angegebene Methoden
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 25.1
        Autor: Dominik Sauerer
        Beschreibung: korrekte Methodenaufrufe => Test soll durchlaufen
        Testumfang: Parser, Semantikcheck
-}

methodsExistTest1Class = "public class MethodsExist {\
  \  public void test1() {\
   \     Helper helper;\
  \      helper.method();\
  \      helper.method2(3);\
  \      int abc;\
  \      abc = helper.method3();\
  \      abc = helper.xyz;\
 \   }\
\}\
\\
\public class Helper {\
 \   public int xyz;\
 \   public void method() {\
\\
 \   }\
\    public void method2(int x) {\
\\
\    }\
\    public int method3() {\
 \       return 0;\
 \   }\
\}"

methodsExistTest1_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "xyz")], [
    MethodDecl(Just Public, "void", "method", [], Empty),
    MethodDecl(Just Public, "void", "method2", [("int", "x")], Empty),
    MethodDecl(Just Public, "int", "method3", [], Return(Integer(0)))
    ])),
    Class(Just Public, "MethodsExist", ClassBody([],[
        MethodDecl(Just Public, "void", "test1", [], Block([
            LocalVarDecl("Helper", "helper"),
            StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "method", [])),
            StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "method2", [Integer(3)])),
            LocalVarDecl("int", "abc"),
            StmtExprStmt(Assign("abc", StmtExprExpr(MethodCall(LocalOrFieldVar("helper"), "method3", [])) )),
            StmtExprStmt(Assign("abc", InstVar(LocalOrFieldVar("helper"), "xyz") ))
        ]))
    ]))]

methodsExistTest1_AbsSyntaxTypedResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "xyz")], [
    MethodDecl(Just Public, "void", "method", [], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "void", "method2", [("int", "x")], TypedStmt(Empty, "void")),
    MethodDecl(Just Public, "int", "method3", [], TypedStmt(Return(TypedExpr(Integer(0), "int")), "int"))
    ])),
    Class(Just Public, "MethodsExist", ClassBody([],[
        MethodDecl(Just Public, "void", "test1", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("Helper", "helper"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "method", []), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "method2", [TypedExpr(Integer(3), "int")]), "void")), "void"),
            TypedStmt(LocalVarDecl("int", "abc"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("abc", TypedExpr(StmtExprExpr(TypedStmtExpr(MethodCall(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "method3", []), "int")) , "int")), "void")), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("abc", TypedExpr(InstVar(TypedExpr(LocalOrFieldVar("helper"), "Helper"), "xyz") , "int")), "void")), "void")
        ]), "void"))
    ]))]

test_methodsExistTest1_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodsExistTest1_AbsSyntaxResult (parser(methodsExistTest1Class)))
test_methodsExistTest1_SemanticCheck = TestCase (assertEqual "Semantic check" methodsExistTest1_AbsSyntaxTypedResult (typecheckPrg(methodsExistTest1_AbsSyntaxResult)))
allTests_methodsExistTest1 = TestList [test_methodsExistTest1_AbsSyntax, test_methodsExistTest1_SemanticCheck] 






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 25.2
        Autor: Dominik Sauerer
        Beschreibung: nicht existierende Methode => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

methodsExistTest2NotWorkingClass = "public class MethodsExist {\
  \  public void test2_NotWorking() {\
     \   Helper helper;\
     \   helper.xy();\
   \ }\
\}\
\public class Helper {\
 \   public int xyz;\
 \   public void method() {\
\\
 \   }\
\    public void method2(int x) {\
\\
\    }\
\    public int method3() {\
 \       return 0;\
 \   }\
\}"

methodsExistTest2_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "xyz")], [
    MethodDecl(Just Public, "void", "method", [], Empty),
    MethodDecl(Just Public, "void", "method2", [("int", "x")], Empty),
    MethodDecl(Just Public, "int", "method3", [], Return(Integer(0)))
    ])),
    Class(Just Public, "MethodsExist", ClassBody([],[
        MethodDecl(Just Public, "void", "test2_NotWorking", [], Block([
            LocalVarDecl("Helper", "helper"),
            StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "xy", []))
        ]))
    ]))]






------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 25.3
        Autor: Dominik Sauerer
        Beschreibung: Methodenaufruf mit falschen Parametern => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

methodsExistTest3NotWorkingClass = "public class MethodsExist {\
  \  public void test3_NotWorking() {\
  \      Helper helper;\
    \    helper.method(1);\
   \ }\
\}\
\public class Helper {\
 \   public int xyz;\
 \   public void method() {\
\\
 \   }\
\    public void method2(int x) {\
\\
\    }\
\    public int method3() {\
 \       return 0;\
 \   }\
\}"

methodsExistTest3_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "xyz")], [
    MethodDecl(Just Public, "void", "method", [], Empty),
    MethodDecl(Just Public, "void", "method2", [("int", "x")], Empty),
    MethodDecl(Just Public, "int", "method3", [], Return(Integer(0)))
    ])),
    Class(Just Public, "MethodsExist", ClassBody([],[
        MethodDecl(Just Public, "void", "test3_NotWorking", [], Block([
            LocalVarDecl("Helper", "helper"),
            StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "method", [Integer(1)]))
        ]))
    ]))]







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 25.4
        Autor: Dominik Sauerer
        Beschreibung: Methodenaufruf mit falschen Parametern => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

methodsExistTest4NotWorkingClass = "public class MethodsExist {\
  \  public void test4_NotWorking() {\
  \      Helper helper;\
    \    helper.method2(\"Hallo\");\
   \ }\
\}\
\public class Helper {\
 \   public int xyz;\
 \   public void method() {\
\\
 \   }\
\    public void method2(int x) {\
\\
\    }\
\    public int method3() {\
 \       return 0;\
 \   }\
\}"

methodsExistTest4_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "xyz")], [
    MethodDecl(Just Public, "void", "method", [], Empty),
    MethodDecl(Just Public, "void", "method2", [("int", "x")], Empty),
    MethodDecl(Just Public, "int", "method3", [], Return(Integer(0)))
    ])),
    Class(Just Public, "MethodsExist", ClassBody([],[
        MethodDecl(Just Public, "void", "test4_NotWorking", [], Block([
            LocalVarDecl("Helper", "helper"),
            StmtExprStmt(MethodCall(LocalOrFieldVar("helper"), "method2", [String("Hallo")]))
        ]))
    ]))]







------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 25.5
        Autor: Dominik Sauerer
        Beschreibung: Methodenaufruf mit falsch angenommenen Rückgabetyp => Semantikcheck soll fehlschlagen
        Testumfang: Parser, Semantikcheck
-}

methodsExistTest5NotWorkingClass = "public class MethodsExist {\
  \  public void test5_NotWorking() {\
  \      Helper helper;\
   \     char z;\
    \    z = helper.method3();\
   \ }\
\}\
\public class Helper {\
 \   public int xyz;\
 \   public void method() {\
\\
 \   }\
\    public void method2(int x) {\
\\
\    }\
\    public int method3() {\
 \       return 0;\
 \   }\
\}"

methodsExistTest5_AbsSyntaxResult = [Class(Just Public, "Helper", ClassBody([
    FieldDecl(Just Public, "int", "xyz")], [
    MethodDecl(Just Public, "void", "method", [], Empty),
    MethodDecl(Just Public, "void", "method2", [("int", "x")], Empty),
    MethodDecl(Just Public, "int", "method3", [], Return(Integer(0)))
    ])),
    Class(Just Public, "MethodsExist", ClassBody([],[
        MethodDecl(Just Public, "void", "test5_NotWorking", [], Block([
            LocalVarDecl("Helper", "helper"),
            LocalVarDecl("char", "z"),
            StmtExprStmt(Assign("z", StmtExprExpr(MethodCall(LocalOrFieldVar("helper"), "method3", [])) ))
        ]))
    ]))]





test_methodsExistTest2NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodsExistTest2_AbsSyntaxResult (parser(methodsExistTest2NotWorkingClass)))
test_methodsExistTest3NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodsExistTest3_AbsSyntaxResult (parser(methodsExistTest3NotWorkingClass)))
test_methodsExistTest4NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodsExistTest4_AbsSyntaxResult (parser(methodsExistTest4NotWorkingClass)))
test_methodsExistTest5NotWorking_AbsSyntax = TestCase (assertEqual "Abstract Syntax" methodsExistTest5_AbsSyntaxResult (parser(methodsExistTest5NotWorkingClass)))



allTests_methodsExistTest2NotWorking = TestList [test_methodsExistTest2NotWorking_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(methodsExistTest2_AbsSyntaxResult))]                                    
allTests_methodsExistTest3NotWorking = TestList [test_methodsExistTest3NotWorking_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(methodsExistTest3_AbsSyntaxResult))]                                    
allTests_methodsExistTest4NotWorking = TestList [test_methodsExistTest4NotWorking_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(methodsExistTest4_AbsSyntaxResult))]                                    
allTests_methodsExistTest5NotWorking = TestList [test_methodsExistTest5NotWorking_AbsSyntax, (myTestCorrectedTest $ typecheckPrg(methodsExistTest5_AbsSyntaxResult))]                                    

methodsExist_doTests = do
    print "Testcase 25: Testing semantic check, if methods exist (FiedlerSemCheckTestMethodsExist.hs)"
    print "Test 25.1: Methods exist"
    runTestTT allTests_methodsExistTest1
    print "Test 25.2: Failure because of not existing methods"
    runTestTT allTests_methodsExistTest2NotWorking
    print "Test 25.3: Failure because of not existing overloading"
    runTestTT allTests_methodsExistTest3NotWorking
    print "Test 25.4: Failure because of not existing overloading"
    runTestTT allTests_methodsExistTest4NotWorking
    print "Test 25.5: Failure becuase of not existing overloading"
    runTestTT allTests_methodsExistTest5NotWorking

    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")