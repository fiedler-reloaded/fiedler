module FiedlerTestInstances where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 11: Instances
        Autor: Dominik Sauerer
        Beschreibung: Test von Instanziierungen von anderen Klassen
-}
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 11.1
        Autor: Dominik Sauerer
        Beschreibung: Test von Instanziierung ohne Parameter
        Testumfang: gesamter Compiler
-}

instancesMethodClass = "public class Instances {\
  \  public void method() {\
 \       HelperClass helper;\
 \       helper = new HelperClass();\
 \   }\
\}\
\\
\public class HelperClass {\
\\
\}"

instancesMethod_AbsSyntaxResult = [Class(Just Public, "HelperClass", ClassBody([], [])),
    Class(Just Public, "Instances", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [], Block([
            LocalVarDecl("HelperClass", "helper"),
            StmtExprStmt(Assign("helper", StmtExprExpr(New("HelperClass", []))))
        ]))]))]

instancesMethod_AbsSyntaxTypedResult = [Class(Just Public, "HelperClass", ClassBody([], [])),
    Class(Just Public, "Instances", ClassBody([], [
        MethodDecl(Just Public, "void", "method", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("HelperClass", "helper"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helper", TypedExpr(StmtExprExpr(TypedStmtExpr(New("HelperClass", []), "HelperClass")), "HelperClass")), "void")), "void")
        ]), "void"))]))]

instancesMethod_BytecodeResult = [
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 10,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 2},
        super = SuperClass {index_sp = 3},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 0,
        array_fields = [],
        count_methods = 1,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 14,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "method", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 6, index_descr_cp = 7, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "Instances", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 4},
        super = SuperClass {index_sp = 5},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 0,
        array_fields = [],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 6, index_descr_mi = 7, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 8, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 9, index_descr_mi = 7, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 8, tam_len_attr = 21, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 9, array_code_attr = [187,0,2,89,183,0,3,76,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

test_instancesMethod_AbsSyntax = TestCase (assertEqual "Abstract Syntax" instancesMethod_AbsSyntaxResult (parser(instancesMethodClass)))
test_instancesMethod_SemanticCheck = TestCase (assertEqual "Semantic check" instancesMethod_AbsSyntaxTypedResult (typecheckPrg(instancesMethod_AbsSyntaxResult)))
test_instancesMethod_Codegen = TestCase (assertEqual "Codegen" instancesMethod_BytecodeResult (createPrg(instancesMethod_AbsSyntaxTypedResult)))
test_instancesMethod_complete  = TestCase (assertEqual "complete compiler"  instancesMethod_BytecodeResult (compiler(instancesMethodClass)))
allTests_instancesMethod = TestList [test_instancesMethod_AbsSyntax, test_instancesMethod_SemanticCheck, test_instancesMethod_Codegen, test_instancesMethod_complete]




------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
{-
        Testcase 11.2
        Autor: Dominik Sauerer
        Beschreibung: Test von Instanziierung mit Parametern
        Testumfang: gesamter Compiler
-}

instancesOtherMethodClass = "public class Instances {\
  \  public void otherMethod() {\
 \       int x;\
 \       x = 5;\
  \      OtherHelperClass helperClass;\
  \      helperClass = new OtherHelperClass(x);\
  \  }\
\}\
\\
\public class OtherHelperClass {\
 \   private int value;\
 \   public OtherHelperClass(int x) {\
 \       value = x;\
 \   }\
\}"

instancesOtherMethod_AbsSyntaxResult = [Class(Just Public, "OtherHelperClass", ClassBody([
    FieldDecl(Just Private, "int", "value")
    ], [
        ConstructorDecl(Just Public, "OtherHelperClass", [("int", "x")], StmtExprStmt(Assign("value", LocalOrFieldVar("x"))))
    ])),
    Class(Just Public, "Instances", ClassBody([], [
        MethodDecl(Just Public, "void", "otherMethod", [], Block([
            LocalVarDecl("int", "x"),
            StmtExprStmt(Assign("x", Integer(5))),
            LocalVarDecl("OtherHelperClass", "helperClass"),
            StmtExprStmt(Assign("helperClass", StmtExprExpr(New("OtherHelperClass", [LocalOrFieldVar("x")]))))
        ]))]))]

instancesOtherMethod_AbsSyntaxTypedResult = [Class(Just Public, "OtherHelperClass", ClassBody([
    FieldDecl(Just Private, "int", "value")
    ], [
        ConstructorDecl(Just Public, "OtherHelperClass", [("int", "x")], TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("value", TypedExpr(LocalOrFieldVar("x"), "int")), "void")), "void"))
    ])),
    Class(Just Public, "Instances", ClassBody([], [
        MethodDecl(Just Public, "void", "otherMethod", [], TypedStmt(Block([
            TypedStmt(LocalVarDecl("int", "x"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("x", TypedExpr(Integer(5), "int")), "void")), "void"),
            TypedStmt(LocalVarDecl("OtherHelperClass", "helperClass"), "void"),
            TypedStmt(StmtExprStmt(TypedStmtExpr(Assign("helperClass", TypedExpr(StmtExprExpr(TypedStmtExpr(New("OtherHelperClass", [TypedExpr(LocalOrFieldVar("x"), "int")]),"OtherHelperClass")), "OtherHelperClass")), "void")), "void")
        ]), "void"))]))]

instancesOtherMethod_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 15,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 4, index_nameandtype_cp = 10, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 3, index_nameandtype_cp = 11, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "value", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(I)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 14, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 5, index_descr_cp = 6, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "OtherHelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 3},
    super = SuperClass {index_sp = 4},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 1,
    array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 5, index_descr_fi = 6, tam_fi = 0, array_attr_fi = []}],
    count_methods = 1,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 8, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 9, tam_len_attr = 22, len_stack_attr = 2, len_local_attr = 2, tam_code_attr = 10, array_code_attr = [42,183,0,1,42,27,181,0,2,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 16,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 10, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 11, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "otherMethod", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 6, index_descr_cp = 7, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "OtherHelperClass", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 6, index_descr_cp = 15, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "Instances", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "(I)V", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 4},
        super = SuperClass {index_sp = 5},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 0,
        array_fields = [],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 6, index_descr_mi = 7, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 8, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 9, index_descr_mi = 7, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 8, tam_len_attr = 24, len_stack_attr = 3, len_local_attr = 3, tam_code_attr = 12, array_code_attr = [8,60,187,0,2,89,27,183,0,3,77,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

test_instancesOtherMethod_AbsSyntax = TestCase (assertEqual "Abstract Syntax" instancesOtherMethod_AbsSyntaxResult (parser(instancesOtherMethodClass)))
test_instancesOtherMethod_SemanticCheck = TestCase (assertEqual "Semantic check" instancesOtherMethod_AbsSyntaxTypedResult (typecheckPrg(instancesOtherMethod_AbsSyntaxResult)))
test_instancesOtherMethod_Codegen = TestCase (assertEqual "Codegen" instancesOtherMethod_BytecodeResult (createPrg(instancesOtherMethod_AbsSyntaxTypedResult)))
test_instancesOtherMethod_complete  = TestCase (assertEqual "complete compiler"  instancesOtherMethod_BytecodeResult (compiler(instancesOtherMethodClass)))
allTests_instancesOtherMethod = TestList [test_instancesOtherMethod_AbsSyntax, test_instancesOtherMethod_SemanticCheck, test_instancesOtherMethod_Codegen, test_instancesOtherMethod_complete]



instances_doTests = do
    print "Testcase 11: Testing using instances (FiedlerTestInstances.hs)"
    --print (compiler(calculationsClass))
    print "Test 11.1: instances of empty class with default constructor"
    runTestTT allTests_instancesMethod
    print "Test 11.2: instances of non empty class with special constructor"
    runTestTT allTests_instancesOtherMethod
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")