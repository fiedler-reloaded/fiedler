module FiedlerTestConstructor where

import Test.HUnit
import Parser

import AbstrakteSyntax
import ClassFormat
import SemantikCheck
import ClassFile

import TestTools

{-
        Testcase 14: Constructor
        Autor: Dominik Sauerer
        Beschreibung: Test von eigenen Konstruktoren, abseits des Standardkonstruktors
        Testumfang: gesamter Compiler
-}

constructorClass = "public class Constructor {\
 \   public Constructor(int x, char y) {\
 \       \
 \   }\
\}"

constructor_AbsSyntaxResult = [Class(Just Public, "Constructor", ClassBody([], [
        ConstructorDecl(Just Public, "Constructor", [("int", "x"), ("char", "y")], Empty)]))]

constructor_AbsSyntaxTypedResult = [Class(Just Public, "Constructor", ClassBody([], [
        ConstructorDecl(Just Public, "Constructor", [("int", "x"), ("char", "y")], TypedStmt(Empty, "void"))]))]

constructor_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 11,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 5, cad_cp = "(IC)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 10, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "Constructor", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 1,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 3, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}], count_attributes = 0,
    array_attributes = []}]

test_constructor_AbsSyntax = TestCase (assertEqual "Abstract Syntax" constructor_AbsSyntaxResult (parser(constructorClass)))
test_constructor_SemanticCheck = TestCase (assertEqual "Semantic check" constructor_AbsSyntaxTypedResult (typecheckPrg(constructor_AbsSyntaxResult)))
test_constructor_Codegen = TestCase (assertEqual "Codegen" constructor_BytecodeResult (createPrg(constructor_AbsSyntaxTypedResult)))
test_constructor_complete  = TestCase (assertEqual "complete compiler"  constructor_BytecodeResult (compiler(constructorClass)))
allTests_constructor = TestList [test_constructor_AbsSyntax, test_constructor_SemanticCheck, test_constructor_Codegen, test_constructor_complete]

constructor_doTests = do
    print "Testcase 14: Testing constructor (FiedlerTestConstructor.hs)"
    --print (compiler(constructorClass))
    runTestTT allTests_constructor
    putStrLn("\n")
    putStrLn("\n")
    putStrLn("\n")