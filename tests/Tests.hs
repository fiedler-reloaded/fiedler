module Main where

import Test.HUnit
import AbstrakteSyntax
import Scanner
import Parser
import TestTools
import ClassFormat
import ClassFile
import SemantikCheck

import FiedlerTestEmptyJava
import FiedlerTestIdentifier
import FiedlerTestAttributes
import FiedlerTestVoidMethod
import FiedlerTestIntMethod
import FiedlerTestMethodWithParam
import FiedlerTestMultipleClasses
import FiedlerTestIfElse
import FiedlerTestWhile
import FiedlerSemCheckTestCorrectReturns
import FiedlerTestConstructor
import FiedlerTestCalculations
import FiedlerTestAssignments
import FiedlerTestInstances
import FiedlerTestInstanceCalls
import FiedlerTestInstanceVars
import FiedlerTestAccessFlags
import FiedlerTestBreakContinue
import FiedlerTestNull
import FiedlerSemCheckTestCorrectAssignments
import FiedlerTestThis
import FiedlerTestManyParams
import FiedlerSemCheckTestBoolInIfAndWhile
import FiedlerSemCheckTestMethodsExist
import FiedlerTestAccessFlagsMethods
import FiedlerSemCheckTestFalseConstructor
import FiedlerTestMethodEndAfterIf

{-
    main-Methode für alle Tests
    Autor: Dominik Sauerer
    Beschreibung: Ausgangspunkt für ausführbare Testdatei.
    Bei auszuführenden Tests bitte Kommentarsymbole entfernen
-}
main = do
    emptyJava_doTests
    attributes_doTests
    voidMethod_doTests
    intMethod_doTests
    methodWithParam_doTests
    multipleClasses_doTests
    ifElse_doTests
    while_doTests
    assignments_doTests
    calculations_doTests
    instances_doTests
    instanceCalls_doTests
    instanceVars_doTests
    constructor_doTests
    breakContinue_doTests
    methodEndAfterIf_doTests
    null_doTests
    manyParams_doTests
    this_doTests
    accessFlags_doTests
    accessFlagsMethods_doTests

    correctReturns_doTests
    correctAssignments_doTests
    falseConstructor_doTests
    methodsExist_doTests
    boolInIfAndWhile_doTests

    identifier_doTests