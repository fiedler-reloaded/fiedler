module SemantikCheck where
import AbstrakteSyntax
import Data.List
import Data.Maybe

-- author: Marcel Burbach

-- structure of this file:
-- starting functions + helper functions
-- checking statements + helper functions
-- checking expressions + helper functions
-- checking statement expressions + helper functions
-- general helper functions

---------------------------
-- ## starting functions --
---------------------------

-- point of entrance: typechecks whole program
typecheckPrg :: [Class] -> [Class]
typecheckPrg c = 
  typecheckClasses c c

-- recursive call to typecheck all classes after another
typecheckClasses :: [Class] -> [Class] -> [Class]
typecheckClasses [] _ = []
typecheckClasses (a:as) b = (typecheckClass a b : typecheckClasses as b)

-- typechecks a class
typecheckClass :: Class -> [Class] -> Class
typecheckClass (Class(acc, str, ClassBody(fields, methods))) cls =
  let 
    symtab = fillSymtab fields
    shouldBeTrue = checkConstructorExistence str methods
    ms = checkMethods methods symtab cls str
  in 
    if (shouldBeTrue == True) then
      Class(acc, str, ClassBody(fields, ms))
    else 
      error "semantic check error" 

-- recursive call to typecheck all methods after another
checkMethods :: [MethodDecl] -> [(Maybe Access, String, Type)] -> [Class] -> Type -> [MethodDecl]
checkMethods [] _ _ _ = []
checkMethods (m:ms) symtab cls class_name = (checkMethod m symtab cls class_name : checkMethods ms symtab cls class_name)

-- typechecks a method
checkMethod :: MethodDecl -> [(Maybe Access, String, Type)] -> [Class] -> Type -> MethodDecl
checkMethod (MethodDecl(a, t, methodName, f, s)) symtab cls class_name =
  let
    symtab_ready = mergeMethodSymtab f symtab
    stmt = typecheckStmt s symtab_ready cls t class_name
  in  
    if (getTypeFromStmt stmt == t) || ((typeNotPrimitive t) && ((getTypeFromStmt stmt) == "NullType")) then
      MethodDecl(a, t, methodName, f, stmt)
    else
      error "semantic check error"

checkMethod (ConstructorDecl(a, constructorName, f, s)) symtab cls class_name = 
  let
    symtab_ready = mergeMethodSymtab f symtab
    stmt = typecheckStmt s symtab_ready cls "void" class_name
  in
    ConstructorDecl(a, constructorName, f, stmt)

-- starting functions: HELPER FUNCTIONS ------------------------------------------------

-- typecheckClass: checks if constructors exist and if they are named after the class itself
checkConstructorExistence :: String -> [MethodDecl] -> Bool
checkConstructorExistence _ [] = True
checkConstructorExistence className (ConstructorDecl(_, constructorName, _, statement): tail) = 
  if (className == constructorName) then
    checkConstructorExistence className tail
  else
    error "semantik check error"
checkConstructorExistence className (MethodDecl(_, _, _, _, _): tail) = 
  checkConstructorExistence className tail

-- typecheckClass: fills class variables into the symtab
fillSymtab :: [FieldDecl] -> [(Maybe Access, String, Type)]
fillSymtab [] = []
fillSymtab (FieldDecl(a, t, s) : l) = ((a, s, t) : fillSymtab l)

-- checkMethod: fills method parameters into the symtab
mergeMethodSymtab :: [(Type, String)] -> [(Maybe Access, String, Type)] -> [(Maybe Access, String, Type)]
mergeMethodSymtab [] symtab = symtab 
mergeMethodSymtab ((ty,str):tail) symtab = ((Nothing, str, ty) : mergeMethodSymtab tail symtab)





------------------------------
-- ## get type functions ## --
------------------------------

-- returns type of stmt
getTypeFromStmt :: Stmt -> Type
getTypeFromStmt (TypedStmt(_, typ)) = typ

-- returns type of expr
getTypeFromExpr :: Expr -> Type
getTypeFromExpr (TypedExpr(_, typ)) = typ

-- returns type of stmtexpr
getTypeFromStmtExpr :: StmtExpr -> Type
getTypeFromStmtExpr (TypedStmtExpr(_, typ)) = typ





----------------------------
-- ## checking statements ##
----------------------------
typecheckStmt :: Stmt -> [(Maybe Access, String, Type)] -> [Class] -> Type -> Type -> Stmt

-- if: if without else
typecheckStmt (If(be, ifs, Nothing)) symtab cls expected_return class_name =
  let
    exp = typecheckExpr be symtab cls class_name
    ifstmt = typecheckStmt ifs symtab cls expected_return class_name
  in
    if ((getTypeFromExpr exp) == "boolean") then
      if (getTypeFromStmt ifstmt == "void") || (getTypeFromStmt ifstmt == expected_return) then
        TypedStmt(If(exp, ifstmt, Nothing), "void")
      else
        error "semantic check error"
    else                  
     error "semantic check error"

-- if: if with else
typecheckStmt (If(be, ifs, elses)) symtab cls expected_return class_name =
  let
    exp = typecheckExpr be symtab cls class_name
    ifstmt = typecheckStmt ifs symtab cls expected_return class_name
    elsestmt = typecheckStmt (fromJust elses) symtab cls expected_return class_name
  in
    if ((getTypeFromExpr exp) == "boolean") then
      if ((getTypeFromStmt ifstmt) == (getTypeFromStmt elsestmt)) && (getTypeFromStmt ifstmt == expected_return) then
        TypedStmt(If(exp, ifstmt, (Just elsestmt)), getTypeFromStmt ifstmt)
      else if (getTypeFromStmt ifstmt == "void") && (getTypeFromStmt elsestmt == expected_return) then
        TypedStmt(If(exp, ifstmt, (Just elsestmt)), getTypeFromStmt elsestmt)
      else if (getTypeFromStmt elsestmt == "void") && (getTypeFromStmt ifstmt == expected_return) then
        TypedStmt(If(exp, ifstmt, (Just elsestmt)), "void")
        else if (getTypeFromStmt elsestmt == "void") && (getTypeFromStmt ifstmt == "void") then
        TypedStmt(If(exp, ifstmt, (Just elsestmt)), "void")
      else
        error "semantic check error"
    else
     error "semantic check error"

-- elseif: if with else ifs
typecheckStmt (ElseIf(be, ifs, elses, Nothing)) symtab cls expected_return class_name = 
  let
    exp = typecheckExpr be symtab cls class_name
    ifstmt = typecheckStmt ifs symtab cls expected_return class_name
    elsestmt = typecheckIfElses elses symtab cls expected_return class_name
    ty = getFirstElseIfType elsestmt
    ifelse_ty = semancheckIfElses elsestmt ty
  in
    if ((getTypeFromExpr exp) == "boolean") then
      -- if and else if either have void or expected return
      if (((getTypeFromStmt ifstmt == "void") || (getTypeFromStmt ifstmt == expected_return)) && ((ty == "void") || (ty == expected_return))) then
        TypedStmt(ElseIf(exp, ifstmt, elsestmt, Nothing), "void")
      -- if sth has wrong return throw error 
      else
        error "semantic check error"
    else                  
     error "semantic check error"

-- elseif: if with else ifs and else
typecheckStmt (ElseIf(be, ifs, elses, Just onlyelse)) symtab cls expected_return class_name = 
  let
    exp = typecheckExpr be symtab cls class_name
    ifstmt = typecheckStmt ifs symtab cls expected_return class_name
    elsesty = typecheckIfElses elses symtab cls expected_return class_name
    elsestmt = typecheckStmt onlyelse symtab cls expected_return class_name
    ty = getFirstElseIfType elsesty
    ifelse_ty = semancheckIfElses elsesty ty
  in
    if ((getTypeFromExpr exp) == "boolean") then
      -- everything with expected return
      if ((getTypeFromStmt ifstmt) == (getTypeFromStmt elsestmt)) && ((getTypeFromStmt elsestmt) == ty) && (getTypeFromStmt ifstmt == expected_return) then
        TypedStmt(ElseIf(exp, ifstmt, elsesty, Just elsestmt), getTypeFromStmt elsestmt)
      -- else has expected return, others void (could be merged with first case)
      else if ((getTypeFromStmt ifstmt == "void") && (ty == "void")) && (getTypeFromStmt elsestmt == expected_return) then
        TypedStmt(ElseIf(exp, ifstmt, elsesty, Just elsestmt), getTypeFromStmt elsestmt)
      -- else is void, others either void or expected return
      else if (getTypeFromStmt elsestmt == "void") && (((getTypeFromStmt ifstmt == expected_return) && (ty == expected_return)) || ((getTypeFromStmt ifstmt == "void") && (ty == expected_return)) || ((getTypeFromStmt ifstmt == expected_return) && (ty == "void")) ||((getTypeFromStmt ifstmt == "void") && (ty == "void"))) then
        TypedStmt(ElseIf(exp, ifstmt, elsesty, Just elsestmt), "void")
      -- if sth has wrong return throw error
      else    
        TypedStmt(ElseIf(exp, ifstmt, elsesty, Just elsestmt), "void")
    else                  
     error "semantic check error"
    
-- while
typecheckStmt (While(be, s)) symtab cls expected_return class_name =
  let
    exp = typecheckExpr be symtab cls class_name
    stmt = typecheckStmt s symtab cls expected_return class_name
  in
    if ((getTypeFromExpr exp) == "boolean") then
      TypedStmt(While(exp, stmt), getTypeFromStmt stmt)
    else                  
     error "semantic check error"

-- break
typecheckStmt Break symtab cls expected_return class_name = 
  TypedStmt(Break, "void")

-- continue
typecheckStmt Continue symtab cls expected_return class_name = 
  TypedStmt(Continue, "void")

-- block
typecheckStmt (Block(sts)) symtab cls expected_return class_name = 
  let
    stmt = typecheckStmts sts symtab cls expected_return class_name
  in
    TypedStmt(Block(stmt), getTypeLastStmt stmt)

-- empty: void
typecheckStmt Empty symtab cls expected_return class_name = TypedStmt(Empty, "void")

-- returnVoid
typecheckStmt ReturnVoid symtab cls expected_return class_name = 
  if (expected_return == "void") then
    TypedStmt(ReturnVoid, "void")
  else 
    error "semantic check error"

-- return: with value
typecheckStmt (Return (e)) symtab cls expected_return class_name = 
  let
    typedexpr = typecheckExpr e symtab cls class_name
  in
    if (getTypeFromExpr typedexpr == expected_return) || ((typeNotPrimitive expected_return) && ((getTypeFromExpr typedexpr) == "NullType")) then
      TypedStmt(Return (typedexpr), getTypeFromExpr(typedexpr))
    else 
      error "semantic check error"

-- localVarDecl
typecheckStmt (LocalVarDecl(ty, str)) symtab cls expected_return class_name = 
  TypedStmt(LocalVarDecl(ty, str), "void")

-- stmtExprStmt
typecheckStmt (StmtExprStmt(stmtExpr)) symtab cls expected_return class_name = 
  let
    stmtExprTyped = typecheckStmtExpr stmtExpr symtab cls class_name
  in
    TypedStmt(StmtExprStmt(stmtExprTyped), getTypeFromStmtExpr stmtExprTyped)


-- stmt: HELPER FUNCTIONS -----------------------------------------

-- Block: check all stmts in a block and return typed versions
typecheckStmts :: [Stmt] -> [(Maybe Access, String, Type)] -> [Class] -> Type -> Type -> [Stmt]
typecheckStmts (stmt:[]) symtab cls expected_return class_name = [typecheckStmt stmt symtab cls expected_return class_name ]
typecheckStmts (stmt:stmts) symtab cls expected_return class_name = 
  let
    symtab_r = addToSymtab stmt symtab
  in
    (typecheckStmt stmt symtab cls expected_return class_name : typecheckStmts stmts symtab_r cls expected_return class_name)

-- Block: get type of last stmt in a block (will be the type of the block)
getTypeLastStmt :: [Stmt] -> Type
getTypeLastStmt (x:[]) = getTypeFromStmt x
getTypeLastStmt (x:xs) = getTypeLastStmt xs

-- ElseIf: typechecks all else if constructs and returns typed versions
typecheckIfElses :: [(Expr, Stmt)] -> [(Maybe Access, String, Type)] -> [Class] -> Type -> Type -> [(Expr, Stmt)]
typecheckIfElses (head:[]) symtab cls expected_return class_name = 
    let
    exp = typecheckExpr (fst(head)) symtab cls class_name
    stmt = typecheckStmt (snd(head)) symtab cls expected_return class_name
  in
    if (getTypeFromExpr exp == "boolean") then
      [(exp, stmt)]
    else
      error "semantic check error"

typecheckIfElses (head:tail) symtab cls expected_return class_name = 
  let
    symtab_r = addToSymtab stmt symtab
    exp = typecheckExpr (fst(head)) symtab cls class_name
    stmt = typecheckStmt (snd(head)) symtab cls expected_return class_name
  in
    if (getTypeFromExpr exp == "boolean") then
      ((exp, stmt) : typecheckIfElses tail symtab_r cls expected_return class_name)
    else
      error "semantic check error"

-- ElseIf: fetches type of first else if statement as reference to check all other else if types
getFirstElseIfType :: [(Expr, Stmt)] -> Type
getFirstElseIfType ((expr, stmt):tail) = (getTypeFromStmt stmt)

-- ElseIf: compares the types of all else if statements to see if they have the expected return or void
--         throws error if return is not void and doesn't match the expected return
semancheckIfElses :: [(Expr, Stmt)] -> Type -> Type
semancheckIfElses ((expr, stmt):[]) expected_from_others=
  if (getTypeFromStmt stmt == expected_from_others) then
    expected_from_others
  else if (getTypeFromStmt stmt == "void") then
    "void"
  else
    error "semantic check error"

semancheckIfElses ((expr, stmt):tail) expected_from_others = 
  if (getTypeFromStmt stmt == expected_from_others) then
    semancheckIfElses tail expected_from_others
  else if (getTypeFromStmt stmt == "void") then
    "void"
  else
    error "semantic check error"

-- typecheckIfElses(IfElse), typecheckStmts(Block): adds a declaration to the symtab 
addToSymtab :: Stmt -> [(Maybe Access, String, Type)] -> [(Maybe Access, String, Type)]
addToSymtab (LocalVarDecl(ty, str)) symtab = symtab ++ [(Nothing, str, ty)]
addToSymtab _ symtab = symtab 





-----------------------------
-- ## checking expressions ##
-----------------------------
typecheckExpr:: Expr -> [(Maybe Access, String, Type)] -> [Class] -> Type -> Expr

-- this: reference to the class in which the expression is called
typecheckExpr This symtab cls class_name = TypedExpr(This, class_name)

-- localOrFieldVar: reference to a variable within the same class
--                  throws error if variable can't be found
typecheckExpr (LocalOrFieldVar (str)) symtab cls class_name = 
  let
    erg = find (\(ac, var, ty) -> var == str) symtab
  in
  if erg /= Nothing then
    TypedExpr(LocalOrFieldVar (str), getThirdElement(fromJust erg))
  else
    error "semantic check error"

-- instVar: reference to a variable outside the class
--          throws error if class or variable can't be found.
typecheckExpr (InstVar(ex, str)) symtab cls class_name = 
  let
    exp = typecheckExpr ex symtab cls class_name
    cl = find (\(Class(c_a, c_t, ClassBody(fld, m))) -> (getTypeFromExpr exp) == c_t) cls
    ty = findDecl str (fromJust cl) class_name
  in 
    TypedExpr(InstVar(exp, str), ty)

-- unary: expressions like i++ or i!
typecheckExpr (Unary (str, expr)) symtab cls class_name = 
  let
    exp = typecheckExpr expr symtab cls class_name
  in 
    if (getTypeFromExpr exp == "boolean") then
      if (str == "!") then
        TypedExpr(Unary (str, exp), "boolean")
      else 
        error "semantic check error"
    else if (getTypeFromExpr exp == "int") then
      if (str == "+" || str == "++" || str == "-" || str == "--" || str == "~") then
        TypedExpr(Unary (str, exp), "int")
      else
        error "semantic check error"
    else
      error "semantic check error"

-- binary: arithmetic or boolean expressions with two parameters
typecheckExpr (Binary(str, ex1, ex2)) symtab cls class_name = 
  let
    exp1 = typecheckExpr ex1 symtab cls class_name
    exp2 = typecheckExpr ex2 symtab cls class_name
  in 
    if ((getTypeFromExpr exp1) == (getTypeFromExpr exp2)) || (((typeNotPrimitive (getTypeFromExpr exp1)) && ((getTypeFromExpr exp2) == "NullType")) || ((typeNotPrimitive (getTypeFromExpr exp2)) && ((getTypeFromExpr exp1) == "NullType")))  then
      if (str == "+" || str == "-" || str == "*" || str == "/" || str == "%" || str == "&" || str == "|" || str == "<<" || str == ">>" || str == "^") then
        TypedExpr(Binary(str, exp1, exp2), "int")
      else if (str == "==" || str == "!=" || str == ">" || str == ">=" || str == "<" || str == "<=" || str == "&&" || str == "||" || str == "?:" || str == "instanceof") then
        TypedExpr(Binary(str, exp1, exp2), "boolean")
      else 
        error "semantic check error"
    else
      error "semantic check error"

-- integer
typecheckExpr (Integer (str)) symtab cls class_name = TypedExpr(Integer str, "int")

-- bool
typecheckExpr (Bool (str)) symtab cls class_name = TypedExpr(Bool str, "boolean")

-- char
typecheckExpr (Char (str)) symtab cls class_name = TypedExpr(Char str, "char")

-- string
typecheckExpr (String (str)) symtab cls class_name = TypedExpr(String str, "String")

-- jNull
typecheckExpr Jnull symtab cls class_name = TypedExpr(Jnull, "NullType")

-- stmtExprExpr
typecheckExpr (StmtExprExpr(stmtexpr)) symtab cls class_name = 
  let 
    stmtexprtyped = typecheckStmtExpr stmtexpr symtab cls class_name
  in 
    TypedExpr(StmtExprExpr(stmtexprtyped), getTypeFromStmtExpr stmtexprtyped)


-- expr: HELPER FUNCTIONS --------------------------------------------------

-- instvar: searches for declaration of variable in a class
--          throws error if not found or class or variable are private/protected
findDecl :: String -> Class -> Type -> Type
findDecl (str) (Class(c_a, c_t, ClassBody(fld, m))) class_name= 
  let
    erg = find (\(FieldDecl(f_a, f_t, f_str)) -> str == f_str) fld
    tuple = getTypeOfFieldDecl(fromJust(erg))
  in 
    -- if variable from own class is called, access doesn't matter
    if (c_t == class_name) then
      tuple
    -- check access if variable from other class is called
    else if (check_access c_a) && (check_access(getAccessOfFieldDecl(fromJust(erg)))) then
      tuple
    else 
      error "semantic check error"

-- findDecl(instVar): returns type of field declaration
getTypeOfFieldDecl :: FieldDecl -> Type
getTypeOfFieldDecl (FieldDecl(_, b, _)) = b

-- findDecl(instVar): returns access of field declaration
getAccessOfFieldDecl :: FieldDecl -> Maybe Access
getAccessOfFieldDecl (FieldDecl(a, _, _)) = a





------------------------------------------
-- ## checking statement-expressions ## --
------------------------------------------
typecheckStmtExpr:: StmtExpr -> [(Maybe Access, String, Type)] -> [Class] -> Type -> StmtExpr

-- assign: checks if assigned value to variable fits, checks if null can be assigned
--         throws error if types don't match or null is not allowed
typecheckStmtExpr (Assign(str, ex)) symtab cls class_name =
  let
    exp = typecheckExpr ex symtab cls class_name
    erg = find (\(ac, var, ty) -> var == str) symtab
  in 
    if (erg  /= Nothing) then
      if (getTypeFromExpr exp == getThirdElement(fromJust erg) || ((getTypeFromExpr exp == "NullType" ) && (typeNotPrimitive (getThirdElement(fromJust erg))))) then
        TypedStmtExpr(Assign(str, exp), "void") 
      else
        error "semantic check error"
    else
      error "semantic check error"

-- new: instantiation of a class, calls constructor
--      throws error if class does not exist or no constructor with given parameters exists
typecheckStmtExpr (New(str, params)) symtab cls class_name = 
  let
    exps = typeExpressions params symtab cls class_name
    reverse_exps = reverseList exps
    expressionTypes = getExprTypes reverse_exps
    params_correct = checkConstructor str expressionTypes symtab cls class_name
  in
    if (params_correct == True) then
      TypedStmtExpr(New(str, reverse_exps), str)
    else
      error "semantic check error"

-- methodCall: call of method
--             throws error if class or method does not exist or if given parameters do not match
typecheckStmtExpr (MethodCall(exp, str, exs)) symtab cls class_name =
  let
    expp = typecheckExpr exp symtab cls class_name
    cl_str = getTypeFromExpr expp
    cl = find (\(Class(a, name, body)) -> name == cl_str) cls
    ty = getMethodType (fromJust cl) str (getNameOfClass (fromJust cl))
    exps = typeExpressions exs symtab cls class_name
    exps_reversed = reverseList exps
    expressionTypes = getExprTypes exps_reversed
    checker = checkMethodsParams (fromJust cl) str expressionTypes cls class_name
  in
    if (checker == True) then
      TypedStmtExpr(MethodCall(expp, str, exps_reversed), ty)
    else
      error "semantik check error"

-- stmtexpr: HELPER FUNCTIONS ------------------------------------------------

-- assign: checks if an expression is primitive or not (needed for null checking)
typeNotPrimitive :: Type -> Bool
typeNotPrimitive "boolean" = False
typeNotPrimitive "int" = False
typeNotPrimitive "char" = False
typeNotPrimitive _ = True

-- new: checks if class exists and then searches for a constructor
--      throws error if class can't be found or is private or protected 
checkConstructor :: String -> [Type] -> [(Maybe Access, String, Type)] -> [Class] -> Type -> Bool
checkConstructor str expressionTypes symtab cls class_name = 
  let
    cl = find (\(Class(a, name, body)) -> name == str) cls
  in
    if (getNameOfClass (fromJust cl) == class_name) || (check_access(getAccessFromClass(fromJust(cl)))) then
      searchForAppropriateConstructor (fromJust cl) expressionTypes class_name
    else
      error "semantic check error"

-- checkConstructor(new): returns name of class
getNameOfClass :: Class -> Type
getNameOfClass (Class(a, name, body)) = name

-- checkConstructor(new): checks if a constructor exists and then searches for one with fitting parameters
searchForAppropriateConstructor :: Class -> [Type] -> Type -> Bool
searchForAppropriateConstructor (Class(_, t, ClassBody(_, methodDecls))) expressionTypes class_name =
  if (checkConstructorExists methodDecls t class_name) then
    searchMethodDeclsForAppropriateConstructor methodDecls expressionTypes
  else
    expressionTypes == []
  
-- searchForApproriateConstructor(new): searches method decls of a class for a constructor
--                                      throws error if constructor is private or protected
checkConstructorExists :: [MethodDecl] -> Type -> Type -> Bool
checkConstructorExists [] _ _ = False
checkConstructorExists (ConstructorDecl(a, _, _, _) : tail) class_called class_in = 
  if (class_called == class_in) || (check_access a) then
    True
  else
    checkConstructorExists tail class_called class_in
checkConstructorExists (head: tail) class_called class_in = checkConstructorExists tail class_called class_in

-- searchForAppropriateConstructor(new): checks all constructors for one with fitting parameters
searchMethodDeclsForAppropriateConstructor :: [MethodDecl] -> [Type] -> Bool
searchMethodDeclsForAppropriateConstructor [] _ = False
searchMethodDeclsForAppropriateConstructor (MethodDecl(_, _, _, _, _) : tail) expressionTypes =
  searchMethodDeclsForAppropriateConstructor tail expressionTypes
searchMethodDeclsForAppropriateConstructor (ConstructorDecl(a, b, c, d) : tail) expressionTypes =
  compareParamTypesToExpr (ConstructorDecl(a, b, c, d)) expressionTypes || searchMethodDeclsForAppropriateConstructor tail expressionTypes

-- methodCall: returns type of method, if method can be found (expected return type)
getMethodType :: Class -> String -> Type -> Type
getMethodType (Class(_, t, ClassBody(_, methodDecls))) methodName class_name =
  getMethodTypeFromMethodDecls methodDecls methodName t class_name

-- getMethodType(methodCall): searches for method with the right name
--                            throws error if method is not found or access is private or protected
getMethodTypeFromMethodDecls :: [MethodDecl] -> String -> Type -> Type -> Type
getMethodTypeFromMethodDecls [] _ _ _ = error "semantic check error"
getMethodTypeFromMethodDecls (MethodDecl(a, methodType, methodName1, _, _) : tail) methodName2 class_called class_in
  | (methodName1 == methodName2) && ((class_called == class_in) || (check_access a)) = methodType
  | otherwise = getMethodTypeFromMethodDecls tail methodName2 class_called class_in
getMethodTypeFromMethodDecls (_ : tail) methodName class_called class_in= getMethodTypeFromMethodDecls tail methodName class_called class_in

-- methodCall: checks if class can be accessed and then searches for method
--             throws error if class is private or protected
checkMethodsParams :: Class -> String -> [Type] -> [Class] -> Type -> Bool
checkMethodsParams cl mtd_str expressionTypes cls class_name = 
    if (getNameOfClass cl == class_name) || (check_access(getAccessFromClass cl)) then
      searchForAppropriateMethod cl mtd_str expressionTypes class_name
    else
      error "semantic check error"

-- checkMethodsParams(methodCall): searches methods of a class to find the referenced one
searchForAppropriateMethod :: Class -> String -> [Type] -> Type -> Bool
searchForAppropriateMethod (Class(_, t, ClassBody(_, methodDecls))) methodName expressionTypes class_name =
    searchMethodDeclsForAppropriateMethod methodDecls methodName expressionTypes t class_name

-- searchForAppropriateMethod(methodCall): checks if parameters of method fit with the given ones
searchMethodDeclsForAppropriateMethod :: [MethodDecl] -> String -> [Type] -> Type -> Type -> Bool
searchMethodDeclsForAppropriateMethod [] _ _ _ _ = False
searchMethodDeclsForAppropriateMethod (MethodDecl(a, b, c, d, e) : tail) callMethodName expressionTypes class_expected class_in
  | (callMethodName == c) && ((class_expected == class_in) || (check_access a)) = compareParamTypesToExpr (MethodDecl(a, b, c, d, e)) expressionTypes || searchMethodDeclsForAppropriateMethod tail callMethodName expressionTypes class_expected class_in
  | otherwise = searchMethodDeclsForAppropriateMethod tail callMethodName expressionTypes class_expected class_in
searchMethodDeclsForAppropriateMethod (ConstructorDecl(_, _, _, _) : tail) callMethodName expressionTypes class_expected class_in =
  searchMethodDeclsForAppropriateMethod tail callMethodName expressionTypes class_expected class_in

-- new, methodCall, checkConstructor(new): typechecks expressions and returns typed versions
typeExpressions :: [Expr] -> [(Maybe Access, String, Type)] -> [Class] -> Type -> [Expr]
typeExpressions [] symtab cls  _ = []
typeExpressions (exp:exps) symtab cls class_name = 
    (typeExpressions exps symtab cls class_name ++ [typecheckExpr exp symtab cls class_name])

-- new, methodCall, checkConstructor(new): reverses a list
reverseList :: [Expr] -> [Expr]
reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

-- checkMethodsParam(methodCall), checkConstructor(new): returns access of class
getAccessFromClass :: Class -> Maybe Access
getAccessFromClass (Class(a, name, body)) = a

-- searchMethodDeclsForAppropriateConstructor(new), searchMethodDeclsForAppropriateMethod(methodCall): compares parameters to the ones asked for either in a method or a constructor
compareParamTypesToExpr :: MethodDecl -> [Type] -> Bool
compareParamTypesToExpr (ConstructorDecl(_, _, params, _)) expressionTypes =
  let
    methodParamTypes = getMethodParamTypes params
  in
    compareMethodTypesToExprTypes methodParamTypes expressionTypes
    
compareParamTypesToExpr (MethodDecl(_, _, _, params, _)) expressionTypes =
  let
    methodParamTypes = getMethodParamTypes params
  in
    compareMethodTypesToExprTypes methodParamTypes expressionTypes

-- compareParamTypesToExpr(new, MethodCall): retrieves types of parameters from a method
getMethodParamTypes :: [(Type,String)] -> [Type]
getMethodParamTypes [] = []
getMethodParamTypes ((currentType, _) : tail) = 
  (currentType : (getMethodParamTypes tail))

-- checkMethodsParam(methodCall), checkConstructor(new): retrieves types of expressions
getExprTypes :: [Expr] -> [Type]
getExprTypes [] = []
getExprTypes (currentExpr : tail) =
  (getTypeFromExpr currentExpr : (getExprTypes tail))

-- compareParamTypesToExpr(new, methodCall): compares a list of types
compareMethodTypesToExprTypes :: [Type] -> [Type] -> Bool
compareMethodTypesToExprTypes [] [] = True
compareMethodTypesToExprTypes [] _ = False
compareMethodTypesToExprTypes _ [] = False
compareMethodTypesToExprTypes (paramType : paramTail) (exprType : exprTail)
    | paramType == exprType = True && compareMethodTypesToExprTypes paramTail exprTail
    | otherwise = False





------------------------------------------
-- ## general helper functions ## --
------------------------------------------

-- findDecl(instVar), new, methodCall: checks access of class or variable 
check_access :: Maybe Access -> Bool
check_access (Just Private) = False
check_access (Just Protected) = False
check_access _ = True

-- localOrFieldVar, assign: returns third element of a tuple
getThirdElement :: (a, b, c) -> c
getThirdElement (a, b, c) = c