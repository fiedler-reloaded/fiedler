public class AccessFlagsMethods {
    Helper helper;
    public AccessFlagsMethods() {
        helper = new Helper();
    }
    public char test1() {
        return helper.doAnotherThing('p');
    }
}

public class Helper {
    private void doSomething() {

    }
    protected int doIt(int x) {
        return 1;
    }
    public char doAnotherThing(char z) {
        return z;
    }
}