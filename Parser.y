{
module Parser (parser) where
import AbstrakteSyntax
import Scanner
}

%name program
%tokentype { Token }
%error { parseError }
  
%token 
	CLASS { CLASS }
	IDENTIFIER { IDENTIFIER $$ }
	OPENCUR { OPENCUR }
	CLOSECUR { CLOSECUR }
	PUBLIC { PUBLIC }
	PROTECTED { PROTECTED }
	PRIVATE { PRIVATE }
	STATIC { STATIC }
	BOOLEAN { BOOLEAN }
	INTEGER { INTEGER }
	CHAR { CHAR }
	SEMICOLON { SEMICOLON }
	VOID { VOID }
	OPENBR { OPENBR }
	CLOSEBR { CLOSEBR }
	COMMA { COMMA }
	IF { IF }
	ELSE { ELSE }
	THIS { THIS }
	BOOLLITERAL { BOOLLITERAL $$ }
	EQUALS { EQUALS }
    GREATERTHAN { GREATERTHAN }
    LESSTHAN { LESSTHAN }
    GREATERTHANE { GREATERTHANE }
    LESSTHANE { LESSTHANE }
	INTLITERAL { INTLITERAL $$}
	CHARLITERAL { CHARLITERAL $$}
	STRINGLITERAL { STRINGLITERAL $$}
	NOTEQUAL { NOTEQUAL }
	WHILE { WHILE }
	NEW { NEW }
	PLUS { PLUS }
	MINUS { MINUS }
	MULT { MULT }
	DIV { DIV }
	MODULO { MODULO }
	COMPLEMENT { COMPLEMENT }
	BAND { BAND }
	BOR { BOR }
	BXOR { BXOR }
	LSHIFT { LSHIFT }
	RSHIFT { RSHIFT }
	RETURN { RETURN }
	DECREMENT { DECREMENT }
	INCREMENT { INCREMENT }
	DOT { DOT }
	JNULL { JNULL }
	BREAK { BREAK }
	CONTINUE { CONTINUE }
	EXCLAMATION { EXCLAMATION }
	LOGIOR { LOGIOR }
	LOGIAND { LOGIAND }


%%

program : classDeclaration { [$1] }
	| program classDeclaration { ($2:$1) }

classDeclaration : CLASS IDENTIFIER classBody { Class(Nothing, $2, $3) }
				| access CLASS IDENTIFIER classBody { Class(Just $1, $3, $4) }

classBody : OPENCUR CLOSECUR  { ClassBody([], []) }
		  | OPENCUR classBodyDeclarations CLOSECUR { $2 }


classBodyDeclarations : classBodyDeclaration { $1 }
						| classBodyDeclarations classBodyDeclaration { ClassBody(getFields($1)++getFields($2),getMeths($1)++getMeths($2)) }

classBodyDeclaration: fieldDeclaration { ClassBody([$1], []) }
					| methodDeclaration { ClassBody([], [$1]) }

fieldDeclaration : type IDENTIFIER SEMICOLON { FieldDecl(Nothing, $1, $2) }
 		 		| access type IDENTIFIER  SEMICOLON { FieldDecl(Just $1, $2, $3) }

type: INTEGER { "int" }
	| BOOLEAN { "boolean" }
	| CHAR { "char" }
	| IDENTIFIER { $1 }

access : PUBLIC { Public }
		| PROTECTED { Protected }
        | PRIVATE { Private }
		| STATIC { Static }

methodDeclaration : IDENTIFIER paramList methStatement { ConstructorDecl(Nothing, $1, $2, $3) }
					| access IDENTIFIER paramList methStatement { ConstructorDecl(Just $1, $2, $3, $4) }
					| type IDENTIFIER paramList methStatement { MethodDecl(Nothing, $1, $2, $3, $4)}
					| access type IDENTIFIER paramList methStatement { MethodDecl(Just $1, $2, $3, $4, $5)}
					| VOID IDENTIFIER paramList methStatement { MethodDecl(Nothing, "void", $2, $3, $4)}
					| access VOID IDENTIFIER paramList methStatement { MethodDecl(Just $1, "void", $3, $4, $5)}

paramList : OPENBR CLOSEBR { [] }
			| OPENBR params CLOSEBR { $2 }

params : param { [$1] }
		| param COMMA params { ($1:$3) }

param: type IDENTIFIER { ($1,$2) }

exprList : OPENBR CLOSEBR { [] }
		| OPENBR exprs CLOSEBR { $2 }

exprs : Expr { [$1] }
		| exprs COMMA Expr { ($3:$1) }

elifs : elif { [$1] }
		| elifs elif{ ($2:$1) }

elif : ELSE IF OPENBR ConditionalExprs CLOSEBR methStatement { ($4,$6) }

methStatement: OPENCUR CLOSECUR { Empty }
			| OPENCUR statement CLOSECUR { $2 }
			| OPENCUR  block CLOSECUR { Block($2) }
			
block : statement { [$1] }
		| statement block { ($1:$2) } 

statement : type IDENTIFIER SEMICOLON { LocalVarDecl($1, $2) }
			| IF OPENBR ConditionalExprs CLOSEBR methStatement { If($3, $5, Nothing)}
			| IF OPENBR ConditionalExprs CLOSEBR methStatement ELSE methStatement { If($3, $5, Just $7)}
			| IF OPENBR ConditionalExprs CLOSEBR methStatement elifs { ElseIf($3, $5 , reverseList($6), Nothing) }
			| IF OPENBR ConditionalExprs CLOSEBR methStatement elifs ELSE methStatement { ElseIf($3, $5 , reverseList($6), Just $8) }
			| WHILE OPENBR ConditionalExprs CLOSEBR methStatement { While($3,$5) }
			| statementExpression SEMICOLON { StmtExprStmt($1) }
			| BREAK SEMICOLON { Break }
			| CONTINUE SEMICOLON { Continue }
			| RETURN Expr SEMICOLON { Return($2) }
			| RETURN SEMICOLON { ReturnVoid }

statementExpression : IDENTIFIER EQUALS Expr { Assign($1,$3) }
					| NEW IDENTIFIER exprList { New($2,reverseList($3)) }
					| IDENTIFIER DOT IDENTIFIER exprList { MethodCall(LocalOrFieldVar($1),$3,reverseList($4)) }
					| THIS DOT IDENTIFIER exprList { MethodCall(This,$3,reverseList($4)) }
          			
					

Expr : THIS { This }
	| IDENTIFIER INCREMENT { Unary("++",LocalOrFieldVar($1)) }
	| IDENTIFIER DECREMENT { Unary("--",LocalOrFieldVar($1)) }
	| COMPLEMENT IDENTIFIER { Unary("~",LocalOrFieldVar($2)) }
	| IDENTIFIER { LocalOrFieldVar($1) }
	| NEW IDENTIFIER exprList { StmtExprExpr(New($2,reverseList($3)))}
	| IDENTIFIER DOT IDENTIFIER exprList { StmtExprExpr(MethodCall(LocalOrFieldVar($1),$3,reverseList($4))) }
	| IDENTIFIER DOT IDENTIFIER { InstVar(LocalOrFieldVar($1), $3) }
	| THIS DOT IDENTIFIER exprList { StmtExprExpr(MethodCall(This,$3,reverseList($4))) }
	| THIS DOT IDENTIFIER { InstVar(This, $3) }
	| BOOLLITERAL { Bool($1) }
	| CHARLITERAL { Char($1) }
	| INTLITERAL { Integer($1) }
	| JNULL { Jnull }
	| STRINGLITERAL { String($1) }
	| OPENBR Expr CLOSEBR { $2 }
	| Expr LogicalOperator Expr { Binary($2,$1,$3) }
	| Expr ArithmeticOperator Expr { Binary($2,$1,$3) }

ConditionalExprs : ExprWithoutArithmeticOps { $1 }
				| ExprWithoutArithmeticOps ExprOperator ExprWithoutArithmeticOps { Binary($2,$1,$3) }

ExprWithoutArithmeticOps : THIS { This }
	| OPENBR ExprWithoutArithmeticOps CLOSEBR { $2 }
	| IDENTIFIER { LocalOrFieldVar($1) }
	| BOOLLITERAL { Bool($1) }
	| CHARLITERAL { Char($1) }
	| INTLITERAL { Integer($1) }
	| JNULL { Jnull }
	| STRINGLITERAL { String($1) }
	| NEW IDENTIFIER exprList { StmtExprExpr(New($2,reverseList($3)))}
	| IDENTIFIER DOT IDENTIFIER exprList { StmtExprExpr(MethodCall(LocalOrFieldVar($1),$3,reverseList($4))) }
	| IDENTIFIER DOT IDENTIFIER { InstVar(LocalOrFieldVar($1), $3) }
	| THIS DOT IDENTIFIER exprList { StmtExprExpr(MethodCall(This,$3,reverseList($4))) }
	| THIS DOT IDENTIFIER { InstVar(This, $3) }
	| IDENTIFIER INCREMENT { Unary("+",LocalOrFieldVar($1)) }
	| IDENTIFIER DECREMENT { Unary("-",LocalOrFieldVar($1)) }
	| EXCLAMATION IDENTIFIER { Unary("!",LocalOrFieldVar($2)) }
	| ExprWithoutArithmeticOps LogicalOperator ExprWithoutArithmeticOps { Binary($2,$1,$3) }


LogicalOperator : EQUALS EQUALS { "==" }
				| GREATERTHAN { ">" }
				| LESSTHAN { "<" }
    			| GREATERTHANE { ">=" }
    			| LESSTHANE { "<=" }
				| NOTEQUAL { "!=" }
				
ExprOperator: LOGIOR { "||" }
				| LOGIAND{ "&&" }

ArithmeticOperator : EQUALS { "=" }
					| PLUS { "+" }
					| MINUS { "-" }
					| MULT { "*"}
					| DIV { "/" }
					| MODULO { "%" }
					| BOR { "|" }
					| BAND { "&" }
					| BXOR { "^" }
					| LSHIFT { "<<" }
					| RSHIFT { ">>" }


{
getFields :: ClassBody -> [FieldDecl]
getFields (ClassBody(fs,ms)) = fs

getMeths :: ClassBody -> [MethodDecl]
getMeths (ClassBody(fs,ms)) = ms

reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

parseError :: [Token] -> a
parseError _ = error "Parse error"

parser :: String -> [Class]
parser = program . alexScanTokens



mainP = do
	s <- readFile "eingabe.java"
	print (alexScanTokens s)
	print (parser s)
}
