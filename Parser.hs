{-# OPTIONS_GHC -w #-}
module Parser (parser) where
import AbstrakteSyntax
import Scanner
import qualified Data.Array as Happy_Data_Array
import qualified Data.Bits as Bits
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.20.0

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25 t26 t27 t28 t29
	= HappyTerminal (Token)
	| HappyErrorToken Prelude.Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18
	| HappyAbsSyn19 t19
	| HappyAbsSyn20 t20
	| HappyAbsSyn21 t21
	| HappyAbsSyn22 t22
	| HappyAbsSyn23 t23
	| HappyAbsSyn24 t24
	| HappyAbsSyn25 t25
	| HappyAbsSyn26 t26
	| HappyAbsSyn27 t27
	| HappyAbsSyn28 t28
	| HappyAbsSyn29 t29

happyExpList :: Happy_Data_Array.Array Prelude.Int Prelude.Int
happyExpList = Happy_Data_Array.listArray (0,403) ([0,8192,30,0,0,0,32768,120,0,0,0,0,0,0,0,0,0,8,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,32768,120,0,0,0,0,0,0,0,0,0,32,0,0,0,0,64,0,0,0,0,512,0,0,0,0,0,0,0,0,0,53248,191,0,0,0,16384,767,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,57408,2,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,16,0,0,0,2048,0,0,0,0,4096,568,0,0,0,16384,0,0,0,0,0,4096,0,0,0,0,4,0,0,0,0,16384,1,0,0,0,0,0,0,0,0,0,0,0,0,0,2048,0,0,0,0,0,0,0,0,0,0,1024,0,0,0,0,2,0,0,0,0,20480,0,0,0,0,16,0,0,0,0,0,8,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,33669,12290,49664,0,0,8,0,0,0,0,0,0,0,0,0,64,0,0,0,0,1024,0,0,0,0,1024,2574,192,776,0,0,64,0,0,0,0,0,2,1024,0,0,0,0,0,0,0,16384,0,0,0,0,0,0,0,1,0,0,4,0,0,0,256,0,0,0,0,1024,6224,8348,128,0,0,64,0,0,0,0,256,0,0,0,0,897,0,0,0,0,0,0,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8192,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,57360,57123,7,0,0,0,0,448,0,0,0,0,0,0,0,4097,9990,8200,0,0,0,0,16384,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4096,0,0,0,0,16384,0,0,0,0,0,0,0,0,0,0,16384,0,0,0,0,16,28769,2,18,0,64,0,0,0,0,256,1552,39,288,0,1024,6208,8348,128,0,4096,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3588,49162,2048,3,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,256,0,0,0,0,0,62014,125,0,0,8192,0,0,0,0,0,9184,0,24,0,0,0,49152,1,0,64,49540,9,72,0,0,0,0,16,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,16,0,0,0,0,128,0,0,0,0,0,0,0,0,16384,35840,2497,2050,0,0,0,0,0,0,0,16384,0,0,0,0,16,0,0,0,0,0,15880,32242,0,0,0,0,0,0,0,0,0,0,0,0,4096,0,0,0,0,16384,33792,2497,2050,0,0,4097,9990,8200,0,0,0,0,0,0,0,0,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,1024,0,0,0,0,0,0,0,0,0,32768,1,0,0,0,0,36736,8060,0,0,0,0,0,0,0,512,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1024,0,0,0,0,1,0,0,0,0,32768,9184,0,0,0,0,0,0,0,0,0,0,0,0,0,256,0,0,0,0,1024,6208,156,1152,0,4096,24832,624,4608,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,32,0,0,0,0,0,0,0,0,0,0,256,0,0,0,0,57344,35,0,0,0,0,0,0,0,0,1024,0,0,0,0,0,0,0,0,0,16384,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1024,6208,8348,128,0,0,0,0,0,0,0,0,0,0,0,0,0,51448,503,0,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,2048,512,0,0,0,0,0,0,0,0,0,1024,0,0,0,0,0,0,0,0,0,8,2,0,0,0,0,0,0,0,0,64,49540,9,72,0,0,32,0,0,0,2048,0,0,0,0,0,0,0,0,0
	])

{-# NOINLINE happyExpListPerState #-}
happyExpListPerState st =
    token_strs_expected
  where token_strs = ["error","%dummy","%start_program","program","classDeclaration","classBody","classBodyDeclarations","classBodyDeclaration","fieldDeclaration","type","access","methodDeclaration","paramList","params","param","exprList","exprs","elifs","elif","methStatement","block","statement","statementExpression","Expr","ConditionalExprs","ExprWithoutArithmeticOps","LogicalOperator","ExprOperator","ArithmeticOperator","CLASS","IDENTIFIER","OPENCUR","CLOSECUR","PUBLIC","PROTECTED","PRIVATE","STATIC","BOOLEAN","INTEGER","CHAR","SEMICOLON","VOID","OPENBR","CLOSEBR","COMMA","IF","ELSE","THIS","BOOLLITERAL","EQUALS","GREATERTHAN","LESSTHAN","GREATERTHANE","LESSTHANE","INTLITERAL","CHARLITERAL","STRINGLITERAL","NOTEQUAL","WHILE","NEW","PLUS","MINUS","MULT","DIV","MODULO","COMPLEMENT","BAND","BOR","BXOR","LSHIFT","RSHIFT","RETURN","DECREMENT","INCREMENT","DOT","JNULL","BREAK","CONTINUE","EXCLAMATION","LOGIOR","LOGIAND","%eof"]
        bit_start = st Prelude.* 82
        bit_end = (st Prelude.+ 1) Prelude.* 82
        read_bit = readArrayBit happyExpList
        bits = Prelude.map read_bit [bit_start..bit_end Prelude.- 1]
        bits_indexed = Prelude.zip bits [0..81]
        token_strs_expected = Prelude.concatMap f bits_indexed
        f (Prelude.False, _) = []
        f (Prelude.True, nr) = [token_strs Prelude.!! nr]

action_0 (30) = happyShift action_4
action_0 (34) = happyShift action_5
action_0 (35) = happyShift action_6
action_0 (36) = happyShift action_7
action_0 (37) = happyShift action_8
action_0 (4) = happyGoto action_9
action_0 (5) = happyGoto action_2
action_0 (11) = happyGoto action_3
action_0 _ = happyFail (happyExpListPerState 0)

action_1 (30) = happyShift action_4
action_1 (34) = happyShift action_5
action_1 (35) = happyShift action_6
action_1 (36) = happyShift action_7
action_1 (37) = happyShift action_8
action_1 (5) = happyGoto action_2
action_1 (11) = happyGoto action_3
action_1 _ = happyFail (happyExpListPerState 1)

action_2 _ = happyReduce_1

action_3 (30) = happyShift action_12
action_3 _ = happyFail (happyExpListPerState 3)

action_4 (31) = happyShift action_11
action_4 _ = happyFail (happyExpListPerState 4)

action_5 _ = happyReduce_17

action_6 _ = happyReduce_18

action_7 _ = happyReduce_19

action_8 _ = happyReduce_20

action_9 (30) = happyShift action_4
action_9 (34) = happyShift action_5
action_9 (35) = happyShift action_6
action_9 (36) = happyShift action_7
action_9 (37) = happyShift action_8
action_9 (82) = happyAccept
action_9 (5) = happyGoto action_10
action_9 (11) = happyGoto action_3
action_9 _ = happyFail (happyExpListPerState 9)

action_10 _ = happyReduce_2

action_11 (32) = happyShift action_15
action_11 (6) = happyGoto action_14
action_11 _ = happyFail (happyExpListPerState 11)

action_12 (31) = happyShift action_13
action_12 _ = happyFail (happyExpListPerState 12)

action_13 (32) = happyShift action_15
action_13 (6) = happyGoto action_28
action_13 _ = happyFail (happyExpListPerState 13)

action_14 _ = happyReduce_3

action_15 (31) = happyShift action_22
action_15 (33) = happyShift action_23
action_15 (34) = happyShift action_5
action_15 (35) = happyShift action_6
action_15 (36) = happyShift action_7
action_15 (37) = happyShift action_8
action_15 (38) = happyShift action_24
action_15 (39) = happyShift action_25
action_15 (40) = happyShift action_26
action_15 (42) = happyShift action_27
action_15 (7) = happyGoto action_16
action_15 (8) = happyGoto action_17
action_15 (9) = happyGoto action_18
action_15 (10) = happyGoto action_19
action_15 (11) = happyGoto action_20
action_15 (12) = happyGoto action_21
action_15 _ = happyFail (happyExpListPerState 15)

action_16 (31) = happyShift action_22
action_16 (33) = happyShift action_37
action_16 (34) = happyShift action_5
action_16 (35) = happyShift action_6
action_16 (36) = happyShift action_7
action_16 (37) = happyShift action_8
action_16 (38) = happyShift action_24
action_16 (39) = happyShift action_25
action_16 (40) = happyShift action_26
action_16 (42) = happyShift action_27
action_16 (8) = happyGoto action_36
action_16 (9) = happyGoto action_18
action_16 (10) = happyGoto action_19
action_16 (11) = happyGoto action_20
action_16 (12) = happyGoto action_21
action_16 _ = happyFail (happyExpListPerState 16)

action_17 _ = happyReduce_7

action_18 _ = happyReduce_9

action_19 (31) = happyShift action_35
action_19 _ = happyFail (happyExpListPerState 19)

action_20 (31) = happyShift action_33
action_20 (38) = happyShift action_24
action_20 (39) = happyShift action_25
action_20 (40) = happyShift action_26
action_20 (42) = happyShift action_34
action_20 (10) = happyGoto action_32
action_20 _ = happyFail (happyExpListPerState 20)

action_21 _ = happyReduce_10

action_22 (43) = happyShift action_31
action_22 (13) = happyGoto action_30
action_22 _ = happyReduce_16

action_23 _ = happyReduce_5

action_24 _ = happyReduce_14

action_25 _ = happyReduce_13

action_26 _ = happyReduce_15

action_27 (31) = happyShift action_29
action_27 _ = happyFail (happyExpListPerState 27)

action_28 _ = happyReduce_4

action_29 (43) = happyShift action_31
action_29 (13) = happyGoto action_50
action_29 _ = happyFail (happyExpListPerState 29)

action_30 (32) = happyShift action_49
action_30 (20) = happyGoto action_48
action_30 _ = happyFail (happyExpListPerState 30)

action_31 (31) = happyShift action_46
action_31 (38) = happyShift action_24
action_31 (39) = happyShift action_25
action_31 (40) = happyShift action_26
action_31 (44) = happyShift action_47
action_31 (10) = happyGoto action_43
action_31 (14) = happyGoto action_44
action_31 (15) = happyGoto action_45
action_31 _ = happyFail (happyExpListPerState 31)

action_32 (31) = happyShift action_42
action_32 _ = happyFail (happyExpListPerState 32)

action_33 (43) = happyShift action_31
action_33 (13) = happyGoto action_41
action_33 _ = happyReduce_16

action_34 (31) = happyShift action_40
action_34 _ = happyFail (happyExpListPerState 34)

action_35 (41) = happyShift action_39
action_35 (43) = happyShift action_31
action_35 (13) = happyGoto action_38
action_35 _ = happyFail (happyExpListPerState 35)

action_36 _ = happyReduce_8

action_37 _ = happyReduce_6

action_38 (32) = happyShift action_49
action_38 (20) = happyGoto action_72
action_38 _ = happyFail (happyExpListPerState 38)

action_39 _ = happyReduce_11

action_40 (43) = happyShift action_31
action_40 (13) = happyGoto action_71
action_40 _ = happyFail (happyExpListPerState 40)

action_41 (32) = happyShift action_49
action_41 (20) = happyGoto action_70
action_41 _ = happyFail (happyExpListPerState 41)

action_42 (41) = happyShift action_69
action_42 (43) = happyShift action_31
action_42 (13) = happyGoto action_68
action_42 _ = happyFail (happyExpListPerState 42)

action_43 (31) = happyShift action_67
action_43 _ = happyFail (happyExpListPerState 43)

action_44 (44) = happyShift action_66
action_44 _ = happyFail (happyExpListPerState 44)

action_45 (45) = happyShift action_65
action_45 _ = happyReduce_29

action_46 _ = happyReduce_16

action_47 _ = happyReduce_27

action_48 _ = happyReduce_21

action_49 (31) = happyShift action_56
action_49 (33) = happyShift action_57
action_49 (38) = happyShift action_24
action_49 (39) = happyShift action_25
action_49 (40) = happyShift action_26
action_49 (46) = happyShift action_58
action_49 (48) = happyShift action_59
action_49 (59) = happyShift action_60
action_49 (60) = happyShift action_61
action_49 (72) = happyShift action_62
action_49 (77) = happyShift action_63
action_49 (78) = happyShift action_64
action_49 (10) = happyGoto action_52
action_49 (21) = happyGoto action_53
action_49 (22) = happyGoto action_54
action_49 (23) = happyGoto action_55
action_49 _ = happyFail (happyExpListPerState 49)

action_50 (32) = happyShift action_49
action_50 (20) = happyGoto action_51
action_50 _ = happyFail (happyExpListPerState 50)

action_51 _ = happyReduce_25

action_52 (31) = happyShift action_101
action_52 _ = happyFail (happyExpListPerState 52)

action_53 (33) = happyShift action_100
action_53 _ = happyFail (happyExpListPerState 53)

action_54 (31) = happyShift action_56
action_54 (33) = happyShift action_99
action_54 (38) = happyShift action_24
action_54 (39) = happyShift action_25
action_54 (40) = happyShift action_26
action_54 (46) = happyShift action_58
action_54 (48) = happyShift action_59
action_54 (59) = happyShift action_60
action_54 (60) = happyShift action_61
action_54 (72) = happyShift action_62
action_54 (77) = happyShift action_63
action_54 (78) = happyShift action_64
action_54 (10) = happyGoto action_52
action_54 (21) = happyGoto action_97
action_54 (22) = happyGoto action_98
action_54 (23) = happyGoto action_55
action_54 _ = happyFail (happyExpListPerState 54)

action_55 (41) = happyShift action_96
action_55 _ = happyFail (happyExpListPerState 55)

action_56 (50) = happyShift action_94
action_56 (75) = happyShift action_95
action_56 _ = happyReduce_16

action_57 _ = happyReduce_39

action_58 (43) = happyShift action_93
action_58 _ = happyFail (happyExpListPerState 58)

action_59 (75) = happyShift action_92
action_59 _ = happyFail (happyExpListPerState 59)

action_60 (43) = happyShift action_91
action_60 _ = happyFail (happyExpListPerState 60)

action_61 (31) = happyShift action_90
action_61 _ = happyFail (happyExpListPerState 61)

action_62 (31) = happyShift action_79
action_62 (41) = happyShift action_80
action_62 (43) = happyShift action_81
action_62 (48) = happyShift action_82
action_62 (49) = happyShift action_83
action_62 (55) = happyShift action_84
action_62 (56) = happyShift action_85
action_62 (57) = happyShift action_86
action_62 (60) = happyShift action_87
action_62 (66) = happyShift action_88
action_62 (76) = happyShift action_89
action_62 (24) = happyGoto action_78
action_62 _ = happyFail (happyExpListPerState 62)

action_63 (41) = happyShift action_77
action_63 _ = happyFail (happyExpListPerState 63)

action_64 (41) = happyShift action_76
action_64 _ = happyFail (happyExpListPerState 64)

action_65 (31) = happyShift action_46
action_65 (38) = happyShift action_24
action_65 (39) = happyShift action_25
action_65 (40) = happyShift action_26
action_65 (10) = happyGoto action_43
action_65 (14) = happyGoto action_75
action_65 (15) = happyGoto action_45
action_65 _ = happyFail (happyExpListPerState 65)

action_66 _ = happyReduce_28

action_67 _ = happyReduce_31

action_68 (32) = happyShift action_49
action_68 (20) = happyGoto action_74
action_68 _ = happyFail (happyExpListPerState 68)

action_69 _ = happyReduce_12

action_70 _ = happyReduce_22

action_71 (32) = happyShift action_49
action_71 (20) = happyGoto action_73
action_71 _ = happyFail (happyExpListPerState 71)

action_72 _ = happyReduce_23

action_73 _ = happyReduce_26

action_74 _ = happyReduce_24

action_75 _ = happyReduce_30

action_76 _ = happyReduce_52

action_77 _ = happyReduce_51

action_78 (41) = happyShift action_130
action_78 (50) = happyShift action_131
action_78 (51) = happyShift action_132
action_78 (52) = happyShift action_133
action_78 (53) = happyShift action_134
action_78 (54) = happyShift action_135
action_78 (58) = happyShift action_136
action_78 (61) = happyShift action_137
action_78 (62) = happyShift action_138
action_78 (63) = happyShift action_139
action_78 (64) = happyShift action_140
action_78 (65) = happyShift action_141
action_78 (67) = happyShift action_142
action_78 (68) = happyShift action_143
action_78 (69) = happyShift action_144
action_78 (70) = happyShift action_145
action_78 (71) = happyShift action_146
action_78 (27) = happyGoto action_128
action_78 (29) = happyGoto action_129
action_78 _ = happyFail (happyExpListPerState 78)

action_79 (73) = happyShift action_125
action_79 (74) = happyShift action_126
action_79 (75) = happyShift action_127
action_79 _ = happyReduce_63

action_80 _ = happyReduce_54

action_81 (31) = happyShift action_79
action_81 (43) = happyShift action_81
action_81 (48) = happyShift action_82
action_81 (49) = happyShift action_83
action_81 (55) = happyShift action_84
action_81 (56) = happyShift action_85
action_81 (57) = happyShift action_86
action_81 (60) = happyShift action_87
action_81 (66) = happyShift action_88
action_81 (76) = happyShift action_89
action_81 (24) = happyGoto action_124
action_81 _ = happyFail (happyExpListPerState 81)

action_82 (75) = happyShift action_123
action_82 _ = happyReduce_59

action_83 _ = happyReduce_69

action_84 _ = happyReduce_71

action_85 _ = happyReduce_70

action_86 _ = happyReduce_73

action_87 (31) = happyShift action_122
action_87 _ = happyFail (happyExpListPerState 87)

action_88 (31) = happyShift action_121
action_88 _ = happyFail (happyExpListPerState 88)

action_89 _ = happyReduce_72

action_90 (43) = happyShift action_120
action_90 (16) = happyGoto action_119
action_90 _ = happyFail (happyExpListPerState 90)

action_91 (31) = happyShift action_107
action_91 (43) = happyShift action_108
action_91 (48) = happyShift action_109
action_91 (49) = happyShift action_110
action_91 (55) = happyShift action_111
action_91 (56) = happyShift action_112
action_91 (57) = happyShift action_113
action_91 (60) = happyShift action_114
action_91 (76) = happyShift action_115
action_91 (79) = happyShift action_116
action_91 (25) = happyGoto action_118
action_91 (26) = happyGoto action_106
action_91 _ = happyFail (happyExpListPerState 91)

action_92 (31) = happyShift action_117
action_92 _ = happyFail (happyExpListPerState 92)

action_93 (31) = happyShift action_107
action_93 (43) = happyShift action_108
action_93 (48) = happyShift action_109
action_93 (49) = happyShift action_110
action_93 (55) = happyShift action_111
action_93 (56) = happyShift action_112
action_93 (57) = happyShift action_113
action_93 (60) = happyShift action_114
action_93 (76) = happyShift action_115
action_93 (79) = happyShift action_116
action_93 (25) = happyGoto action_105
action_93 (26) = happyGoto action_106
action_93 _ = happyFail (happyExpListPerState 93)

action_94 (31) = happyShift action_79
action_94 (43) = happyShift action_81
action_94 (48) = happyShift action_82
action_94 (49) = happyShift action_83
action_94 (55) = happyShift action_84
action_94 (56) = happyShift action_85
action_94 (57) = happyShift action_86
action_94 (60) = happyShift action_87
action_94 (66) = happyShift action_88
action_94 (76) = happyShift action_89
action_94 (24) = happyGoto action_104
action_94 _ = happyFail (happyExpListPerState 94)

action_95 (31) = happyShift action_103
action_95 _ = happyFail (happyExpListPerState 95)

action_96 _ = happyReduce_50

action_97 _ = happyReduce_43

action_98 (31) = happyShift action_56
action_98 (38) = happyShift action_24
action_98 (39) = happyShift action_25
action_98 (40) = happyShift action_26
action_98 (46) = happyShift action_58
action_98 (48) = happyShift action_59
action_98 (59) = happyShift action_60
action_98 (60) = happyShift action_61
action_98 (72) = happyShift action_62
action_98 (77) = happyShift action_63
action_98 (78) = happyShift action_64
action_98 (10) = happyGoto action_52
action_98 (21) = happyGoto action_97
action_98 (22) = happyGoto action_98
action_98 (23) = happyGoto action_55
action_98 _ = happyReduce_42

action_99 _ = happyReduce_40

action_100 _ = happyReduce_41

action_101 (41) = happyShift action_102
action_101 _ = happyFail (happyExpListPerState 101)

action_102 _ = happyReduce_44

action_103 (43) = happyShift action_120
action_103 (16) = happyGoto action_172
action_103 _ = happyFail (happyExpListPerState 103)

action_104 (50) = happyShift action_131
action_104 (51) = happyShift action_132
action_104 (52) = happyShift action_133
action_104 (53) = happyShift action_134
action_104 (54) = happyShift action_135
action_104 (58) = happyShift action_136
action_104 (61) = happyShift action_137
action_104 (62) = happyShift action_138
action_104 (63) = happyShift action_139
action_104 (64) = happyShift action_140
action_104 (65) = happyShift action_141
action_104 (67) = happyShift action_142
action_104 (68) = happyShift action_143
action_104 (69) = happyShift action_144
action_104 (70) = happyShift action_145
action_104 (71) = happyShift action_146
action_104 (27) = happyGoto action_128
action_104 (29) = happyGoto action_129
action_104 _ = happyReduce_55

action_105 (44) = happyShift action_171
action_105 _ = happyFail (happyExpListPerState 105)

action_106 (50) = happyShift action_168
action_106 (51) = happyShift action_132
action_106 (52) = happyShift action_133
action_106 (53) = happyShift action_134
action_106 (54) = happyShift action_135
action_106 (58) = happyShift action_136
action_106 (80) = happyShift action_169
action_106 (81) = happyShift action_170
action_106 (27) = happyGoto action_166
action_106 (28) = happyGoto action_167
action_106 _ = happyReduce_77

action_107 (73) = happyShift action_163
action_107 (74) = happyShift action_164
action_107 (75) = happyShift action_165
action_107 _ = happyReduce_81

action_108 (31) = happyShift action_107
action_108 (43) = happyShift action_108
action_108 (48) = happyShift action_109
action_108 (49) = happyShift action_110
action_108 (55) = happyShift action_111
action_108 (56) = happyShift action_112
action_108 (57) = happyShift action_113
action_108 (60) = happyShift action_114
action_108 (76) = happyShift action_115
action_108 (79) = happyShift action_116
action_108 (26) = happyGoto action_162
action_108 _ = happyFail (happyExpListPerState 108)

action_109 (75) = happyShift action_161
action_109 _ = happyReduce_79

action_110 _ = happyReduce_82

action_111 _ = happyReduce_84

action_112 _ = happyReduce_83

action_113 _ = happyReduce_86

action_114 (31) = happyShift action_160
action_114 _ = happyFail (happyExpListPerState 114)

action_115 _ = happyReduce_85

action_116 (31) = happyShift action_159
action_116 _ = happyFail (happyExpListPerState 116)

action_117 (43) = happyShift action_120
action_117 (16) = happyGoto action_158
action_117 _ = happyFail (happyExpListPerState 117)

action_118 (44) = happyShift action_157
action_118 _ = happyFail (happyExpListPerState 118)

action_119 _ = happyReduce_56

action_120 (31) = happyShift action_79
action_120 (43) = happyShift action_81
action_120 (44) = happyShift action_156
action_120 (48) = happyShift action_82
action_120 (49) = happyShift action_83
action_120 (55) = happyShift action_84
action_120 (56) = happyShift action_85
action_120 (57) = happyShift action_86
action_120 (60) = happyShift action_87
action_120 (66) = happyShift action_88
action_120 (76) = happyShift action_89
action_120 (17) = happyGoto action_154
action_120 (24) = happyGoto action_155
action_120 _ = happyFail (happyExpListPerState 120)

action_121 _ = happyReduce_62

action_122 (43) = happyShift action_120
action_122 (16) = happyGoto action_153
action_122 _ = happyFail (happyExpListPerState 122)

action_123 (31) = happyShift action_152
action_123 _ = happyFail (happyExpListPerState 123)

action_124 (44) = happyShift action_151
action_124 (50) = happyShift action_131
action_124 (51) = happyShift action_132
action_124 (52) = happyShift action_133
action_124 (53) = happyShift action_134
action_124 (54) = happyShift action_135
action_124 (58) = happyShift action_136
action_124 (61) = happyShift action_137
action_124 (62) = happyShift action_138
action_124 (63) = happyShift action_139
action_124 (64) = happyShift action_140
action_124 (65) = happyShift action_141
action_124 (67) = happyShift action_142
action_124 (68) = happyShift action_143
action_124 (69) = happyShift action_144
action_124 (70) = happyShift action_145
action_124 (71) = happyShift action_146
action_124 (27) = happyGoto action_128
action_124 (29) = happyGoto action_129
action_124 _ = happyFail (happyExpListPerState 124)

action_125 _ = happyReduce_61

action_126 _ = happyReduce_60

action_127 (31) = happyShift action_150
action_127 _ = happyFail (happyExpListPerState 127)

action_128 (31) = happyShift action_79
action_128 (43) = happyShift action_81
action_128 (48) = happyShift action_82
action_128 (49) = happyShift action_83
action_128 (55) = happyShift action_84
action_128 (56) = happyShift action_85
action_128 (57) = happyShift action_86
action_128 (60) = happyShift action_87
action_128 (66) = happyShift action_88
action_128 (76) = happyShift action_89
action_128 (24) = happyGoto action_149
action_128 _ = happyFail (happyExpListPerState 128)

action_129 (31) = happyShift action_79
action_129 (43) = happyShift action_81
action_129 (48) = happyShift action_82
action_129 (49) = happyShift action_83
action_129 (55) = happyShift action_84
action_129 (56) = happyShift action_85
action_129 (57) = happyShift action_86
action_129 (60) = happyShift action_87
action_129 (66) = happyShift action_88
action_129 (76) = happyShift action_89
action_129 (24) = happyGoto action_148
action_129 _ = happyFail (happyExpListPerState 129)

action_130 _ = happyReduce_53

action_131 (50) = happyShift action_147
action_131 _ = happyReduce_104

action_132 _ = happyReduce_97

action_133 _ = happyReduce_98

action_134 _ = happyReduce_99

action_135 _ = happyReduce_100

action_136 _ = happyReduce_101

action_137 _ = happyReduce_105

action_138 _ = happyReduce_106

action_139 _ = happyReduce_107

action_140 _ = happyReduce_108

action_141 _ = happyReduce_109

action_142 _ = happyReduce_111

action_143 _ = happyReduce_110

action_144 _ = happyReduce_112

action_145 _ = happyReduce_113

action_146 _ = happyReduce_114

action_147 _ = happyReduce_96

action_148 (50) = happyShift action_131
action_148 (51) = happyShift action_132
action_148 (52) = happyShift action_133
action_148 (53) = happyShift action_134
action_148 (54) = happyShift action_135
action_148 (58) = happyShift action_136
action_148 (61) = happyShift action_137
action_148 (62) = happyShift action_138
action_148 (63) = happyShift action_139
action_148 (64) = happyShift action_140
action_148 (65) = happyShift action_141
action_148 (67) = happyShift action_142
action_148 (68) = happyShift action_143
action_148 (69) = happyShift action_144
action_148 (70) = happyShift action_145
action_148 (71) = happyShift action_146
action_148 (27) = happyGoto action_128
action_148 (29) = happyGoto action_129
action_148 _ = happyReduce_76

action_149 (50) = happyShift action_131
action_149 (51) = happyShift action_132
action_149 (52) = happyShift action_133
action_149 (53) = happyShift action_134
action_149 (54) = happyShift action_135
action_149 (58) = happyShift action_136
action_149 (61) = happyShift action_137
action_149 (62) = happyShift action_138
action_149 (63) = happyShift action_139
action_149 (64) = happyShift action_140
action_149 (65) = happyShift action_141
action_149 (67) = happyShift action_142
action_149 (68) = happyShift action_143
action_149 (69) = happyShift action_144
action_149 (70) = happyShift action_145
action_149 (71) = happyShift action_146
action_149 (27) = happyGoto action_128
action_149 (29) = happyGoto action_129
action_149 _ = happyReduce_75

action_150 (43) = happyShift action_120
action_150 (16) = happyGoto action_184
action_150 _ = happyReduce_66

action_151 _ = happyReduce_74

action_152 (43) = happyShift action_120
action_152 (16) = happyGoto action_183
action_152 _ = happyReduce_68

action_153 _ = happyReduce_64

action_154 (44) = happyShift action_181
action_154 (45) = happyShift action_182
action_154 _ = happyFail (happyExpListPerState 154)

action_155 (50) = happyShift action_131
action_155 (51) = happyShift action_132
action_155 (52) = happyShift action_133
action_155 (53) = happyShift action_134
action_155 (54) = happyShift action_135
action_155 (58) = happyShift action_136
action_155 (61) = happyShift action_137
action_155 (62) = happyShift action_138
action_155 (63) = happyShift action_139
action_155 (64) = happyShift action_140
action_155 (65) = happyShift action_141
action_155 (67) = happyShift action_142
action_155 (68) = happyShift action_143
action_155 (69) = happyShift action_144
action_155 (70) = happyShift action_145
action_155 (71) = happyShift action_146
action_155 (27) = happyGoto action_128
action_155 (29) = happyGoto action_129
action_155 _ = happyReduce_34

action_156 _ = happyReduce_32

action_157 (32) = happyShift action_49
action_157 (20) = happyGoto action_180
action_157 _ = happyFail (happyExpListPerState 157)

action_158 _ = happyReduce_58

action_159 _ = happyReduce_94

action_160 (43) = happyShift action_120
action_160 (16) = happyGoto action_179
action_160 _ = happyFail (happyExpListPerState 160)

action_161 (31) = happyShift action_178
action_161 _ = happyFail (happyExpListPerState 161)

action_162 (44) = happyShift action_177
action_162 (50) = happyShift action_168
action_162 (51) = happyShift action_132
action_162 (52) = happyShift action_133
action_162 (53) = happyShift action_134
action_162 (54) = happyShift action_135
action_162 (58) = happyShift action_136
action_162 (27) = happyGoto action_166
action_162 _ = happyFail (happyExpListPerState 162)

action_163 _ = happyReduce_93

action_164 _ = happyReduce_92

action_165 (31) = happyShift action_176
action_165 _ = happyFail (happyExpListPerState 165)

action_166 (31) = happyShift action_107
action_166 (43) = happyShift action_108
action_166 (48) = happyShift action_109
action_166 (49) = happyShift action_110
action_166 (55) = happyShift action_111
action_166 (56) = happyShift action_112
action_166 (57) = happyShift action_113
action_166 (60) = happyShift action_114
action_166 (76) = happyShift action_115
action_166 (79) = happyShift action_116
action_166 (26) = happyGoto action_175
action_166 _ = happyFail (happyExpListPerState 166)

action_167 (31) = happyShift action_107
action_167 (43) = happyShift action_108
action_167 (48) = happyShift action_109
action_167 (49) = happyShift action_110
action_167 (55) = happyShift action_111
action_167 (56) = happyShift action_112
action_167 (57) = happyShift action_113
action_167 (60) = happyShift action_114
action_167 (76) = happyShift action_115
action_167 (79) = happyShift action_116
action_167 (26) = happyGoto action_174
action_167 _ = happyFail (happyExpListPerState 167)

action_168 (50) = happyShift action_147
action_168 _ = happyFail (happyExpListPerState 168)

action_169 _ = happyReduce_102

action_170 _ = happyReduce_103

action_171 (32) = happyShift action_49
action_171 (20) = happyGoto action_173
action_171 _ = happyFail (happyExpListPerState 171)

action_172 _ = happyReduce_57

action_173 (47) = happyShift action_190
action_173 (18) = happyGoto action_188
action_173 (19) = happyGoto action_189
action_173 _ = happyReduce_45

action_174 (50) = happyShift action_168
action_174 (51) = happyShift action_132
action_174 (52) = happyShift action_133
action_174 (53) = happyShift action_134
action_174 (54) = happyShift action_135
action_174 (58) = happyShift action_136
action_174 (27) = happyGoto action_166
action_174 _ = happyReduce_78

action_175 (50) = happyShift action_168
action_175 (51) = happyShift action_132
action_175 (52) = happyShift action_133
action_175 (53) = happyShift action_134
action_175 (54) = happyShift action_135
action_175 (58) = happyShift action_136
action_175 (27) = happyGoto action_166
action_175 _ = happyReduce_95

action_176 (43) = happyShift action_120
action_176 (16) = happyGoto action_187
action_176 _ = happyReduce_89

action_177 _ = happyReduce_80

action_178 (43) = happyShift action_120
action_178 (16) = happyGoto action_186
action_178 _ = happyReduce_91

action_179 _ = happyReduce_87

action_180 _ = happyReduce_49

action_181 _ = happyReduce_33

action_182 (31) = happyShift action_79
action_182 (43) = happyShift action_81
action_182 (48) = happyShift action_82
action_182 (49) = happyShift action_83
action_182 (55) = happyShift action_84
action_182 (56) = happyShift action_85
action_182 (57) = happyShift action_86
action_182 (60) = happyShift action_87
action_182 (66) = happyShift action_88
action_182 (76) = happyShift action_89
action_182 (24) = happyGoto action_185
action_182 _ = happyFail (happyExpListPerState 182)

action_183 _ = happyReduce_67

action_184 _ = happyReduce_65

action_185 (50) = happyShift action_131
action_185 (51) = happyShift action_132
action_185 (52) = happyShift action_133
action_185 (53) = happyShift action_134
action_185 (54) = happyShift action_135
action_185 (58) = happyShift action_136
action_185 (61) = happyShift action_137
action_185 (62) = happyShift action_138
action_185 (63) = happyShift action_139
action_185 (64) = happyShift action_140
action_185 (65) = happyShift action_141
action_185 (67) = happyShift action_142
action_185 (68) = happyShift action_143
action_185 (69) = happyShift action_144
action_185 (70) = happyShift action_145
action_185 (71) = happyShift action_146
action_185 (27) = happyGoto action_128
action_185 (29) = happyGoto action_129
action_185 _ = happyReduce_35

action_186 _ = happyReduce_90

action_187 _ = happyReduce_88

action_188 (47) = happyShift action_194
action_188 (19) = happyGoto action_193
action_188 _ = happyReduce_47

action_189 _ = happyReduce_36

action_190 (32) = happyShift action_49
action_190 (46) = happyShift action_192
action_190 (20) = happyGoto action_191
action_190 _ = happyFail (happyExpListPerState 190)

action_191 _ = happyReduce_46

action_192 (43) = happyShift action_196
action_192 _ = happyFail (happyExpListPerState 192)

action_193 _ = happyReduce_37

action_194 (32) = happyShift action_49
action_194 (46) = happyShift action_192
action_194 (20) = happyGoto action_195
action_194 _ = happyFail (happyExpListPerState 194)

action_195 _ = happyReduce_48

action_196 (31) = happyShift action_107
action_196 (43) = happyShift action_108
action_196 (48) = happyShift action_109
action_196 (49) = happyShift action_110
action_196 (55) = happyShift action_111
action_196 (56) = happyShift action_112
action_196 (57) = happyShift action_113
action_196 (60) = happyShift action_114
action_196 (76) = happyShift action_115
action_196 (79) = happyShift action_116
action_196 (25) = happyGoto action_197
action_196 (26) = happyGoto action_106
action_196 _ = happyFail (happyExpListPerState 196)

action_197 (44) = happyShift action_198
action_197 _ = happyFail (happyExpListPerState 197)

action_198 (32) = happyShift action_49
action_198 (20) = happyGoto action_199
action_198 _ = happyFail (happyExpListPerState 198)

action_199 _ = happyReduce_38

happyReduce_1 = happySpecReduce_1  4 happyReduction_1
happyReduction_1 (HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn4
		 ([happy_var_1]
	)
happyReduction_1 _  = notHappyAtAll 

happyReduce_2 = happySpecReduce_2  4 happyReduction_2
happyReduction_2 (HappyAbsSyn5  happy_var_2)
	(HappyAbsSyn4  happy_var_1)
	 =  HappyAbsSyn4
		 ((happy_var_2:happy_var_1)
	)
happyReduction_2 _ _  = notHappyAtAll 

happyReduce_3 = happySpecReduce_3  5 happyReduction_3
happyReduction_3 (HappyAbsSyn6  happy_var_3)
	(HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn5
		 (Class(Nothing, happy_var_2, happy_var_3)
	)
happyReduction_3 _ _ _  = notHappyAtAll 

happyReduce_4 = happyReduce 4 5 happyReduction_4
happyReduction_4 ((HappyAbsSyn6  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn5
		 (Class(Just happy_var_1, happy_var_3, happy_var_4)
	) `HappyStk` happyRest

happyReduce_5 = happySpecReduce_2  6 happyReduction_5
happyReduction_5 _
	_
	 =  HappyAbsSyn6
		 (ClassBody([], [])
	)

happyReduce_6 = happySpecReduce_3  6 happyReduction_6
happyReduction_6 _
	(HappyAbsSyn7  happy_var_2)
	_
	 =  HappyAbsSyn6
		 (happy_var_2
	)
happyReduction_6 _ _ _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_1  7 happyReduction_7
happyReduction_7 (HappyAbsSyn8  happy_var_1)
	 =  HappyAbsSyn7
		 (happy_var_1
	)
happyReduction_7 _  = notHappyAtAll 

happyReduce_8 = happySpecReduce_2  7 happyReduction_8
happyReduction_8 (HappyAbsSyn8  happy_var_2)
	(HappyAbsSyn7  happy_var_1)
	 =  HappyAbsSyn7
		 (ClassBody(getFields(happy_var_1)++getFields(happy_var_2),getMeths(happy_var_1)++getMeths(happy_var_2))
	)
happyReduction_8 _ _  = notHappyAtAll 

happyReduce_9 = happySpecReduce_1  8 happyReduction_9
happyReduction_9 (HappyAbsSyn9  happy_var_1)
	 =  HappyAbsSyn8
		 (ClassBody([happy_var_1], [])
	)
happyReduction_9 _  = notHappyAtAll 

happyReduce_10 = happySpecReduce_1  8 happyReduction_10
happyReduction_10 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn8
		 (ClassBody([], [happy_var_1])
	)
happyReduction_10 _  = notHappyAtAll 

happyReduce_11 = happySpecReduce_3  9 happyReduction_11
happyReduction_11 _
	(HappyTerminal (IDENTIFIER happy_var_2))
	(HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn9
		 (FieldDecl(Nothing, happy_var_1, happy_var_2)
	)
happyReduction_11 _ _ _  = notHappyAtAll 

happyReduce_12 = happyReduce 4 9 happyReduction_12
happyReduction_12 (_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	(HappyAbsSyn10  happy_var_2) `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn9
		 (FieldDecl(Just happy_var_1, happy_var_2, happy_var_3)
	) `HappyStk` happyRest

happyReduce_13 = happySpecReduce_1  10 happyReduction_13
happyReduction_13 _
	 =  HappyAbsSyn10
		 ("int"
	)

happyReduce_14 = happySpecReduce_1  10 happyReduction_14
happyReduction_14 _
	 =  HappyAbsSyn10
		 ("boolean"
	)

happyReduce_15 = happySpecReduce_1  10 happyReduction_15
happyReduction_15 _
	 =  HappyAbsSyn10
		 ("char"
	)

happyReduce_16 = happySpecReduce_1  10 happyReduction_16
happyReduction_16 (HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn10
		 (happy_var_1
	)
happyReduction_16 _  = notHappyAtAll 

happyReduce_17 = happySpecReduce_1  11 happyReduction_17
happyReduction_17 _
	 =  HappyAbsSyn11
		 (Public
	)

happyReduce_18 = happySpecReduce_1  11 happyReduction_18
happyReduction_18 _
	 =  HappyAbsSyn11
		 (Protected
	)

happyReduce_19 = happySpecReduce_1  11 happyReduction_19
happyReduction_19 _
	 =  HappyAbsSyn11
		 (Private
	)

happyReduce_20 = happySpecReduce_1  11 happyReduction_20
happyReduction_20 _
	 =  HappyAbsSyn11
		 (Static
	)

happyReduce_21 = happySpecReduce_3  12 happyReduction_21
happyReduction_21 (HappyAbsSyn20  happy_var_3)
	(HappyAbsSyn13  happy_var_2)
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn12
		 (ConstructorDecl(Nothing, happy_var_1, happy_var_2, happy_var_3)
	)
happyReduction_21 _ _ _  = notHappyAtAll 

happyReduce_22 = happyReduce 4 12 happyReduction_22
happyReduction_22 ((HappyAbsSyn20  happy_var_4) `HappyStk`
	(HappyAbsSyn13  happy_var_3) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_2)) `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn12
		 (ConstructorDecl(Just happy_var_1, happy_var_2, happy_var_3, happy_var_4)
	) `HappyStk` happyRest

happyReduce_23 = happyReduce 4 12 happyReduction_23
happyReduction_23 ((HappyAbsSyn20  happy_var_4) `HappyStk`
	(HappyAbsSyn13  happy_var_3) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_2)) `HappyStk`
	(HappyAbsSyn10  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn12
		 (MethodDecl(Nothing, happy_var_1, happy_var_2, happy_var_3, happy_var_4)
	) `HappyStk` happyRest

happyReduce_24 = happyReduce 5 12 happyReduction_24
happyReduction_24 ((HappyAbsSyn20  happy_var_5) `HappyStk`
	(HappyAbsSyn13  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	(HappyAbsSyn10  happy_var_2) `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn12
		 (MethodDecl(Just happy_var_1, happy_var_2, happy_var_3, happy_var_4, happy_var_5)
	) `HappyStk` happyRest

happyReduce_25 = happyReduce 4 12 happyReduction_25
happyReduction_25 ((HappyAbsSyn20  happy_var_4) `HappyStk`
	(HappyAbsSyn13  happy_var_3) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn12
		 (MethodDecl(Nothing, "void", happy_var_2, happy_var_3, happy_var_4)
	) `HappyStk` happyRest

happyReduce_26 = happyReduce 5 12 happyReduction_26
happyReduction_26 ((HappyAbsSyn20  happy_var_5) `HappyStk`
	(HappyAbsSyn13  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn12
		 (MethodDecl(Just happy_var_1, "void", happy_var_3, happy_var_4, happy_var_5)
	) `HappyStk` happyRest

happyReduce_27 = happySpecReduce_2  13 happyReduction_27
happyReduction_27 _
	_
	 =  HappyAbsSyn13
		 ([]
	)

happyReduce_28 = happySpecReduce_3  13 happyReduction_28
happyReduction_28 _
	(HappyAbsSyn14  happy_var_2)
	_
	 =  HappyAbsSyn13
		 (happy_var_2
	)
happyReduction_28 _ _ _  = notHappyAtAll 

happyReduce_29 = happySpecReduce_1  14 happyReduction_29
happyReduction_29 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn14
		 ([happy_var_1]
	)
happyReduction_29 _  = notHappyAtAll 

happyReduce_30 = happySpecReduce_3  14 happyReduction_30
happyReduction_30 (HappyAbsSyn14  happy_var_3)
	_
	(HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn14
		 ((happy_var_1:happy_var_3)
	)
happyReduction_30 _ _ _  = notHappyAtAll 

happyReduce_31 = happySpecReduce_2  15 happyReduction_31
happyReduction_31 (HappyTerminal (IDENTIFIER happy_var_2))
	(HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn15
		 ((happy_var_1,happy_var_2)
	)
happyReduction_31 _ _  = notHappyAtAll 

happyReduce_32 = happySpecReduce_2  16 happyReduction_32
happyReduction_32 _
	_
	 =  HappyAbsSyn16
		 ([]
	)

happyReduce_33 = happySpecReduce_3  16 happyReduction_33
happyReduction_33 _
	(HappyAbsSyn17  happy_var_2)
	_
	 =  HappyAbsSyn16
		 (happy_var_2
	)
happyReduction_33 _ _ _  = notHappyAtAll 

happyReduce_34 = happySpecReduce_1  17 happyReduction_34
happyReduction_34 (HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn17
		 ([happy_var_1]
	)
happyReduction_34 _  = notHappyAtAll 

happyReduce_35 = happySpecReduce_3  17 happyReduction_35
happyReduction_35 (HappyAbsSyn24  happy_var_3)
	_
	(HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn17
		 ((happy_var_3:happy_var_1)
	)
happyReduction_35 _ _ _  = notHappyAtAll 

happyReduce_36 = happySpecReduce_1  18 happyReduction_36
happyReduction_36 (HappyAbsSyn19  happy_var_1)
	 =  HappyAbsSyn18
		 ([happy_var_1]
	)
happyReduction_36 _  = notHappyAtAll 

happyReduce_37 = happySpecReduce_2  18 happyReduction_37
happyReduction_37 (HappyAbsSyn19  happy_var_2)
	(HappyAbsSyn18  happy_var_1)
	 =  HappyAbsSyn18
		 ((happy_var_2:happy_var_1)
	)
happyReduction_37 _ _  = notHappyAtAll 

happyReduce_38 = happyReduce 6 19 happyReduction_38
happyReduction_38 ((HappyAbsSyn20  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_4) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn19
		 ((happy_var_4,happy_var_6)
	) `HappyStk` happyRest

happyReduce_39 = happySpecReduce_2  20 happyReduction_39
happyReduction_39 _
	_
	 =  HappyAbsSyn20
		 (Empty
	)

happyReduce_40 = happySpecReduce_3  20 happyReduction_40
happyReduction_40 _
	(HappyAbsSyn22  happy_var_2)
	_
	 =  HappyAbsSyn20
		 (happy_var_2
	)
happyReduction_40 _ _ _  = notHappyAtAll 

happyReduce_41 = happySpecReduce_3  20 happyReduction_41
happyReduction_41 _
	(HappyAbsSyn21  happy_var_2)
	_
	 =  HappyAbsSyn20
		 (Block(happy_var_2)
	)
happyReduction_41 _ _ _  = notHappyAtAll 

happyReduce_42 = happySpecReduce_1  21 happyReduction_42
happyReduction_42 (HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn21
		 ([happy_var_1]
	)
happyReduction_42 _  = notHappyAtAll 

happyReduce_43 = happySpecReduce_2  21 happyReduction_43
happyReduction_43 (HappyAbsSyn21  happy_var_2)
	(HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn21
		 ((happy_var_1:happy_var_2)
	)
happyReduction_43 _ _  = notHappyAtAll 

happyReduce_44 = happySpecReduce_3  22 happyReduction_44
happyReduction_44 _
	(HappyTerminal (IDENTIFIER happy_var_2))
	(HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn22
		 (LocalVarDecl(happy_var_1, happy_var_2)
	)
happyReduction_44 _ _ _  = notHappyAtAll 

happyReduce_45 = happyReduce 5 22 happyReduction_45
happyReduction_45 ((HappyAbsSyn20  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn22
		 (If(happy_var_3, happy_var_5, Nothing)
	) `HappyStk` happyRest

happyReduce_46 = happyReduce 7 22 happyReduction_46
happyReduction_46 ((HappyAbsSyn20  happy_var_7) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn20  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn22
		 (If(happy_var_3, happy_var_5, Just happy_var_7)
	) `HappyStk` happyRest

happyReduce_47 = happyReduce 6 22 happyReduction_47
happyReduction_47 ((HappyAbsSyn18  happy_var_6) `HappyStk`
	(HappyAbsSyn20  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn22
		 (ElseIf(happy_var_3, happy_var_5 , reverseList(happy_var_6), Nothing)
	) `HappyStk` happyRest

happyReduce_48 = happyReduce 8 22 happyReduction_48
happyReduction_48 ((HappyAbsSyn20  happy_var_8) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn18  happy_var_6) `HappyStk`
	(HappyAbsSyn20  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn22
		 (ElseIf(happy_var_3, happy_var_5 , reverseList(happy_var_6), Just happy_var_8)
	) `HappyStk` happyRest

happyReduce_49 = happyReduce 5 22 happyReduction_49
happyReduction_49 ((HappyAbsSyn20  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn22
		 (While(happy_var_3,happy_var_5)
	) `HappyStk` happyRest

happyReduce_50 = happySpecReduce_2  22 happyReduction_50
happyReduction_50 _
	(HappyAbsSyn23  happy_var_1)
	 =  HappyAbsSyn22
		 (StmtExprStmt(happy_var_1)
	)
happyReduction_50 _ _  = notHappyAtAll 

happyReduce_51 = happySpecReduce_2  22 happyReduction_51
happyReduction_51 _
	_
	 =  HappyAbsSyn22
		 (Break
	)

happyReduce_52 = happySpecReduce_2  22 happyReduction_52
happyReduction_52 _
	_
	 =  HappyAbsSyn22
		 (Continue
	)

happyReduce_53 = happySpecReduce_3  22 happyReduction_53
happyReduction_53 _
	(HappyAbsSyn24  happy_var_2)
	_
	 =  HappyAbsSyn22
		 (Return(happy_var_2)
	)
happyReduction_53 _ _ _  = notHappyAtAll 

happyReduce_54 = happySpecReduce_2  22 happyReduction_54
happyReduction_54 _
	_
	 =  HappyAbsSyn22
		 (ReturnVoid
	)

happyReduce_55 = happySpecReduce_3  23 happyReduction_55
happyReduction_55 (HappyAbsSyn24  happy_var_3)
	_
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn23
		 (Assign(happy_var_1,happy_var_3)
	)
happyReduction_55 _ _ _  = notHappyAtAll 

happyReduce_56 = happySpecReduce_3  23 happyReduction_56
happyReduction_56 (HappyAbsSyn16  happy_var_3)
	(HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn23
		 (New(happy_var_2,reverseList(happy_var_3))
	)
happyReduction_56 _ _ _  = notHappyAtAll 

happyReduce_57 = happyReduce 4 23 happyReduction_57
happyReduction_57 ((HappyAbsSyn16  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn23
		 (MethodCall(LocalOrFieldVar(happy_var_1),happy_var_3,reverseList(happy_var_4))
	) `HappyStk` happyRest

happyReduce_58 = happyReduce 4 23 happyReduction_58
happyReduction_58 ((HappyAbsSyn16  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn23
		 (MethodCall(This,happy_var_3,reverseList(happy_var_4))
	) `HappyStk` happyRest

happyReduce_59 = happySpecReduce_1  24 happyReduction_59
happyReduction_59 _
	 =  HappyAbsSyn24
		 (This
	)

happyReduce_60 = happySpecReduce_2  24 happyReduction_60
happyReduction_60 _
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn24
		 (Unary("++",LocalOrFieldVar(happy_var_1))
	)
happyReduction_60 _ _  = notHappyAtAll 

happyReduce_61 = happySpecReduce_2  24 happyReduction_61
happyReduction_61 _
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn24
		 (Unary("--",LocalOrFieldVar(happy_var_1))
	)
happyReduction_61 _ _  = notHappyAtAll 

happyReduce_62 = happySpecReduce_2  24 happyReduction_62
happyReduction_62 (HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn24
		 (Unary("~",LocalOrFieldVar(happy_var_2))
	)
happyReduction_62 _ _  = notHappyAtAll 

happyReduce_63 = happySpecReduce_1  24 happyReduction_63
happyReduction_63 (HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn24
		 (LocalOrFieldVar(happy_var_1)
	)
happyReduction_63 _  = notHappyAtAll 

happyReduce_64 = happySpecReduce_3  24 happyReduction_64
happyReduction_64 (HappyAbsSyn16  happy_var_3)
	(HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn24
		 (StmtExprExpr(New(happy_var_2,reverseList(happy_var_3)))
	)
happyReduction_64 _ _ _  = notHappyAtAll 

happyReduce_65 = happyReduce 4 24 happyReduction_65
happyReduction_65 ((HappyAbsSyn16  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn24
		 (StmtExprExpr(MethodCall(LocalOrFieldVar(happy_var_1),happy_var_3,reverseList(happy_var_4)))
	) `HappyStk` happyRest

happyReduce_66 = happySpecReduce_3  24 happyReduction_66
happyReduction_66 (HappyTerminal (IDENTIFIER happy_var_3))
	_
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn24
		 (InstVar(LocalOrFieldVar(happy_var_1), happy_var_3)
	)
happyReduction_66 _ _ _  = notHappyAtAll 

happyReduce_67 = happyReduce 4 24 happyReduction_67
happyReduction_67 ((HappyAbsSyn16  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn24
		 (StmtExprExpr(MethodCall(This,happy_var_3,reverseList(happy_var_4)))
	) `HappyStk` happyRest

happyReduce_68 = happySpecReduce_3  24 happyReduction_68
happyReduction_68 (HappyTerminal (IDENTIFIER happy_var_3))
	_
	_
	 =  HappyAbsSyn24
		 (InstVar(This, happy_var_3)
	)
happyReduction_68 _ _ _  = notHappyAtAll 

happyReduce_69 = happySpecReduce_1  24 happyReduction_69
happyReduction_69 (HappyTerminal (BOOLLITERAL happy_var_1))
	 =  HappyAbsSyn24
		 (Bool(happy_var_1)
	)
happyReduction_69 _  = notHappyAtAll 

happyReduce_70 = happySpecReduce_1  24 happyReduction_70
happyReduction_70 (HappyTerminal (CHARLITERAL happy_var_1))
	 =  HappyAbsSyn24
		 (Char(happy_var_1)
	)
happyReduction_70 _  = notHappyAtAll 

happyReduce_71 = happySpecReduce_1  24 happyReduction_71
happyReduction_71 (HappyTerminal (INTLITERAL happy_var_1))
	 =  HappyAbsSyn24
		 (Integer(happy_var_1)
	)
happyReduction_71 _  = notHappyAtAll 

happyReduce_72 = happySpecReduce_1  24 happyReduction_72
happyReduction_72 _
	 =  HappyAbsSyn24
		 (Jnull
	)

happyReduce_73 = happySpecReduce_1  24 happyReduction_73
happyReduction_73 (HappyTerminal (STRINGLITERAL happy_var_1))
	 =  HappyAbsSyn24
		 (String(happy_var_1)
	)
happyReduction_73 _  = notHappyAtAll 

happyReduce_74 = happySpecReduce_3  24 happyReduction_74
happyReduction_74 _
	(HappyAbsSyn24  happy_var_2)
	_
	 =  HappyAbsSyn24
		 (happy_var_2
	)
happyReduction_74 _ _ _  = notHappyAtAll 

happyReduce_75 = happySpecReduce_3  24 happyReduction_75
happyReduction_75 (HappyAbsSyn24  happy_var_3)
	(HappyAbsSyn27  happy_var_2)
	(HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn24
		 (Binary(happy_var_2,happy_var_1,happy_var_3)
	)
happyReduction_75 _ _ _  = notHappyAtAll 

happyReduce_76 = happySpecReduce_3  24 happyReduction_76
happyReduction_76 (HappyAbsSyn24  happy_var_3)
	(HappyAbsSyn29  happy_var_2)
	(HappyAbsSyn24  happy_var_1)
	 =  HappyAbsSyn24
		 (Binary(happy_var_2,happy_var_1,happy_var_3)
	)
happyReduction_76 _ _ _  = notHappyAtAll 

happyReduce_77 = happySpecReduce_1  25 happyReduction_77
happyReduction_77 (HappyAbsSyn26  happy_var_1)
	 =  HappyAbsSyn25
		 (happy_var_1
	)
happyReduction_77 _  = notHappyAtAll 

happyReduce_78 = happySpecReduce_3  25 happyReduction_78
happyReduction_78 (HappyAbsSyn26  happy_var_3)
	(HappyAbsSyn28  happy_var_2)
	(HappyAbsSyn26  happy_var_1)
	 =  HappyAbsSyn25
		 (Binary(happy_var_2,happy_var_1,happy_var_3)
	)
happyReduction_78 _ _ _  = notHappyAtAll 

happyReduce_79 = happySpecReduce_1  26 happyReduction_79
happyReduction_79 _
	 =  HappyAbsSyn26
		 (This
	)

happyReduce_80 = happySpecReduce_3  26 happyReduction_80
happyReduction_80 _
	(HappyAbsSyn26  happy_var_2)
	_
	 =  HappyAbsSyn26
		 (happy_var_2
	)
happyReduction_80 _ _ _  = notHappyAtAll 

happyReduce_81 = happySpecReduce_1  26 happyReduction_81
happyReduction_81 (HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn26
		 (LocalOrFieldVar(happy_var_1)
	)
happyReduction_81 _  = notHappyAtAll 

happyReduce_82 = happySpecReduce_1  26 happyReduction_82
happyReduction_82 (HappyTerminal (BOOLLITERAL happy_var_1))
	 =  HappyAbsSyn26
		 (Bool(happy_var_1)
	)
happyReduction_82 _  = notHappyAtAll 

happyReduce_83 = happySpecReduce_1  26 happyReduction_83
happyReduction_83 (HappyTerminal (CHARLITERAL happy_var_1))
	 =  HappyAbsSyn26
		 (Char(happy_var_1)
	)
happyReduction_83 _  = notHappyAtAll 

happyReduce_84 = happySpecReduce_1  26 happyReduction_84
happyReduction_84 (HappyTerminal (INTLITERAL happy_var_1))
	 =  HappyAbsSyn26
		 (Integer(happy_var_1)
	)
happyReduction_84 _  = notHappyAtAll 

happyReduce_85 = happySpecReduce_1  26 happyReduction_85
happyReduction_85 _
	 =  HappyAbsSyn26
		 (Jnull
	)

happyReduce_86 = happySpecReduce_1  26 happyReduction_86
happyReduction_86 (HappyTerminal (STRINGLITERAL happy_var_1))
	 =  HappyAbsSyn26
		 (String(happy_var_1)
	)
happyReduction_86 _  = notHappyAtAll 

happyReduce_87 = happySpecReduce_3  26 happyReduction_87
happyReduction_87 (HappyAbsSyn16  happy_var_3)
	(HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn26
		 (StmtExprExpr(New(happy_var_2,reverseList(happy_var_3)))
	)
happyReduction_87 _ _ _  = notHappyAtAll 

happyReduce_88 = happyReduce 4 26 happyReduction_88
happyReduction_88 ((HappyAbsSyn16  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn26
		 (StmtExprExpr(MethodCall(LocalOrFieldVar(happy_var_1),happy_var_3,reverseList(happy_var_4)))
	) `HappyStk` happyRest

happyReduce_89 = happySpecReduce_3  26 happyReduction_89
happyReduction_89 (HappyTerminal (IDENTIFIER happy_var_3))
	_
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn26
		 (InstVar(LocalOrFieldVar(happy_var_1), happy_var_3)
	)
happyReduction_89 _ _ _  = notHappyAtAll 

happyReduce_90 = happyReduce 4 26 happyReduction_90
happyReduction_90 ((HappyAbsSyn16  happy_var_4) `HappyStk`
	(HappyTerminal (IDENTIFIER happy_var_3)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn26
		 (StmtExprExpr(MethodCall(This,happy_var_3,reverseList(happy_var_4)))
	) `HappyStk` happyRest

happyReduce_91 = happySpecReduce_3  26 happyReduction_91
happyReduction_91 (HappyTerminal (IDENTIFIER happy_var_3))
	_
	_
	 =  HappyAbsSyn26
		 (InstVar(This, happy_var_3)
	)
happyReduction_91 _ _ _  = notHappyAtAll 

happyReduce_92 = happySpecReduce_2  26 happyReduction_92
happyReduction_92 _
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn26
		 (Unary("+",LocalOrFieldVar(happy_var_1))
	)
happyReduction_92 _ _  = notHappyAtAll 

happyReduce_93 = happySpecReduce_2  26 happyReduction_93
happyReduction_93 _
	(HappyTerminal (IDENTIFIER happy_var_1))
	 =  HappyAbsSyn26
		 (Unary("-",LocalOrFieldVar(happy_var_1))
	)
happyReduction_93 _ _  = notHappyAtAll 

happyReduce_94 = happySpecReduce_2  26 happyReduction_94
happyReduction_94 (HappyTerminal (IDENTIFIER happy_var_2))
	_
	 =  HappyAbsSyn26
		 (Unary("!",LocalOrFieldVar(happy_var_2))
	)
happyReduction_94 _ _  = notHappyAtAll 

happyReduce_95 = happySpecReduce_3  26 happyReduction_95
happyReduction_95 (HappyAbsSyn26  happy_var_3)
	(HappyAbsSyn27  happy_var_2)
	(HappyAbsSyn26  happy_var_1)
	 =  HappyAbsSyn26
		 (Binary(happy_var_2,happy_var_1,happy_var_3)
	)
happyReduction_95 _ _ _  = notHappyAtAll 

happyReduce_96 = happySpecReduce_2  27 happyReduction_96
happyReduction_96 _
	_
	 =  HappyAbsSyn27
		 ("=="
	)

happyReduce_97 = happySpecReduce_1  27 happyReduction_97
happyReduction_97 _
	 =  HappyAbsSyn27
		 (">"
	)

happyReduce_98 = happySpecReduce_1  27 happyReduction_98
happyReduction_98 _
	 =  HappyAbsSyn27
		 ("<"
	)

happyReduce_99 = happySpecReduce_1  27 happyReduction_99
happyReduction_99 _
	 =  HappyAbsSyn27
		 (">="
	)

happyReduce_100 = happySpecReduce_1  27 happyReduction_100
happyReduction_100 _
	 =  HappyAbsSyn27
		 ("<="
	)

happyReduce_101 = happySpecReduce_1  27 happyReduction_101
happyReduction_101 _
	 =  HappyAbsSyn27
		 ("!="
	)

happyReduce_102 = happySpecReduce_1  28 happyReduction_102
happyReduction_102 _
	 =  HappyAbsSyn28
		 ("||"
	)

happyReduce_103 = happySpecReduce_1  28 happyReduction_103
happyReduction_103 _
	 =  HappyAbsSyn28
		 ("&&"
	)

happyReduce_104 = happySpecReduce_1  29 happyReduction_104
happyReduction_104 _
	 =  HappyAbsSyn29
		 ("="
	)

happyReduce_105 = happySpecReduce_1  29 happyReduction_105
happyReduction_105 _
	 =  HappyAbsSyn29
		 ("+"
	)

happyReduce_106 = happySpecReduce_1  29 happyReduction_106
happyReduction_106 _
	 =  HappyAbsSyn29
		 ("-"
	)

happyReduce_107 = happySpecReduce_1  29 happyReduction_107
happyReduction_107 _
	 =  HappyAbsSyn29
		 ("*"
	)

happyReduce_108 = happySpecReduce_1  29 happyReduction_108
happyReduction_108 _
	 =  HappyAbsSyn29
		 ("/"
	)

happyReduce_109 = happySpecReduce_1  29 happyReduction_109
happyReduction_109 _
	 =  HappyAbsSyn29
		 ("%"
	)

happyReduce_110 = happySpecReduce_1  29 happyReduction_110
happyReduction_110 _
	 =  HappyAbsSyn29
		 ("|"
	)

happyReduce_111 = happySpecReduce_1  29 happyReduction_111
happyReduction_111 _
	 =  HappyAbsSyn29
		 ("&"
	)

happyReduce_112 = happySpecReduce_1  29 happyReduction_112
happyReduction_112 _
	 =  HappyAbsSyn29
		 ("^"
	)

happyReduce_113 = happySpecReduce_1  29 happyReduction_113
happyReduction_113 _
	 =  HappyAbsSyn29
		 ("<<"
	)

happyReduce_114 = happySpecReduce_1  29 happyReduction_114
happyReduction_114 _
	 =  HappyAbsSyn29
		 (">>"
	)

happyNewToken action sts stk [] =
	action 82 82 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	CLASS -> cont 30;
	IDENTIFIER happy_dollar_dollar -> cont 31;
	OPENCUR -> cont 32;
	CLOSECUR -> cont 33;
	PUBLIC -> cont 34;
	PROTECTED -> cont 35;
	PRIVATE -> cont 36;
	STATIC -> cont 37;
	BOOLEAN -> cont 38;
	INTEGER -> cont 39;
	CHAR -> cont 40;
	SEMICOLON -> cont 41;
	VOID -> cont 42;
	OPENBR -> cont 43;
	CLOSEBR -> cont 44;
	COMMA -> cont 45;
	IF -> cont 46;
	ELSE -> cont 47;
	THIS -> cont 48;
	BOOLLITERAL happy_dollar_dollar -> cont 49;
	EQUALS -> cont 50;
	GREATERTHAN -> cont 51;
	LESSTHAN -> cont 52;
	GREATERTHANE -> cont 53;
	LESSTHANE -> cont 54;
	INTLITERAL happy_dollar_dollar -> cont 55;
	CHARLITERAL happy_dollar_dollar -> cont 56;
	STRINGLITERAL happy_dollar_dollar -> cont 57;
	NOTEQUAL -> cont 58;
	WHILE -> cont 59;
	NEW -> cont 60;
	PLUS -> cont 61;
	MINUS -> cont 62;
	MULT -> cont 63;
	DIV -> cont 64;
	MODULO -> cont 65;
	COMPLEMENT -> cont 66;
	BAND -> cont 67;
	BOR -> cont 68;
	BXOR -> cont 69;
	LSHIFT -> cont 70;
	RSHIFT -> cont 71;
	RETURN -> cont 72;
	DECREMENT -> cont 73;
	INCREMENT -> cont 74;
	DOT -> cont 75;
	JNULL -> cont 76;
	BREAK -> cont 77;
	CONTINUE -> cont 78;
	EXCLAMATION -> cont 79;
	LOGIOR -> cont 80;
	LOGIAND -> cont 81;
	_ -> happyError' ((tk:tks), [])
	}

happyError_ explist 82 tk tks = happyError' (tks, explist)
happyError_ explist _ tk tks = happyError' ((tk:tks), explist)

newtype HappyIdentity a = HappyIdentity a
happyIdentity = HappyIdentity
happyRunIdentity (HappyIdentity a) = a

instance Prelude.Functor HappyIdentity where
    fmap f (HappyIdentity a) = HappyIdentity (f a)

instance Applicative HappyIdentity where
    pure  = HappyIdentity
    (<*>) = ap
instance Prelude.Monad HappyIdentity where
    return = pure
    (HappyIdentity p) >>= q = q p

happyThen :: () => HappyIdentity a -> (a -> HappyIdentity b) -> HappyIdentity b
happyThen = (Prelude.>>=)
happyReturn :: () => a -> HappyIdentity a
happyReturn = (Prelude.return)
happyThen1 m k tks = (Prelude.>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> HappyIdentity a
happyReturn1 = \a tks -> (Prelude.return) a
happyError' :: () => ([(Token)], [Prelude.String]) -> HappyIdentity a
happyError' = HappyIdentity Prelude.. (\(tokens, _) -> parseError tokens)
program tks = happyRunIdentity happySomeParser where
 happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


getFields :: ClassBody -> [FieldDecl]
getFields (ClassBody(fs,ms)) = fs

getMeths :: ClassBody -> [MethodDecl]
getMeths (ClassBody(fs,ms)) = ms

reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]

parseError :: [Token] -> a
parseError _ = error "Parse error"

parser :: String -> [Class]
parser = program . alexScanTokens



mainP = do
	s <- readFile "eingabe.java"
	print (alexScanTokens s)
	print (parser s)
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- $Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp $










































data Happy_IntList = HappyCons Prelude.Int Happy_IntList








































infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is ERROR_TOK, it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action









































indexShortOffAddr arr off = arr Happy_Data_Array.! off


{-# INLINE happyLt #-}
happyLt x y = (x Prelude.< y)






readArrayBit arr bit =
    Bits.testBit (indexShortOffAddr arr (bit `Prelude.div` 16)) (bit `Prelude.mod` 16)






-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Prelude.Int ->                    -- token number
         Prelude.Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k Prelude.- ((1) :: Prelude.Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail [] (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             _ = nt :: Prelude.Int
             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n Prelude.- ((1) :: Prelude.Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n Prelude.- ((1)::Prelude.Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction









happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery (ERROR_TOK is the error token)

-- parse error if we are in recovery and we fail again
happyFail explist (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ explist i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  ERROR_TOK tk old_st CONS(HAPPYSTATE(action),sts) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        DO_ACTION(action,ERROR_TOK,tk,sts,(saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail explist i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ((HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = Prelude.error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `Prelude.seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.









{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
