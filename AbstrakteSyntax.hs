module AbstrakteSyntax where

data Class = Class(Maybe Access, Type, ClassBody)
            deriving(Eq, Show)

data ClassBody = ClassBody([FieldDecl], [MethodDecl])
            deriving(Eq, Show)

data FieldDecl = FieldDecl(Maybe Access, Type, String)
            deriving(Eq, Show)

data MethodDecl = MethodDecl(Maybe Access, Type, String,[(Type,String)], Stmt)
          | ConstructorDecl(Maybe Access, String, [(Type,String)], Stmt)
            deriving(Eq, Show)

data Stmt = Block([Stmt])
          | ReturnVoid
          | Return( Expr )
          | While( Expr , Stmt )
          | LocalVarDecl(Type, String)
          | If(Expr, Stmt, Maybe Stmt) 
          | ElseIf(Expr, Stmt , [(Expr,Stmt)], Maybe Stmt)              
          | StmtExprStmt(StmtExpr)          --new
          | Empty
          | Break
          | Continue
          | TypedStmt(Stmt, Type)
          -- For(Expr, Expr, Expr, Stmt)
          -- Switch(Expr, [(Expr:Stmt)])
          deriving(Eq, Show)

data StmtExpr = Assign(String, Expr)            --i = 5
          | New(Type, [Expr])                   --new Haus()
          | MethodCall(Expr, String, [Expr])    
          | TypedStmtExpr(StmtExpr, Type)       
          deriving(Eq, Show)


data Expr = This
          | LocalOrFieldVar(String)
          | InstVar(Expr, String)
          | Unary(String, Expr) --(i++)
          | Binary(String, Expr, Expr) --(i+j)
          | Integer(Integer)
          | Bool(Bool)
          | Char(Char)
          | String(String)
          | Jnull
          | StmtExprExpr(StmtExpr)              --i = 5+5
          | TypedExpr(Expr, Type)
          deriving(Eq, Show)

type Prg = [Class]

data Access = Public
          | Private
          | Protected
          | Static
          deriving (Eq, Show)

type Type = String
