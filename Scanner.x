{
module Scanner (alexScanTokens, Token(..)) where
}

%wrapper "basic"

tokens :-
    $white+     ;
    "//".*      ;
    "/*"($white|.)*"*/" ;
    
    Main {\s -> MAINCLASS}
    class {\s -> CLASS}
    static{\s -> STATIC}
    main {\s -> MAINMETHOD}
    new {\s -> NEW}    
    this {\s -> THIS}
    return {\s -> RETURN}
    
    abstract {\s -> ABSTRACT}
    protected {\s -> PROTECTED}
    private{\s -> PRIVATE}
    public{\s -> PUBLIC}

    "{" {\s -> OPENCUR}
    "}" {\s -> CLOSECUR}
    "(" {\s -> OPENBR}
    ")" {\s -> CLOSEBR}
    ";" {\s -> SEMICOLON}
    ":" {\s -> COLON}
    "," {\s -> COMMA}

    "+" {\s -> PLUS}
    "-" {\s -> MINUS}
    "*" {\s -> MULT}
    "/" {\s -> DIV}
    "%" {\s -> MODULO}
    "~" {\s -> COMPLEMENT}
    "&" {\s -> BAND}
    "|" {\s -> BOR}
    "^" {\s -> BXOR}
    "<<" {\s -> LSHIFT}
    ">>" {\s -> RSHIFT}

    "++"{\s -> INCREMENT}
    "--"{\s -> DECREMENT}

    "&&"{\s -> LOGIAND}
    "||"{\s -> LOGIOR}

    "=" {\s -> EQUALS}
    ">" {\s -> GREATERTHAN}
    "<" {\s -> LESSTHAN}
    "!" {\s -> EXCLAMATION}
    ">=" {\s -> GREATERTHANE}
    "<=" {\s -> LESSTHANE}
    "!=" {\s -> NOTEQUAL}
    
    break {\s -> BREAK}
    continue {\s -> CONTINUE}
    switch {\s -> SWITCH}
    case {\s -> CASE}
    try {\s -> TRY}
    catch {\s-> CATCH}
    for {\s -> FOR}
    while {\s -> WHILE}
    default {\s -> DEFAULT}
    if {\s -> IF}
    else {\s -> ELSE}
    "." {\s -> DOT}

    boolean {\s -> BOOLEAN}                    
    int {\s -> INTEGER}                    
    char{\s -> CHAR}                    
    void{\s -> VOID}  

                       
    ((\+? | \-?)0 | (\+? | \-?)[1-9][0-9]*[L]?) { \s -> INTLITERAL(read s) } 
    0[xX][0-9a-fA-F]+[L]? { \s -> HEXALITERAL(read s) }             
    (true | false) {\s -> case s of
                            "true" -> BOOLLITERAL(True)
                            "false" -> BOOLLITERAL(False)}                    
    null {\s -> JNULL}             
    '(. | $white )' {\(s:ss) -> CHARLITERAL(head(ss))}
    (\"([a-zA-Z0-9])*\") {\s -> STRINGLITERAL (read s)}
    ([a-zA-Z\9500\241\9500\228\9500\194\9500\251\9500\9565\9500\163]|"_"|"$")([a-zA-Z0-9\9500\241\9500\228\9500\194\9500\251\9500\9565\9500\163]|"$"|"_")* {\s -> IDENTIFIER s}

    --auch Zahlen am Anfang O.o
    --($printable # $white)($printable # $white)* {\s -> IDENTIFIER s}

{
data Token = MAINCLASS
    | CLASS
    | STATIC
    | MAINMETHOD
    | NEW
    | THIS
    | RETURN
    | ABSTRACT
    
    | PROTECTED
    | PRIVATE
    | PUBLIC
    
    | PLUS
    | MINUS
    | MULT
    | DIV
    | MODULO
    | COMPLEMENT
    | BAND
    | BOR
    | BXOR
    | LSHIFT
    | RSHIFT


    | INCREMENT
    | DECREMENT

    | EQUALS
    | GREATERTHAN
    | LESSTHAN
    | GREATERTHANE
    | EXCLAMATION
    | LESSTHANE
    | NOTEQUAL

    | LOGIAND
    | LOGIOR

    | OPENCUR
    | CLOSECUR
    | OPENBR
    | CLOSEBR
    | SEMICOLON
    | COLON
    | COMMA



    | BREAK
    | CONTINUE
    | SWITCH
    | CASE
    | TRY
    | CATCH
    | FOR
    | WHILE
    | DEFAULT
    | IF
    | ELSE
    | DOT

    | BOOLEAN
    | INTEGER
    | CHAR
    | VOID



    | INTLITERAL Integer
    | HEXALITERAL Integer
    | BOOLLITERAL Bool
    | STRINGLITERAL String
    | JNULL
    | CHARLITERAL Char
    | IDENTIFIER String
    deriving(Eq,Show)

main = do
    s <- readFile "eingabe.java"
    -- s <- getContents
    print (alexScanTokens s)
}