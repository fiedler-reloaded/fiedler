module KonstantenPool where
import ClassFormat
import AbstrakteSyntax

classToCP :: Class -> CP_Infos
classToCP(Class(_, classType, ClassBody(fieldDecls, methodDecls))) =
        let
                list0 = []
                list1 = addBasics (classType, methodDecls, list0)
                list2 = addFieldDecls(classType, fieldDecls, list1)
                list3 = addMethodDecls(methodDecls, list2)
        in
                list3

addBasics :: (Type, [MethodDecl], CP_Infos) -> CP_Infos
addBasics(classType, methodDecls, cpInfos) = 
        let
                list0 = addClassToPool (classType, cpInfos)
                list1 = addClassToPool ("java/lang/Object", list0)
                list2 = addStandardConstructor (classType, methodDecls, list1)
                list3 = utf8StringToPool ("()V", list2)
        in
                utf8StringToPool ("Code", list3)

addStandardConstructor :: (Type, [MethodDecl], CP_Infos) -> CP_Infos
addStandardConstructor (classType, methodDecls, cpList)
        | checkConstructorExists (methodDecls) = addSuperclassConstructor (cpList)
        | otherwise = let 
                        newCpInfos = addConstructor (classType, cpList)
                in
                        addSuperclassConstructor (newCpInfos)

addSuperclassConstructor :: (CP_Infos) -> CP_Infos
addSuperclassConstructor (cpInfos) =
        let
                constructorName = "<init>"
                constructorDescriptor = "()V"
                list1 = utf8StringToPool (constructorName, cpInfos)
                list2 = utf8StringToPool (constructorDescriptor, list1)
                list3 = nameAndTypeToPool (constructorName, constructorDescriptor, list2)
        in
                methodRefToPool ("java/lang/Object", constructorName, constructorDescriptor, list3)

addClassToPool :: (String, CP_Infos) -> CP_Infos 
addClassToPool(className, cpInfos) =
        let
                list0 = utf8StringToPool (className, cpInfos)
        in
                if (findClassIndex (cpInfos, className) == -1) then
                        classTypeToPool(className, list0)
                else
                        cpInfos

addFieldDecls :: (String, [FieldDecl], CP_Infos) -> CP_Infos
addFieldDecls (_, [], cpInfos) = cpInfos
addFieldDecls(className, (head :tail), cpInfos)
        | tail == [] = addSingleFieldDecl (className, head, cpInfos)
        | otherwise = addSingleFieldDecl(className, head, addFieldDecls(className, tail, cpInfos))

addSingleFieldDecl :: (String, FieldDecl, CP_Infos) -> CP_Infos
addSingleFieldDecl(className, FieldDecl(_, fieldDeclType, fieldDeclsString), cpInfos) =
        let
                typ = typeToCPType (fieldDeclType)
                list0 = utf8StringToPool (fieldDeclsString, cpInfos)
                list1 = utf8StringToPool (typ, list0)
                list2 = nameAndTypeToPool (fieldDeclsString, typ, list1)
                list3 = fieldRefToPool (className, fieldDeclsString, typ, list2)
        in
                addFieldDeclType (fieldDeclType, list3)

addFieldDeclType :: (String, CP_Infos) -> CP_Infos
addFieldDeclType(fieldDeclType, cpInfos) = 
        let
                addString = typeToCPType(fieldDeclType)
        in
                utf8StringToPool (addString, cpInfos)

typeToCPType :: (String) -> String
typeToCPType(inputString)
        | inputString == "String" = "Ljava/lang/String;"
        | inputString == "boolean" = "Z"
        | inputString == "int" = "I"
        | inputString == "char" = "C"
        | inputString == "void" = "V"
        | otherwise = "L" ++ inputString ++ ";"

addMethodDecls :: ([MethodDecl], CP_Infos) -> CP_Infos
addMethodDecls([], cpInfos) = cpInfos
addMethodDecls((head :tail), cpInfos)
        | tail == [] = addSingleMethodDecl (head, cpInfos)
        | otherwise = addSingleMethodDecl(head, addMethodDecls(tail, cpInfos))

addSingleMethodDecl :: (MethodDecl, CP_Infos) -> CP_Infos
addSingleMethodDecl(ConstructorDecl(_, className, params, statement), cpInfos) =
        let
                methodType = "void"
                constructorName = "<init>"
                constructorDescriptor = "()V"
                list1 = utf8StringToPool (constructorName, cpInfos)
                list2 = addMethodType(methodType, params, list1)
                list3 = addStmtToPool(statement, list2)
                list4 = nameAndTypeToPool (constructorName, constructorDescriptor, list3)
        in
                methodRefToPool (className, constructorName, constructorDescriptor, list4)
addSingleMethodDecl(MethodDecl(_, methodType, methodName, params, statement), cpInfos) =
        let
                list0 = addMethodType(methodType, params, cpInfos)
                list1 = utf8StringToPool (methodName, list0)
        in
                addStmtToPool(statement, list1)

addMethodType :: (String, [(Type, String)], CP_Infos) -> CP_Infos
addMethodType(methodType, params, cpInfos) =
        let
                cpString = getCPParamString (methodType, params)
        in
                utf8StringToPool(cpString, cpInfos)

getCPParamString :: (String, [(Type, String)]) -> String
getCPParamString (methodType, params) =
        let
                paramTypes = getParamTypes(params)
                cpMethodType = typeToCPType(methodType)
        in
                "(" ++ paramTypes ++ ")" ++ cpMethodType

getParamTypes :: [(Type, String)] -> String
getParamTypes([]) = ""
getParamTypes((inputType, _) :tail) =
        let
                addString = typeToCPType(inputType)
        in
                addString ++ getParamTypes (tail)

getParamTypesFromTypedExpr :: ([Expr]) -> String
getParamTypesFromTypedExpr([]) = ""
getParamTypesFromTypedExpr((TypedExpr(_, inputType)) : tail) =
        let
                addString = typeToCPType(inputType)
        in
                addString ++ getParamTypesFromTypedExpr (tail)


getCPParamStringFromTypedExpr :: (String, [Expr]) -> String
getCPParamStringFromTypedExpr (methodType, expressions) =
        let
                paramTypes = getParamTypesFromTypedExpr(expressions)
                cpMethodType = typeToCPType(methodType)
        in
                "(" ++ paramTypes ++ ")" ++ cpMethodType
        

addSingleParam :: ((Type, String), CP_Infos) -> CP_Infos
addSingleParam((paramType, paramString), cpInfos) = 
        let 
                list0 = addFieldDeclType(paramType, cpInfos)
        in
                utf8StringToPool(paramString, list0)

addStmtToPool :: (Stmt, CP_Infos) -> CP_Infos
addStmtToPool(Block([]), cpInfos) = cpInfos
addStmtToPool(Block((head: tail)), cpInfos) =
        let
                list0 = addStmtToPool(head, cpInfos)
        in
                addStmtToPool(Block(tail), list0)
addStmtToPool(Return(expr), cpInfos) = addExprToPool(expr, cpInfos)
addStmtToPool(While(expr, stmt), cpInfos) = 
        let
                list0 = addStmtToPool(stmt, cpInfos)
        in
                addExprToPool(expr, list0)
addStmtToPool(If(expr, stmt, Just stmt1), cpInfos) = 
        let
                list0 = addStmtToPool(stmt, cpInfos)
                list1 = addStmtToPool(stmt1, list0)
        in
                addExprToPool(expr, list1)
addStmtToPool(If(expr, stmt, Nothing), cpInfos) = 
        let
                list0 = addStmtToPool(stmt, cpInfos)
        in
                addExprToPool(expr, list0)
addStmtToPool(ElseIf(expr, stmt, paramList, Just stmt1), cpInfos) =
        let
                newCpInfos = addParamExprToPool(paramList, cpInfos)
                list0 = addStmtToPool(stmt, newCpInfos)
                list1 = addStmtToPool(stmt1, list0)
        in
                addExprToPool(expr, list1)
addStmtToPool(ElseIf(expr, stmt, paramList, Nothing), cpInfos) =
        let
                newCpInfos = addParamExprToPool(paramList, cpInfos)
                list0 = addStmtToPool(stmt, newCpInfos)
        in
                addExprToPool(expr, list0)
addStmtToPool(TypedStmt(x,y), cpInfos) = addStmtToPool(x, cpInfos)
addStmtToPool(StmtExprStmt(x), cpInfos) = addStmtExprToPool(x, cpInfos)
addStmtToPool(_,cpInfos) = cpInfos

addParamExprToPool :: ([(Expr, Stmt)], CP_Infos) -> CP_Infos
addParamExprToPool([], cpInfos) = cpInfos
addParamExprToPool(((expr, stmt) : ls), cpInfos) =
        let
                newCpInfos0 = addParamExprToPool(ls, cpInfos)
                newCpInfos1 = addStmtToPool(stmt, newCpInfos0)
        in
                addExprToPool(expr, newCpInfos1)

addConstructorWithExpressions ::(String, [Expr], CP_Infos) -> CP_Infos
addConstructorWithExpressions(className, expressions, cpInfos) =
        let
                constructorName = "<init>"
                paramCPEntry = getCPParamStringFromTypedExpr("void", expressions)
                list0 = addClassToPool (className, cpInfos)
                list1 = utf8StringToPool (className, list0)
                list2 = utf8StringToPool (constructorName, list1)
                list3 = utf8StringToPool (paramCPEntry, list2)
                list4 = nameAndTypeToPool (constructorName, paramCPEntry, list3)
                list5 = utf8StringToPool (paramCPEntry, list4)
        in
                methodRefToPool (className, constructorName, paramCPEntry, list5)


addStmtExprToPool :: (StmtExpr, CP_Infos) -> CP_Infos
addStmtExprToPool(New(className, expressions), cpInfos) = let
                list0 = addParamsToPool(expressions, cpInfos)
        in
                addConstructorWithExpressions(className, expressions, list0)
addStmtExprToPool(Assign(_, expression), cpInfos) = addExprToPool(expression, cpInfos) 
addStmtExprToPool(TypedStmtExpr(MethodCall(TypedExpr(_, className), methodName, params), methodType), cpInfos) =
        let
                list00 = addParamsToPool(params, cpInfos)
                list0 = utf8StringToPool (methodName, list00)
                cpParamString = getCPParamStringFromTypedExpr (methodType, params)
                list1 = addClassToPool (className, list0)
                list2 = utf8StringToPool (cpParamString, list1)
                list3 = nameAndTypeToPool (methodName, cpParamString, list2)
        in
                methodRefToPool (className, methodName, cpParamString, list3)
addStmtExprToPool(TypedStmtExpr(x,y), cpInfos) = addStmtExprToPool(x, cpInfos)
addStmtExprToPool(_, cpInfos) = cpInfos

addParamsToPool :: ([Expr], CP_Infos) -> CP_Infos
addParamsToPool([], cp) = cp
addParamsToPool((c:cs), cp) = let
                list0 = addExprToPool(c, cp)
        in      
                addParamsToPool(cs, list0)

addExprToPool :: (Expr, CP_Infos) -> CP_Infos
addExprToPool(TypedExpr(LocalOrFieldVar(varName), varType), cpInfos) = 
        addVarStuff ("xx", varType, varName, cpInfos)
addExprToPool(TypedExpr(InstVar(TypedExpr(expr, className), varName), varType), cpInfos) =
        addVarStuff (className, varType, varName, (addExprToPool(expr, cpInfos)))
addExprToPool(String(stringToAdd), cpInfos) = addVarStuff("xx", "String", stringToAdd, cpInfos)
addExprToPool(StmtExprExpr(x), cpInfos) = addStmtExprToPool(x, cpInfos)
addExprToPool(TypedExpr(x,y), cpInfos) = addExprToPool(x, cpInfos)
addExprToPool(Unary(_, expr), cpInfos) = addExprToPool(expr, cpInfos)
addExprToPool(Binary(_, expr1, expr2), cpInfos) = 
        let
                list0 = addExprToPool(expr1, cpInfos)
        in
                addExprToPool(expr2, list0)
addExprToPool(_, cpInfos) = cpInfos

addVarStuff :: (String, String, String, CP_Infos) -> CP_Infos
addVarStuff ("xx", varType, varName, cpInfos) = 
        if varType == "String" then
                addStringInfoToPool (varName, cpInfos)
        else
                cpInfos
addVarStuff (className, varType, varName, cpInfos) = 
        let
                list0 = addClassToPool (className, cpInfos)
        in
                if varType == "String" then
                        addStringInfoToPool (varName, cpInfos)
                else
                        addSingleFieldDecl(className, FieldDecl(Nothing, varType, varName), list0)

addStringInfoToPool :: (String, CP_Infos) -> CP_Infos
addStringInfoToPool (stringContent, cpInfos) =
        let
                list0 = utf8StringToPool (stringContent, cpInfos)
                stringIndex = findUtf8InfoIndex(list0, stringContent)
                info = String_Info TagString stringIndex ""
        in
                addInfoToPool (info, list0)




addConstructor :: (String, CP_Infos) -> CP_Infos
addConstructor(className, cpInfos) = 
        let
                constructorName = "<init>"
                constructorDescriptor = "()V"
                list1 = utf8StringToPool (constructorName, cpInfos)
                list2 = utf8StringToPool (constructorDescriptor, list1)
                list3 = nameAndTypeToPool (constructorName, constructorDescriptor, list2)
        in
                methodRefToPool (className, constructorName, constructorDescriptor, list3)

addInfoToPool :: (CP_Info, CP_Infos) -> CP_Infos
addInfoToPool(a, []) = [a]
addInfoToPool(a, (b : c)) = (b : addInfoToPool(a, c))

utf8TypeToPool :: (Type, CP_Infos) -> CP_Infos
utf8TypeToPool(myTypeInput, cpList)  
        | findUtf8InfoIndex(cpList, myTypeInput) == -1 = addInfoToPool((Utf8_Info TagUtf8 (length myTypeInput) myTypeInput ""), cpList)
        | otherwise = cpList
                

utf8StringToPool :: (String, CP_Infos) -> CP_Infos
utf8StringToPool(stringInput, cpList) 
        | findUtf8InfoIndex(cpList, stringInput) == -1 = addInfoToPool((Utf8_Info TagUtf8 (length stringInput) stringInput ""), cpList)
        | otherwise = cpList

classTypeToPool :: (Type, CP_Infos) -> CP_Infos
classTypeToPool(myTypeInput, cpList) =
        let 
                nameIndex = findUtf8InfoIndex (cpList, myTypeInput)
                info = Class_Info TagClass nameIndex ""
        in
                addInfoToPool(info, cpList)

nameAndTypeToPool :: (String, String, CP_Infos) -> CP_Infos
nameAndTypeToPool(name, descriptor, cpInfos) =
        let
                nameIndex = findUtf8InfoIndex (cpInfos, name)
                descriptorIndex = findUtf8InfoIndex (cpInfos, descriptor)
                info = NameAndType_Info TagNameAndType nameIndex descriptorIndex ""
        in
                if (findNameAndTypeIndex (cpInfos, name, descriptor)  == -1) then
                        addInfoToPool(info, cpInfos)
                else
                        cpInfos

methodRefToPool :: (String, String, String, CP_Infos) -> CP_Infos
methodRefToPool(className, methodName, descriptor, cpInfos) = 
        let
                classIndex = findClassIndex (cpInfos, className)
                nameAndTypeIndex = findNameAndTypeIndex(cpInfos, methodName, descriptor)
                info = MethodRef_Info TagMethodRef classIndex nameAndTypeIndex ""
        in
                addInfoToPool(info, cpInfos)

fieldRefToPool :: (String, String, String, CP_Infos) -> CP_Infos
fieldRefToPool (className, fieldName, descriptor, cpInfos) =
        let
                classIndex = findClassIndex (cpInfos, className)
                nameAndTypeIndex = findNameAndTypeIndex(cpInfos, fieldName, descriptor)
                info = FieldRef_Info TagFieldRef classIndex nameAndTypeIndex ""
        in
                if findFieldRefByNT(cpInfos, fieldName) == -1 then
                        addInfoToPool(info, cpInfos)
                else
                        cpInfos


--Finds Index in ConstantPool

findUtf8InfoIndex :: ([CP_Info], String) -> Int
findUtf8InfoIndex(cp, name) = findUtf8InfoIndexRek(cp, name, 1)

findUtf8InfoIndexRek :: ([CP_Info], String, Int) -> Int
findUtf8InfoIndexRek([], _ , _) = -1
findUtf8InfoIndexRek((c : cs),y,z)
        | equalsName(c, y) = z
        | otherwise = findUtf8InfoIndexRek (cs, y, z+1)

equalsName :: (CP_Info, String) -> Bool
equalsName((Utf8_Info _ _ name _), s) 
        | name == s = True
        | otherwise = False
equalsName((Utf8_Info _ _ name _), s) 
        | name == s = True
        | otherwise = False
equalsName(x,y) = False


findClassIndex :: ([CP_Info], String) ->  Int
findClassIndex(cp, s) =
    let
        id = findUtf8InfoIndex(cp,s)
    in
        findByIndex(cp, id, 1)

findByIndex :: ([CP_Info], Int , Int) -> Int
findByIndex([], _, _) = -1
findByIndex((c : cs),y,z)
        | equalsID(c, y) = z
        | otherwise = findByIndex(cs,y,z+1)

equalsID :: (CP_Info, Int) -> Bool
equalsID((Class_Info _ ind _), id) = if id == ind then True else False
equalsID(x,y) = False

findNameAndTypeIndex :: ([CP_Info], String, String) -> Int
findNameAndTypeIndex([], _, _) = -1
findNameAndTypeIndex(cp, name, typ) =
    let
        nameID = findUtf8InfoIndex(cp, name)
        typID = findUtf8InfoIndex(cp, typ)
    in
        findNTByIndex(cp, nameID, typID, 1)

findNTByIndex :: ([CP_Info], Int , Int, Int) -> Int 
findNTByIndex([], _, _, _) = -1
findNTByIndex((c:cs), name, typ, i)
       | equalsNameAndType(c, name, typ) = i
       | otherwise = findNTByIndex(cs, name, typ, i+1)

equalsNameAndType :: (CP_Info, Int, Int) -> Bool
equalsNameAndType((NameAndType_Info _ name typ _), myName, myType)
    | (name == myName && typ == myType) = True
    | otherwise = False
equalsNameAndType(x,y,z) = False

checkConstructorExists :: ([MethodDecl]) -> Bool
checkConstructorExists ([]) = False
checkConstructorExists (ConstructorDecl(_, _, _, _) : _) = True
checkConstructorExists (MethodDecl(_, _, _, _, _) : tail) = 
        checkConstructorExists (tail)

findFieldRefByNT :: ([CP_Info], String) -> Int
findFieldRefByNT([], _) = -1
findFieldRefByNT((c:cp), name) =
    let
        ntid = findNameAndTypeIndexByNameX((c:cp), name)
    in
        findFRbyId((c:cp), ntid, 1)

findFRbyId :: ([CP_Info], Int, Int) -> Int
findFRbyId([], _, _) = -1
findFRbyId((c:cs), id, i)
    | equalsFR(c, id) = i
    | otherwise  = findFRbyId(cs, id, i+1)


equalsFR :: (CP_Info, Int) -> Bool
equalsFR((FieldRef_Info _ _ n _), id)
    | n == id = True
    | otherwise = False
equalsFR(x, _) = False

findNameAndTypeIndexByNameX :: ([CP_Info], String) -> Int
findNameAndTypeIndexByNameX([], _) = -1
findNameAndTypeIndexByNameX(cp, name) =
    let
        nameID = findUtf8InfoIndex(cp, name)
    in
        findNTByIndexName(cp, nameID, 1)

findNTByIndexName :: ([CP_Info], Int , Int) -> Int 
findNTByIndexName([], _, _) = -1
findNTByIndexName((c:cs), name, i)
       | equalsNameNotType(c, name) = i
       | otherwise = findNTByIndexName(cs, name, i+1)

equalsNameNotType :: (CP_Info, Int) -> Bool
equalsNameNotType((NameAndType_Info _ name _ _), myName)
    | (name == myName) = True
    | otherwise = False
equalsNameNotType(x,y) = False

findStringInCP :: ([CP_Info], String) -> Int
findStringInCP(cp, s) =
    let
        id = findUtf8InfoIndex(cp, s)
    in
        findStringById(cp, id, 1)

findStringById :: ([CP_Info], Int, Int) -> Int
findStringById([], _, _) = -1
findStringById((c:cs), name, id)
    | equalsStringID(c, name) = id
    | otherwise = findStringById(cs, name, id+1)

equalsStringID :: (CP_Info, Int) -> Bool
equalsStringID((String_Info _ s _), id)
    | s == id = True
    | otherwise = False
equalsStringID(x,_) = False

findMethodRef :: ([CP_Info],  String, [String], String, String) -> Int
findMethodRef(cp, name, params, returnType, className) = 
        let
                typ = buildTypeId(cp, name, params, returnType, className)
        in
                findMethodRefInCP(cp, cp, name, typ, className, 1)

findMethodRefInCP :: ([CP_Info], [CP_Info], String, String, String, Int) -> Int
findMethodRefInCP([], ccp, name, typ, className, counter) = -1
findMethodRefInCP((MethodRef_Info a b c d : cp), ccp, name, typ, className, counter) =
        let
                ntReferenz = findNameAndTypeIndex(ccp, name, typ)
                classChecked = checkClass(ccp, ccp, b, className, 1)
        in
                if ntReferenz == c && classChecked then
                        counter
                else
                        findMethodRefInCP(cp, ccp, name, typ, className, counter + 1)
findMethodRefInCP((c:cp), ccp, name, typ, className, counter) = findMethodRefInCP(cp, ccp, name, typ, className, counter + 1)

buildTypeId :: ([CP_Info], String, [String], String, String ) -> String
buildTypeId(cp, name, params, returnType, className) =
        let
                allNTs = findEveryMethodRefInCP(cp, cp, name, className, [])
                listOfTypeIds = getAllTypeIds(cp, cp, allNTs, 1, [])
        in
                checkTypeWithParamList(params, returnType, listOfTypeIds)


getAllTypeIds :: ([CP_Info], [CP_Info], [Int], Int, [String]) -> [String]
getAllTypeIds(_, _, [], _, result) = result
getAllTypeIds((NameAndType_Info a b c d:cp), ccp, (n:ns), counter, result) 
        | counter == n = getAllTypeIds(ccp, ccp, ns, 1, (getStringFromUTF8(ccp, c, 1) : result))
        | otherwise = getAllTypeIds(cp, ccp, (n:ns), counter + 1, result)
getAllTypeIds((c:cp), ccp, (n:ns), counter, result) = getAllTypeIds(cp, ccp, (n:ns), counter + 1, result)

checkTypeWithParamList :: ([String], String, [String]) -> String
checkTypeWithParamList(params, returnType, listOfTypeIds) =
        let
                ownTypeID = buildOwnTypeId(params, returnType, "")
        in
                getTypeIDInList(listOfTypeIds, ownTypeID)

getTypeIDInList :: ([String], String) -> String
getTypeIDInList([], _) = ""  -- TODO (Hier bin ich mir unsicher? Hab ich nicht geschrieben? Was macht diese Methode überhaupt)
getTypeIDInList((t:ts), toFind) =
                if t == toFind then
                        toFind
                else
                        getTypeIDInList(ts, toFind)

buildOwnTypeId :: ([String], String, String) -> String
buildOwnTypeId(ss, ret, "") = buildOwnTypeId(ss, ret, "(")
buildOwnTypeId((s : ss), ret, result)
        | s == "int" = buildOwnTypeId(ss, ret, result ++ "I")
        | s == "char" = buildOwnTypeId(ss, ret, result ++ "C")
        | s == "void" = buildOwnTypeId(ss, ret, result ++ "V")
        | s == "boolean" = buildOwnTypeId(ss, ret, result ++ "Z")
        | s == "String" = buildOwnTypeId(ss, ret, result ++ "Ljava/lang/String;")
        | otherwise = buildOwnTypeId(ss, ret, result ++ "L" ++ s ++ ";")
buildOwnTypeId([], ret, result) 
        | ret == "int" = result ++ ")" ++ "I"
        | ret == "char" = result ++ ")" ++ "C"
        | ret == "void" = result ++ ")" ++ "V"
        | ret == "boolean" = result ++ ")" ++ "Z"
        | ret == "String" = result ++ ")" ++ "Ljava/lang/String;"
        | otherwise = result ++ ")" ++ "L" ++ ret ++ ";"


findEveryMethodRefInCP :: ([CP_Info], [CP_Info], String, String, [Int]) -> [Int]
findEveryMethodRefInCP([], ccp, name, className, resultList) = resultList
findEveryMethodRefInCP((MethodRef_Info a b c d : cp), ccp, name, className, resultList) =
        let
                classCorrect = checkClass( ccp, ccp, b, className, 1)
                --nameAndType = findNTByIndexName(ccp, c, 1)
        in
                if classCorrect then
                        findEveryMethodRefInCP(cp, ccp, name, className, (c : resultList))
                else
                        findEveryMethodRefInCP(cp, ccp, name, className, resultList)
findEveryMethodRefInCP((c : cp), ccp, name, className, resultList) =
        findEveryMethodRefInCP(cp, ccp, name, className, resultList)        


checkName :: ([CP_Info], [CP_Info], Int, String, Int) -> Bool
checkName((Class_Info a b c : cp), ccp, cpid, className, counter)
        | (counter == cpid) = if getStringFromUTF8(ccp, b, 1) == className then True
                                        else False
        | otherwise = checkClass(cp, ccp, cpid, className, counter + 1) 
checkName((c:cp), ccp, cpid, className, counter)
        | (counter == cpid) = False
        | otherwise = checkClass(cp, ccp, cpid, className, counter + 1) 

checkClass :: ([CP_Info], [CP_Info], Int, String, Int) -> Bool
checkClass([], _, _, _, _) = False
checkClass((Class_Info a b c : cp), ccp, cpid, className, counter)
        | (counter == cpid) = if getStringFromUTF8(ccp, b, 1) == className then True
                                        else False
        | otherwise = checkClass(cp, ccp, cpid, className, counter + 1) 
checkClass((c:cp), ccp, cpid, className, counter)
        | (counter == cpid) = False
        | otherwise = checkClass(cp, ccp, cpid, className, counter + 1) 

getStringFromUTF8 :: ([CP_Info], Int, Int) -> String
getStringFromUTF8([], _, _) = "ERROR"
getStringFromUTF8((Utf8_Info a b c d:cp), number, counter)
        | (number == counter) = c
        | otherwise = getStringFromUTF8(cp, number, counter + 1)
getStringFromUTF8((c:cp), number, counter) = getStringFromUTF8(cp, number, counter + 1)
