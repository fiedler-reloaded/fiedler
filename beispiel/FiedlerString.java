public class FiedlerString {
    private char letter;
    private FiedlerString next;

    public FiedlerString(char x) {
        letter = x;
        next = null;
    }

    public boolean appendCharacter(FiedlerString x) {
        if(next == null) {
            next = x;
            return true;
        } else {
            return next.appendCharacter(x);
        }
        return true;
    }

    public FiedlerString deleteCharacter(int x) {
        if(x == 0) {
            return next;
        } else {
            if(next != null) {
                next = next.deleteCharacter(x - 1);
            } 
        }
        return this;
    }

    public char getCharacter(int x) {
        if(x == 0) {
            return letter;
        } else {
            if(next != null) {
                return next.getCharacter(x - 1);
            }
            
        }
        return '?';
    }

    public int getLength(int x) {
        if(next == null) {
            return x + 1;
        }
        return next.getLength(x + 1);
    }
}