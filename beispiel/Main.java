public class Main {
    public static void main(String[] args) {
        
        Calculator ca = new Calculator();
        System.out.println(ca.moduloCalculator_Java(5,3));
        System.out.println(ca.moduloCalculator_while(5,3));
        System.out.println(ca.add(5,3));
        System.out.println(ca.sub(5,3));
        System.out.println(ca.mul(5,3));
        System.out.println(ca.div(5,3));
        System.out.println(ca.calculateDigitSum(598));
        System.out.println(ca.calculateFibonacci_iterative(5));
        System.out.println(ca.calculateFibonacci_recursive(5));
        

        FiedlerString zeichenkette = new FiedlerString('h');
        zeichenkette.appendCharacter(new FiedlerString('a'));
        zeichenkette.appendCharacter(new FiedlerString('l'));
        zeichenkette.appendCharacter(new FiedlerString('l'));
        zeichenkette.appendCharacter(new FiedlerString('o'));
        printString(zeichenkette);
        zeichenkette = zeichenkette.deleteCharacter(4);
        printString(zeichenkette);
        System.out.println(zeichenkette.getCharacter(1));
    }

    public static void printString(FiedlerString x) {
        int length = x.getLength(0);
        for(int i = 0; i<length; i++) {
            System.out.print(x.getCharacter(i));
        }
        System.out.println();
    }
}