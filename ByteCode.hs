module ByteCode where
import ClassFormat
import ClassFile
import Data.Word
import Data.ByteString.Lazy
import System.IO

--in bytestring umwandeln Funktion: pack
fromInt2Word8 :: Int -> Word8
fromInt2Word8 = read.show

mapListfromInt2Word8 :: [Int] -> [Word8]
mapListfromInt2Word8 li = Prelude.map fromInt2Word8 li
-- pack auf mapListfromInt2Word8 aufrufen
-- hPut auf gepackte map aufrufen 
-- aufgerufen mit outh <- openFile "output.class" WriteMode
--                pack outh (mapListfromInt2Word8 **ListaInt**)

generateByteCode :: [ClassFile] -> [(ListaInt,String)]
generateByteCode [] = []
generateByteCode (x:xs) = (generateClassByteCode x : generateByteCode xs)

-- writeByteCode :: ListaInt -> IO ()
-- writeByteCode list = do
--     outh <- openFile "test.class" WriteMode
--     hPut outh(pack(mapListfromInt2Word8 list))

-- writeByteCodes :: [ListaInt] -> Bool
-- writeByteCodes [] = []
-- writeByteCodes (x:xs) = 
--     let 
--         writeByteCodes xs
--     in writeByteCode x


generateClassByteCode :: ClassFile -> (ListaInt,String)
generateClassByteCode(ClassFile magic minver majver count_cp array_cp acfg this super count_interfaces array_interfaces count_fields array_fields count_methods array_methods count_attributes array_attributes) =
    let
        _class_name = constPoolClassName(array_cp, array_cp, this, 1)
        _magic = addMagic(magic)
        _minver = addMinVer(minver)
        _majver = addMaxVer(majver)
        _count_cp = addConst(count_cp)
        _array_cp = constPoolToByteCode(array_cp)
        _acfg = addAccessFlags(acfg)
        _this = thisClassToListaInt(this)
        _super = superClassToListaInt(super)
        _count_interfaces = addConst(count_interfaces)
        _array_interfaces = addArrayInterfaces(array_interfaces)
        _count_fields = addConst(count_fields)
        _array_fields = addFieldInfos(array_fields)
        _count_methods = addConst(count_methods)
        _array_methods = addMethodInfos(array_methods)
        _count_attributes = addConst(count_attributes)
        _array_attributes = addArrayAttributes(array_attributes)
    in 
        ((_magic ++ _minver ++ _majver ++ _count_cp ++ _array_cp ++ _acfg ++ _this ++ _super ++ _count_interfaces ++ _array_interfaces ++ _count_fields ++ _array_fields ++ _count_methods ++ _array_methods ++ _count_attributes ++ _array_attributes),_class_name)

-- breakdown functions for different data structures
bdInt :: Int -> ListaInt
bdInt(big_int) = 
    let
        firstPart = big_int `div` 256
        secondPart = big_int `mod` 256
    in 
        [firstPart, secondPart]
        
bdList :: [Int] -> ListaInt
bdList [] = []
bdList (x:xs) = (bdInt x ++ bdList xs)

doubleBdInt :: Int -> ListaInt
doubleBdInt(big_int) =
    let
        first_int = big_int `div` 2
        second_int = big_int - first_int
    in 
        if big_int < 256 then
            [00, 00, 00, big_int]
        else
            (bdInt(first_int) ++ bdInt(second_int))

bdTupla4List :: [(Int, Int, Int, Int)] -> ListaInt
bdTupla4List [] = []
bdTupla4List (x:xs) = (bdTupla4 x ++ bdTupla4List xs)

bdTupla4 :: (Int, Int, Int, Int) -> ListaInt
bdTupla4 (a ,b ,c ,d) = (bdInt a ++ bdInt b ++ bdInt c ++ bdInt d)

bdTupla5List :: [(Int, Int, Int, Int, Int)] -> ListaInt
bdTupla5List [] = []
bdTupla5List (x:xs) = (bdTupla5 x ++ bdTupla5List xs)

bdTupla5 :: (Int, Int, Int, Int, Int) -> ListaInt
bdTupla5 (a, b, c, d, e) = (bdInt a ++ bdInt b ++ bdInt c ++ bdInt d ++ bdInt e)

bdTupla2List :: [(Int, Int)] -> ListaInt
bdTupla2List [] = []
bdTupla2List (x:xs) = (bdTupla2 x ++ bdTupla2List xs)

bdTupla2 :: (Int, Int) -> ListaInt
bdTupla2 (a, b) = (bdInt a ++ bdInt b)

bdArrayClasses :: [(Int,Int,Int,AccessFlags)] -> ListaInt
bdArrayClasses[] = []
bdArrayClasses(x:xs) = (bdArrayClass x ++ bdArrayClasses xs)

bdArrayClass :: (Int,Int,Int,AccessFlags) -> ListaInt
bdArrayClass (a, b, c, d) = (bdInt a ++ bdInt b ++ bdInt c ++ addAccessFlags d)

--------------- MAGIC; MINVER; MAXVER
addMagic :: Magic -> ListaInt
addMagic magic = [202, 254, 186, 190]

addMinVer :: MinorVersion -> ListaInt
addMinVer (MinorVersion x) = [0, x]

addMaxVer :: MajorVersion -> ListaInt
addMaxVer (MajorVersion x) = [0, x]


------------ ALL COUNTS FOUND IN CLASS FILE
addConst :: Int -> ListaInt
addConst constCount
    | constCount > 255  = bdInt(constCount)
    | otherwise = [00, constCount]


addAccessFlags :: AccessFlags -> ListaInt
addAccessFlags (AccessFlags list) = reqAddAccessFlags list

reqAddAccessFlags :: [Int] -> ListaInt
reqAddAccessFlags x =
    let
        result = addInt(x)
    in
        if (result > 255) then bdInt(result)
        else [00, result]

addInt :: [Int] -> Int
addInt [] = 0
addInt (x:xs) = x + addInt(xs)

thisClassToListaInt :: ThisClass -> ListaInt
thisClassToListaInt (ThisClass index_th)
     |index_th > 255 = bdInt(index_th)
     |otherwise = [00, index_th]

superClassToListaInt :: SuperClass -> ListaInt
superClassToListaInt (SuperClass index_sp)
    |index_sp > 255 = bdInt(index_sp)
    |otherwise = [00, index_sp]


addArrayInterfaces :: [Interface] -> ListaInt
addArrayInterfaces [] = []
addArrayInterfaces (x:xs) = (addArrayInterface x ++ addArrayInterfaces xs)

addArrayInterface :: Interface -> ListaInt
addArrayInterface (Interface x)
    | x > 255 = bdInt(x)
    | otherwise = [00, x]

addMethodInfos :: [Method_Info] -> ListaInt
addMethodInfos [] = []
addMethodInfos (x:xs) = (addMethodInfo x ++ addMethodInfos xs)

addMethodInfo :: Method_Info -> ListaInt
addMethodInfo (Method_Info a b c d e) = (addAccessFlags a ++ addConst b ++ addConst c ++ addConst d ++ addArrayAttributes e)

addFieldInfos :: [Field_Info] -> ListaInt
addFieldInfos [] = []
addFieldInfos (x:xs) = (addFieldInfo x ++ addFieldInfos xs)
addFieldInfo :: Field_Info -> ListaInt
addFieldInfo (Field_Info a b c d e) = (addAccessFlags a ++ addConst b ++ addConst c ++ addConst d ++ addArrayAttributes e)

addArrayAttributes :: Attribute_Infos -> ListaInt
addArrayAttributes [] = []
addArrayAttributes (x:xs) = (addArrayAttribute x ++ addArrayAttributes xs)

addArrayAttribute :: Attribute_Info -> ListaInt
addArrayAttribute (AttributeCode p1 p2 p3 p4 p5 p6 p7 p8 p9 p10) = (bdInt p1 ++ doubleBdInt p2 ++ bdInt p3 ++ bdInt p4 ++ doubleBdInt p5 ++ p6 ++ bdInt p7 ++ bdTupla4List p8 ++ bdInt p9 ++ addArrayAttributes p10)

------------------ CP-INFO
constPoolToByteCode :: CP_Infos -> ListaInt
constPoolToByteCode [] = []
constPoolToByteCode (x:xs) = (cpInfoToByteCode x ++ constPoolToByteCode xs)

constPoolClassName :: (CP_Infos, CP_Infos, ThisClass, Int) -> String
constPoolClassName ([], _, _, _) = "Error"
constPoolClassName (((Class_Info a b c):xs), cp, ThisClass(t), counter) = 
    if (t == counter) then 
        getClassName(b, cp, 1)
    else constPoolClassName(xs, cp, ThisClass(t), counter+1)
constPoolClassName ((x:xs), cp, ThisClass(t), counter) = 
    constPoolClassName(xs, cp, ThisClass(t), counter+1)

getClassName :: (Int, CP_Infos, Int) -> String
getClassName (x, ((Utf8_Info a b c d):is), counter) = 
    if (x == counter) then c
    else getClassName(x, is, counter+1)
getClassName (x, (i:is), counter) = 
    getClassName(x, is, counter+1)

convertStringToInt :: [Char] -> ListaInt
convertStringToInt [] = []
convertStringToInt(x:xs) = (fromEnum(x):convertStringToInt(xs))

cpInfoToByteCode :: CP_Info -> ListaInt
cpInfoToByteCode (MethodRef_Info a b c d) = ([10] ++ methodRefToListaInt(MethodRef_Info a b c d))
cpInfoToByteCode (Utf8_Info a b c d) = ([1] ++ utf8InfoToByteCode(Utf8_Info a b c d) ++ convertStringToInt(c))
cpInfoToByteCode (Class_Info a b c) = ([7] ++ classInfotoListaInt(Class_Info a b c))
cpInfoToByteCode (NameAndType_Info a b c d) = ([12] ++ nameAndTypeToListaInt(NameAndType_Info a b c d))
cpInfoToByteCode (String_Info a b c) = ([8] ++ bdInt(b))
cpInfoToByteCode (Integer_Info a b c) = ([3] ++ bdInt(b))
cpInfoToByteCode (FieldRef_Info a b c d) = ([9] ++ bdInt(b) ++ bdInt(c))
-- cpInfoToByteCode (Float_Info a b c) = (bdInt b)
-- cpInfoToByteCode (Long_Info a b c d) = (11 ++ bdInt b ++ bdInt c)
-- cpInfoToByteCode (Double_Info a b c d) = (11 ++ bdInt b ++ bdInt c)


classInfotoListaInt :: CP_Info -> ListaInt
classInfotoListaInt(Class_Info _ index_cp _)
          |index_cp > 255 = bdInt(index_cp)
          |otherwise = [00, index_cp]

utf8InfoToByteCode :: CP_Info -> ListaInt
utf8InfoToByteCode (Utf8_Info _ tamcp _ _)
        | tamcp > 255 = bdInt(tamcp)
        | otherwise = [00, tamcp]

methodRefToListaInt :: CP_Info -> ListaInt
methodRefToListaInt (MethodRef_Info _ indx_name indx_nameandtype _)
        | (indx_name > 255 && indx_nameandtype > 255) = (bdInt(indx_name) ++ bdInt(indx_nameandtype))
        | (indx_name > 255 && indx_nameandtype <= 255) = (bdInt(indx_name) ++ [00, indx_nameandtype])
        | (indx_name <= 255 && indx_nameandtype > 255) = ([00, indx_name] ++ bdInt(indx_nameandtype))
        | (indx_name <= 255 && indx_nameandtype <= 255) = ([00, indx_name] ++ [00, indx_nameandtype])

nameAndTypeToListaInt ::  CP_Info -> ListaInt
nameAndTypeToListaInt (NameAndType_Info _ indx_name indx_descr _)
        |(indx_name > 255 && indx_descr > 255) = (bdInt(indx_name) ++ bdInt(indx_descr))
        |(indx_name > 255 && indx_descr <= 255) = (bdInt(indx_name) ++ [00, indx_descr])
        |(indx_name <= 255 && indx_descr > 255) = ([00, indx_name] ++ bdInt(indx_descr))
        |(indx_name <= 255 && indx_descr <= 255) = ([00, indx_name] ++ [00, indx_descr])
