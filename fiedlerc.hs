module Main where
import AbstrakteSyntax
import Parser
import ClassFile
import ClassFormat
import SemantikCheck
import ByteCode
import System.Environment
import Data.Word
import Data.ByteString.Lazy
import System.IO as I

compiler :: String -> [(ListaInt, String)]
compiler = generateByteCode . createPrg . typecheckPrg . parser

compilerDebug = createPrg . typecheckPrg . parser

getBytecode :: [(ListaInt, String)] -> [ListaInt]
getBytecode([]) = []
getBytecode((x,y) : s) = (x : getBytecode(s))

getClassnames :: [(ListaInt, String)] -> [String]
getClassnames([]) = []
getClassnames((x,y) : s) = (y : getClassnames(s))

generateClassfiles :: ([ListaInt], [String]) -> IO()
generateClassfiles([],[]) = do
	print "Compiling successful"
generateClassfiles((l:ls), (s:ss)) = do
	outh <- openFile (s ++ ".class") WriteMode
	print (show l)
	hPut outh(pack(mapListfromInt2Word8(l)))
	hClose outh
	generateClassfiles(ls,ss)

emptyJava_BytecodeResult = [ClassFile {
    magic = Magic,
    minver = MinorVersion {numMinVer = 0},
    maxver = MajorVersion {numMaxVer = 49},
    count_cp = 15,
    array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 12, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 13, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 14, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 14, cad_cp = "doAnotherThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 22, cad_cp = "(ILjava/lang/String;)V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "doAThirdThing", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}],
    acfg = AccessFlags [1,32],
    this = ThisClass {index_th = 2},
    super = SuperClass {index_sp = 3},
    count_interfaces = 0,
    array_interfaces = [],
    count_fields = 0,
    array_fields = [],
    count_methods = 4,
    array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 4, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 7, index_descr_mi = 5, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 1, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 8, index_descr_mi = 9, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 13, len_stack_attr = 0, len_local_attr = 3, tam_code_attr = 1, array_code_attr = [177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 15, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 3, array_code_attr = [16,10,172], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
    count_attributes = 0,
    array_attributes = []},
    ClassFile {
        magic = Magic,
        minver = MinorVersion {numMinVer = 0},
        maxver = MajorVersion {numMaxVer = 49},
        count_cp = 21,
        array_cp = [MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 7, index_nameandtype_cp = 14, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 14, desc = ""},FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 6, index_nameandtype_cp = 16, desc = ""},MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 2, index_nameandtype_cp = 17, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},Class_Info {tag_cp = TagClass, index_cp = 19, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "helper", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "LHelperClass;", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "method1", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "HelperClass", desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 8, index_descr_cp = 9, desc = ""},NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 20, index_descr_cp = 11, desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "InstanceCalls", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "doSomething", desc = ""}],
        acfg = AccessFlags [1,32],
        this = ThisClass {index_th = 6},
        super = SuperClass {index_sp = 7},
        count_interfaces = 0,
        array_interfaces = [],
        count_fields = 1,
        array_fields = [Field_Info {af_fi = AccessFlags [2], index_name_fi = 8, index_descr_fi = 9, tam_fi = 0, array_attr_fi = []}],
        count_methods = 2,
        array_methods = [Method_Info {af_mi = AccessFlags [1], index_name_mi = 10, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 28, len_stack_attr = 3, len_local_attr = 1, tam_code_attr = 16, array_code_attr = [42,183,0,1,42,187,0,2,89,183,0,3,181,0,4,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]},Method_Info {af_mi = AccessFlags [1], index_name_mi = 13, index_descr_mi = 11, tam_mi = 1, array_attr_mi = [AttributeCode {index_name_attr = 12, tam_len_attr = 20, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 8, array_code_attr = [42,180,0,4,182,0,5,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]}],
        count_attributes = 0,
        array_attributes = []}]

main = do 
	(a:args) <- getArgs 
	s <- I.readFile a
	--print(parser s)
	generateClassfiles(getBytecode(compiler s), getClassnames(compiler s))
	--generateClassfiles(getBytecode(generateByteCode emptyJava_BytecodeResult), getClassnames(generateByteCode emptyJava_BytecodeResult))
	--(byteCode, classname) <- x
	----outh <- openFile classname WriteMode
	--hPut outh(pack(mapListfromInt2Word8(byteCode)))
	--print (compilerDebug s)
	-- (h:hs) <- compiler s
	-- print (Prelude.head(compiler s))